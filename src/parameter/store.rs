use super::vault::VaultSecretResponse;
use crate::{
  config::*,
  http::{get_credential, AuthenticatedClient, HttpError},
  MessageError, Result,
};
use reqwest::blocking::Client;
use serde_json::Value;
use std::env::var;

pub fn request_value(credential_key: &str, store_code: &str) -> Result<Value> {
  match store_code.to_lowercase().as_str() {
    "env" | "environment" => var(credential_key)
      .map_err(|error| MessageError::RuntimeError(error.to_string()))
      .map(|value| serde_json::from_str(&value).unwrap_or(Value::String(value))),
    "vault" => {
      let credentials = get_credentials_from_vault(credential_key, store_code)?;
      Ok(credentials)
    }
    _ => {
      let credentials = get_credentials_from_backend(credential_key, store_code)?;
      Ok(credentials)
    }
  }
}

fn get_credentials_from_vault(
  credential_key: &str,
  store_code: &str,
) -> std::result::Result<Value, HttpError> {
  let vault_endpoint = get_store_hostname_with_default(store_code, "http://127.0.0.1:8200/v1");
  let vault_token = get_store_token(store_code);

  let credential_url = format!("{vault_endpoint}/secret/data/{credential_key}");

  let client = Client::builder().build().map_err(|e| format!("{e:?}"))?;

  // retrieve the secret indicated by the credential key
  let response: VaultSecretResponse = client
    .get(credential_url)
    .header("X-Vault-Token", vault_token)
    .send()?
    .error_for_status()?
    .json()?;

  // return the secret content as a JSON value
  Ok(response.data.data)
}

fn get_credentials_from_backend(
  credential_key: &str,
  store_code: &str,
) -> std::result::Result<Value, HttpError> {
  let client = AuthenticatedClient::new(store_code)?;
  get_credential(client, credential_key)
}
