use regex::Regex;
use std::fs;
use uuid::Uuid;

static mut INSTANCE_UUID: Option<String> = None;

/// Retrieve the identifier of this instance.
///
/// It can be the Docker container ID.
/// Else an UUID is generated to provide an unique identifier.
///
pub fn get_instance_id() -> String {
  get_internal_instance_id(None)
}

fn get_internal_instance_id(file_prefix: Option<&str>) -> String {
  retrieve_docker_id(file_prefix).unwrap_or_else(|| unsafe {
    if let Some(instance_uuid) = &INSTANCE_UUID {
      instance_uuid.to_string()
    } else {
      let identifier = format!("{:?}", Uuid::new_v4());
      INSTANCE_UUID = Some(identifier.clone());
      identifier
    }
  })
}

fn retrieve_docker_id(file_prefix: Option<&str>) -> Option<String> {
  let file_prefix = file_prefix.unwrap_or("/proc/self");
  fs::read_to_string(format!("{file_prefix}/cgroup"))
    .map(|content| parse_docker_container_id(&content))
    .unwrap_or_else(|_| None)
    .or_else(|| {
      fs::read_to_string(format!("{file_prefix}/mountinfo"))
        .map(|content| parse_docker_container_id(&content))
        .unwrap_or_else(|_| None)
    })
}

fn parse_docker_container_id(content: &str) -> Option<String> {
  let re =
    Regex::new(r"(?:docker/|docker-|cri-containerd-|containers/|pod[a-z0-9-]{36}/)([a-z0-9]{64})")
      .unwrap();
  let caps = re.captures(content)?;
  let mut long_identifier = caps.get(1).map(|c| c.as_str().to_string())?;

  long_identifier.truncate(12);
  Some(long_identifier)
}

#[test]
fn test_get_instance_id() {
  assert_eq!(
    get_internal_instance_id(Some("./tests/instance_id/base")),
    "da9002cb1553".to_string()
  );

  assert_eq!(
    get_internal_instance_id(Some("./tests/instance_id/k8s")),
    "54a5797a68b6".to_string()
  );

  assert_eq!(
    get_internal_instance_id(Some("./tests/instance_id/docker_newer")),
    "73ea6278bc89".to_string()
  );

  assert_eq!(
    get_internal_instance_id(Some("./tests/instance_id/k8s_docker_newer")),
    "4e363357adfd".to_string()
  );

  assert_eq!(
    get_internal_instance_id(Some("./tests/instance_id/eks_containerd")),
    "c7e6a4d37eb6".to_string()
  );

  let str_uuid = get_internal_instance_id(Some("/tmp/file_not_exists"));
  let parsed_uuid = Uuid::parse_str(&str_uuid);
  assert!(parsed_uuid.is_ok());

  assert_eq!(parse_docker_container_id(""), None);
  assert_eq!(parse_docker_container_id("\n"), None);
  assert_eq!(parse_docker_container_id("a:b:c\n"), None);
}
