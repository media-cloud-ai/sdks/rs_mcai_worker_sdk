use crate::job::JobStatus;

/// Worker activity mode
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum WorkerActivity {
  Idle,
  Busy,
  Suspended,
}

impl From<JobStatus> for WorkerActivity {
  fn from(job_status: JobStatus) -> Self {
    match job_status {
      JobStatus::ReadyToStart | JobStatus::Processing | JobStatus::Updated => WorkerActivity::Busy,
      JobStatus::Completed | JobStatus::Stopped | JobStatus::Error | JobStatus::Unknown => {
        WorkerActivity::Idle
      }
    }
  }
}
