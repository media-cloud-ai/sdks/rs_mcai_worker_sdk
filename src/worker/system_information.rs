use crate::worker::WorkerConfiguration;
use nvml_wrapper::{enum_wrappers::device::Clock, error::NvmlError, NVML};
use std::str;
use sysinfo::{Disk, DiskExt, DiskType, Processor, ProcessorExt, SystemExt};

/// System information where the worker is running
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct SystemInformation {
  docker_container_id: String,
  os_version: String,
  memory: MemoryInfo,
  cores: Vec<CoreInfo>,
  gpus: Vec<GpuInfo>,
  disks: Vec<DiskInfo>,
}

/// Memory information where the worker is running
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct MemoryInfo {
  total_memory: u64,
  used_memory: u64,
  total_swap: u64,
  used_swap: u64,
}

/// CPU information where the worker is running
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
struct CoreInfo {
  name: String,
  vendor_id: String,
  brand: String,
  frequency: u64,
}

impl From<&Processor> for CoreInfo {
  fn from(processor: &Processor) -> Self {
    CoreInfo {
      name: processor.name().parse().unwrap(),
      vendor_id: processor.vendor_id().parse().unwrap(),
      brand: processor.brand().parse().unwrap(),
      frequency: processor.frequency(),
    }
  }
}

/// GPU information where the worker is running
#[derive(Debug, Deserialize, Clone, Eq, PartialEq, Serialize)]
struct GpuInfo {
  name: String,
  memory: u64,
  driver_version: String,
  cuda_driver_version: i32,
  max_clock_speed_memory: u32,
  max_clock_speed_sm: u32,
  max_clock_speed_graphics: u32,
  max_clock_speed_video: u32,
}

/// Disk information where the worker is running
#[derive(Debug, Deserialize, Clone, Eq, PartialEq, Serialize)]
struct DiskInfo {
  disk_type: String,
  size: u64,
  format: String,
  mountpoint: String,
}

impl From<&Disk> for DiskInfo {
  fn from(disk: &Disk) -> Self {
    let disk_type = match disk.type_() {
      DiskType::HDD => "HDD",
      DiskType::SSD => "SSD",
      _ => "unknown",
    }
    .to_string();

    DiskInfo {
      disk_type,
      size: disk.total_space(),
      format: str::from_utf8(disk.file_system())
        .map(|format| format.to_string())
        .unwrap_or_default(),
      mountpoint: disk
        .mount_point()
        .to_str()
        .map(|path| path.to_string())
        .unwrap_or_default(),
    }
  }
}

impl SystemInformation {
  pub(crate) fn new(worker_configuration: &WorkerConfiguration) -> Self {
    let mut system = sysinfo::System::new_all();
    system.refresh_all();

    let docker_container_id = worker_configuration.get_instance_id();
    let os_version = system.long_os_version().unwrap();

    let cores: Vec<CoreInfo> = system
      .processors()
      .iter()
      .map(|processor| processor.into())
      .collect();
    let disks: Vec<DiskInfo> = system.disks().iter().map(|disk| disk.into()).collect();
    let gpus = Self::get_gpu_info().unwrap_or_default();

    let total_memory = system.total_memory();
    let used_memory = system.used_memory();
    let total_swap = system.total_swap();
    let used_swap = system.used_swap();
    let memory = MemoryInfo {
      total_memory,
      used_memory,
      total_swap,
      used_swap,
    };

    SystemInformation {
      docker_container_id,
      os_version,
      memory,
      cores,
      gpus,
      disks,
    }
  }

  pub(crate) fn enable_accounting_on_gpu() -> Result<(), NvmlError> {
    let nvml = NVML::init()?;
    let device_count = nvml.device_count()?;
    for device_index in 0..device_count {
      nvml.device_by_index(device_index)?.set_accounting(true)?;
    }
    Ok(())
  }

  fn get_gpu_info() -> Result<Vec<GpuInfo>, NvmlError> {
    let nvml = NVML::init()?;

    let device_count = nvml.device_count()?;
    let mut gpu_list: Vec<GpuInfo> = Vec::with_capacity(device_count as usize);

    let driver_version = nvml.sys_driver_version().unwrap_or_default();
    let cuda_driver_version = nvml.sys_cuda_driver_version().unwrap_or_default();

    for device_index in 0..device_count {
      let device = nvml.device_by_index(device_index)?;
      let name = device.name().unwrap_or_default();

      let memory = device
        .memory_info()
        .map(|memory| memory.total)
        .unwrap_or_default();

      let max_clock_speed_memory = device.max_clock_info(Clock::Memory).unwrap_or_default();
      let max_clock_speed_sm = device.max_clock_info(Clock::SM).unwrap_or_default();
      let max_clock_speed_graphics = device.max_clock_info(Clock::Graphics).unwrap_or_default();
      let max_clock_speed_video = device.max_clock_info(Clock::Video).unwrap_or_default();

      gpu_list.push(GpuInfo {
        name,
        driver_version: driver_version.clone(),
        cuda_driver_version,
        memory,
        max_clock_speed_memory,
        max_clock_speed_sm,
        max_clock_speed_graphics,
        max_clock_speed_video,
      })
    }

    Ok(gpu_list)
  }
}
