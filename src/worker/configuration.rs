use crate::mcai_worker::{McaiWorker, McaiWorkerDescription, McaiWorkerDocumentation};
use mcai_license::McaiWorkerLicense;
use schemars::{schema::RootSchema, JsonSchema};
use semver::Version;
use serde::de::DeserializeOwned;

/// Structure that contains worker description
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct WorkerDescription {
  instance_id: String,
  queue_name: String,
  name: String,
  direct_messaging_queue_name: String,
  label: String,
  description: String,
  #[serde(skip_serializing_if = "Option::is_none", default)]
  documentation: Option<McaiWorkerDocumentation>,
  version: String,
  sdk_version: Version,
  license: McaiWorkerLicense,
  #[serde(skip_serializing_if = "Vec::is_empty", default)]
  authors: Vec<String>,
  #[serde(skip_serializing_if = "Option::is_none", default)]
  homepage: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none", default)]
  repository: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none", default)]
  organisation: Option<String>,
}

impl Eq for WorkerDescription {}

/// Structure that contains configuration for that worker
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct WorkerConfiguration {
  #[serde(flatten)]
  description: WorkerDescription,
  parameters: RootSchema,
}

impl WorkerConfiguration {
  pub fn new<P: DeserializeOwned + JsonSchema, D: McaiWorkerDescription, W: McaiWorker<P, D>>(
    // TODO should be optional
    queue_name: &str,
    mcai_worker: &W,
    instance_id: &str,
  ) -> crate::Result<Self> {
    let package: cargo_toml::Package = include!(concat!(env!("OUT_DIR"), "/mcai_build.rs"));

    let sdk_version = if let Ok(version_string) = package.version.get() {
      Version::parse(version_string).unwrap_or_else(|_| Version::new(0, 0, 0))
    } else {
      Version::new(0, 0, 0)
    };

    let parameters = mcai_worker.get_parameters_schema()?;

    let identifier =
      std::env::var("DIRECT_MESSAGING_IDENTIFIER").unwrap_or_else(|_| instance_id.to_string());

    let direct_messaging_queue_name = format!("direct_messaging_{identifier}");

    let mcai_worker = mcai_worker.get_mcai_worker_description();

    Ok(WorkerConfiguration {
      description: WorkerDescription {
        instance_id: instance_id.to_string(),
        name: mcai_worker.get_name(),
        queue_name: queue_name.to_string(),
        direct_messaging_queue_name,
        label: mcai_worker.get_name(),
        sdk_version,
        version: mcai_worker.get_version(),
        description: mcai_worker.get_description(),
        documentation: mcai_worker.get_documentation(),
        license: mcai_worker.get_license(),
        authors: mcai_worker.get_authors(),
        homepage: mcai_worker.get_homepage(),
        repository: mcai_worker.get_repository(),
        organisation: mcai_worker.get_organisation(),
      },
      parameters,
    })
  }

  pub fn get_instance_id(&self) -> String {
    self.description.instance_id.clone()
  }

  pub fn get_queue_name(&self) -> String {
    self.description.queue_name.clone()
  }

  pub fn get_worker_name(&self) -> String {
    self.description.label.clone()
  }

  pub fn get_worker_version(&self) -> String {
    self.description.version.clone()
  }

  pub fn get_sdk_version(&self) -> String {
    self.description.sdk_version.to_string()
  }

  pub fn get_worker_description(&self) -> WorkerDescription {
    self.description.clone()
  }

  pub fn get_direct_messaging_queue_name(&self) -> String {
    self.description.direct_messaging_queue_name.clone()
  }
}

impl Eq for WorkerConfiguration {}
