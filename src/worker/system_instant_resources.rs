use nvml_wrapper::{enum_wrappers::device::Clock, error::NvmlError, Device, NVML};
use sysinfo::{LoadAvg, NetworkData, NetworkExt, NetworksExt, Processor, ProcessorExt, SystemExt};

/// System instant resources where the worker is running
#[derive(Debug, Deserialize, Clone, Eq, PartialEq, Serialize)]
pub struct SystemInstantResources {
  memory: u64,
  swap: u64,
  load_average: Load,
  cores: Vec<CoreUsage>,
  gpus: Vec<GpuUsage>,
  networks: Vec<NetworkUsage>,
}

/// Processor load where the worker is running
#[derive(Debug, Deserialize, Clone, PartialEq, Serialize)]
struct Load {
  one_minute: f64,
  five_minute: f64,
  fifteen_minute: f64,
}

impl From<LoadAvg> for Load {
  fn from(load_avg: LoadAvg) -> Self {
    Load {
      one_minute: load_avg.one,
      five_minute: load_avg.five,
      fifteen_minute: load_avg.fifteen,
    }
  }
}

impl Eq for Load {}

/// Processors instant resources where the worker is running
#[derive(Debug, Deserialize, Clone, Eq, PartialEq, Serialize)]
struct CoreUsage {
  name: String,
  usage: u64,
}

impl From<&Processor> for CoreUsage {
  fn from(processor: &Processor) -> Self {
    CoreUsage {
      name: processor.name().parse().unwrap_or_default(),
      usage: processor.cpu_usage() as u64,
    }
  }
}

/// Gpu instant resources where the worker is running
#[derive(Debug, Deserialize, Clone, Eq, PartialEq, Serialize)]
struct GpuUsage {
  name: String,
  memory: u64,
  clock_speed_memory: u32,
  clock_speed_sm: u32,
  clock_speed_graphics: u32,
  clock_speed_video: u32,
}

impl From<&Device<'_>> for GpuUsage {
  fn from(device: &Device) -> Self {
    GpuUsage {
      name: device.name().unwrap_or_default(),
      memory: device
        .memory_info()
        .map(|memory| memory.used)
        .unwrap_or_default(),
      clock_speed_memory: device.clock_info(Clock::Memory).unwrap_or_default(),
      clock_speed_sm: device.clock_info(Clock::SM).unwrap_or_default(),
      clock_speed_graphics: device.clock_info(Clock::Graphics).unwrap_or_default(),
      clock_speed_video: device.clock_info(Clock::Video).unwrap_or_default(),
    }
  }
}

/// Network instant resources where the worker is running
#[derive(Debug, Deserialize, Clone, Eq, PartialEq, Serialize)]
struct NetworkUsage {
  name: String,
  total_bytes_received: u64,
  total_bytes_transmitted: u64,
  total_packets_received: u64,
  total_packets_transmitted: u64,
}

impl From<(&String, &NetworkData)> for NetworkUsage {
  fn from((name, network_data): (&String, &NetworkData)) -> Self {
    NetworkUsage {
      name: name.to_string(),
      total_bytes_received: network_data.total_received(),
      total_bytes_transmitted: network_data.total_transmitted(),
      total_packets_received: network_data.total_packets_received(),
      total_packets_transmitted: network_data.total_packets_transmitted(),
    }
  }
}

impl SystemInstantResources {
  pub(crate) fn new() -> SystemInstantResources {
    let mut system = sysinfo::System::new_all();
    system.refresh_all();

    let memory = system.used_memory();
    let swap = system.used_swap();

    let cores: Vec<CoreUsage> = system
      .processors()
      .iter()
      .map(|processor| processor.into())
      .collect();

    let load_average = system.load_average().into();

    let gpus = Self::get_gpu_instant_resources().unwrap_or_default();

    let networks: Vec<NetworkUsage> = system
      .networks()
      .iter()
      .map(|network| network.into())
      .collect();

    SystemInstantResources {
      memory,
      swap,
      cores,
      load_average,
      gpus,
      networks,
    }
  }

  fn get_gpu_instant_resources() -> Result<Vec<GpuUsage>, NvmlError> {
    let nvml = NVML::init()?;
    let device_count = nvml.device_count()?;

    let mut devices: Vec<Device> = Vec::with_capacity(device_count as usize);
    for device_index in 0..device_count {
      devices.push(nvml.device_by_index(device_index)?)
    }

    Ok(devices.iter().map(|device| device.into()).collect())
  }
}
