use super::{
  configuration::WorkerDescription, SystemInformation, WorkerActivity, WorkerConfiguration,
};
use crate::prelude::BlackList;

/// Container to generate the status message for the worker
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct WorkerStatus {
  #[serde(flatten)]
  pub worker_description: WorkerDescription,
  pub activity: WorkerActivity,
  pub system_info: SystemInformation,
}

impl WorkerStatus {
  pub fn new(activity: WorkerActivity, worker_configuration: WorkerConfiguration) -> Self {
    let system_info = SystemInformation::new(&worker_configuration);
    WorkerStatus {
      worker_description: worker_configuration.get_worker_description(),
      activity,
      system_info,
    }
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct WorkerBlackList {
  worker_description: WorkerDescription,
  black_list: Option<BlackList>,
}

impl WorkerBlackList {
  pub fn new(worker_description: WorkerDescription) -> Self {
    Self {
      worker_description,
      black_list: None,
    }
  }

  pub fn set_black_list(&mut self, black_list: BlackList) {
    self.black_list = Some(black_list);
  }
}
