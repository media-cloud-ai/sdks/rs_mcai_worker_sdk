//! Module to manage Job

mod black_list;
mod job_progression;
mod job_result;
mod job_status;

pub use black_list::{BlackList, BlackListChangeMessage, MINIMUM_BLACK_LIST_STEP_FLOW_VERSION};
pub use job_progression::JobProgression;
pub use job_result::JobResult;
pub use job_status::JobStatus;

use crate::{
  parameter::{container::ParametersContainer, store::request_value, Parameter, Requirement},
  MessageError, Result,
};
use jsonschema::JSONSchema;
use schemars::{schema::RootSchema, schema_for, JsonSchema};
use serde::{de::DeserializeOwned, Deserialize};
use serde_json::{Map, Value};
use std::path::Path;

#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
pub struct Job {
  pub job_id: u64,
  pub parameters: Vec<Parameter>,
}

impl Job {
  pub fn new(message: &str) -> Result<Self> {
    let parsed: std::result::Result<Job, _> = serde_json::from_str(message);
    parsed.map_err(|e| MessageError::RuntimeError(format!("unable to parse input message: {e:?}")))
  }

  pub fn get_parameters<P: Sized + DeserializeOwned + JsonSchema>(&self) -> Result<P> {
    let parameters_schema: RootSchema = schema_for!(P);
    let mut parameters = Map::<String, Value>::new();
    for parameter in &self.parameters {
      if let Some(value) = parameter
        .value
        .clone()
        .or_else(|| parameter.default.clone())
      {
        let value = if let Some(store_code) = &parameter.store {
          log::debug!(
            "Retrieve credential value {} from store {}",
            value.to_string(),
            store_code
          );

          if let Value::String(credential_key) = value {
            request_value(&credential_key, store_code)
              .map_err(|e| MessageError::ParameterValueError(format!("{e:?}")))
          } else {
            Err(MessageError::ParameterValueError(format!(
              "Cannot handle credential type for {value:?}"
            )))
          }?
        } else {
          value
        };
        parameters.insert(parameter.id.clone(), value);
      }
    }
    let parameters = Value::Object(parameters);

    Self::validate_parameters(parameters_schema, parameters.clone())?;

    serde_json::from_value(parameters.clone()).map_err(|error| {
      MessageError::ParameterValueError(format!(
        "Cannot get parameters from {parameters:?}: {error:?}"
      ))
    })
  }

  pub fn check_requirements(&self) -> Result<()> {
    if let Ok(requirements) = self.get_parameter::<Requirement>("requirements") {
      if let Some(paths) = requirements.paths {
        for path in paths.iter() {
          let p = Path::new(path);
          if !p.exists() {
            return Err(MessageError::RequirementsError(format!(
              "Warning: Required file does not exists: {p:?}"
            )));
          }
        }
      }
    }
    Ok(())
  }

  pub fn validate_parameters(schema: RootSchema, parameters_value: Value) -> Result<()> {
    let schema_json_value = serde_json::to_value(schema)?;
    let json_schema = JSONSchema::compile(&schema_json_value)
      .map_err(|error| MessageError::RuntimeError(error.to_string()))?;

    json_schema.validate(&parameters_value).map_err(|errors| {
      let mut messages = vec![];
      for error in errors {
        messages.push(error.to_string());
      }
      MessageError::ParameterValueError(format!(
        "Parameters validation errors: {}",
        messages.join(", ")
      ))
    })
  }
}

impl ParametersContainer for Job {
  fn get_parameters(&self) -> &Vec<Parameter> {
    &self.parameters
  }
}
