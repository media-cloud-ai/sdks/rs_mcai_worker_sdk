use semver::Version;

pub const MINIMUM_BLACK_LIST_STEP_FLOW_VERSION: Version = Version {
  major: 1,
  minor: 6,
  patch: 0,
  pre: vec![],
  build: vec![],
};

#[derive(Clone, Debug, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct BlackList {
  job_ids: Vec<u64>,
}

impl BlackList {
  pub fn contains(&self, job_id: u64) -> bool {
    self.job_ids.contains(&job_id)
  }

  pub fn add(&mut self, job_id: u64) {
    self.job_ids.push(job_id);
  }

  pub fn remove(&mut self, job_id: u64) {
    self.job_ids.retain(|id| *id != job_id);
  }

  pub fn get(&self) -> Vec<u64> {
    self.job_ids.clone()
  }
}

impl From<Vec<u64>> for BlackList {
  fn from(job_ids: Vec<u64>) -> Self {
    Self { job_ids }
  }
}

#[derive(Clone, Debug, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct BlackListChangeMessage {
  job_id: u64,
}

impl BlackListChangeMessage {
  pub fn job_id(&self) -> u64 {
    self.job_id
  }
}
