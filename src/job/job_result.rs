use super::{job_status::JobStatus, Job};
use crate::{
  parameter::{container::ParametersContainer, Parameter, ParameterValue},
  prelude::{Arc, Mutex},
};
use chrono::{DateTime, Utc};
use cpu_time::ProcessTime;
use nvml_wrapper::{error::NvmlError, NVML};
use reqwest::Error;
use serde::Serialize;
use std::{process, time::Instant};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct JobResult {
  destination_paths: Vec<String>,
  execution_duration: Arc<Mutex<f64>>,
  job_id: u64,
  datetime: DateTime<Utc>,
  parameters: Vec<Parameter>,
  #[serde(skip_serializing, skip_deserializing, default = "default_instant")]
  start_instant: Instant,
  #[serde(skip_serializing, skip_deserializing, default = "default_cpu_time")]
  cpu_start_instant: Option<ProcessTime>,
  cpu_time: Arc<Mutex<f64>>,
  gpu_time: f64,
  status: JobStatus,
}

fn default_instant() -> Instant {
  Instant::now()
}

fn default_cpu_time() -> Option<ProcessTime> {
  ProcessTime::try_now().ok()
}

fn get_gpu_total_time() -> Result<f64, NvmlError> {
  let nvml = NVML::init()?;
  let device_count = nvml.device_count()?;
  let mut gpu_total_time: f64 = 0.0;
  for device_index in 0..device_count {
    gpu_total_time += nvml
      .device_by_index(device_index)?
      .accounting_stats_for(process::id())?
      .time as f64
  }
  Ok(gpu_total_time)
}

impl JobResult {
  pub fn new(job_id: u64) -> JobResult {
    JobResult {
      destination_paths: vec![],
      execution_duration: Arc::new(Mutex::new(0.0)),
      job_id,
      datetime: Utc::now(),
      parameters: vec![],
      start_instant: Instant::now(),
      status: JobStatus::default(),
      cpu_start_instant: ProcessTime::try_now().ok(),
      cpu_time: Arc::new(Mutex::new(0.0)),
      gpu_time: get_gpu_total_time().unwrap_or_default(),
    }
  }

  pub fn with_status(mut self, status: JobStatus) -> Self {
    self.update_execution_durations();
    self.status = status;
    self.datetime = Utc::now();
    self
  }

  pub fn with_error(mut self, error: Error) -> Self {
    self.update_execution_durations();
    self.datetime = Utc::now();
    self.parameters.push(Parameter {
      id: "message".to_string(),
      kind: String::get_type_as_string(),
      store: None,
      default: None,
      value: serde_json::to_value(error.to_string()).ok(),
    });
    self
  }

  pub fn with_message(mut self, message: &str) -> Self {
    self.parameters.push(Parameter {
      id: "message".to_string(),
      kind: String::get_type_as_string(),
      store: None,
      default: None,
      value: serde_json::to_value(message.to_string()).ok(),
    });
    self
  }

  pub fn with_parameters(mut self, parameters: &mut Vec<Parameter>) -> Self {
    self.parameters.append(parameters);
    self
  }

  pub fn with_destination_paths(mut self, destination_paths: &mut Vec<String>) -> Self {
    self.destination_paths.append(destination_paths);
    self
  }

  pub fn with_json<T>(mut self, id: &str, serializable: &T) -> Result<Self, String>
  where
    T: Serialize + ParameterValue + Sized,
  {
    self.parameters.push(Parameter {
      id: id.to_string(),
      kind: T::get_type_as_string(),
      store: None,
      default: None,
      value: serde_json::to_value(serializable).ok(),
    });
    Ok(self)
  }

  pub fn get_job_id(&self) -> u64 {
    self.job_id
  }

  pub fn get_str_job_id(&self) -> String {
    self.job_id.to_string()
  }

  pub fn get_status(&self) -> &JobStatus {
    &self.status
  }

  pub fn get_datetime(&self) -> DateTime<Utc> {
    self.datetime
  }

  pub fn get_execution_duration(&self) -> f64 {
    *self.execution_duration.lock().unwrap()
  }

  pub fn get_parameters(&self) -> &Vec<Parameter> {
    &self.parameters
  }

  pub fn get_destination_paths(&self) -> &Vec<String> {
    &self.destination_paths
  }

  pub fn update_execution_durations(&mut self) {
    *self.execution_duration.lock().unwrap() = self.start_instant.elapsed().as_secs_f64();
    *self.cpu_time.lock().unwrap() = self
      .cpu_start_instant
      .map(|start| start.elapsed().as_secs_f64())
      .unwrap_or_default();
    self.gpu_time = get_gpu_total_time().unwrap_or_default() - self.gpu_time;
  }
}

impl From<Job> for JobResult {
  fn from(job: Job) -> JobResult {
    JobResult::new(job.job_id)
  }
}

impl From<&Job> for JobResult {
  fn from(job: &Job) -> JobResult {
    JobResult::new(job.job_id)
  }
}

impl ParametersContainer for JobResult {
  fn get_parameters(&self) -> &Vec<Parameter> {
    &self.parameters
  }
}

impl PartialEq for JobResult {
  fn eq(&self, other: &Self) -> bool {
    self.job_id == other.job_id
      && self.status == other.status
      && self.parameters == other.parameters
      && self.destination_paths == other.destination_paths
  }
}

impl Eq for JobResult {}
