use super::{AuthenticatedClient, HttpError};
use semver::Version;

pub fn get_step_flow_version(client: AuthenticatedClient) -> Result<Version, HttpError> {
  let step_flow_url = format!("{}/step_flow", client.endpoint());

  let response = client
    .client()
    .get(step_flow_url)
    .send()?
    .error_for_status()?;

  let step_flow_version: StepFlowVersion = response.json()?;

  Ok(step_flow_version.version)
}

#[derive(Debug, Clone, Deserialize)]
struct StepFlowVersion {
  #[serde(rename = "application")]
  _application: String,
  version: Version,
}
