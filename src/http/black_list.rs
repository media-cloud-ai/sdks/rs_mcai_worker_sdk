use super::{AuthenticatedClient, HttpError};
use crate::job::BlackList;

const BLACK_LIST_RESOURCE_PATH: &str = "step_flow/blacklist";

pub fn get_black_list(client: AuthenticatedClient) -> Result<BlackList, HttpError> {
  let black_list_url = format!("{}/{}", client.endpoint(), BLACK_LIST_RESOURCE_PATH);

  let response = client
    .client()
    .get(black_list_url)
    .send()?
    .error_for_status()?;

  let content = response.text()?;
  let response: BlackListResponse = serde_json::from_str(&content)?;
  let blacklist = BlackList::from(response.data);

  Ok(blacklist)
}

pub fn delete_job_from_black_list(
  client: AuthenticatedClient,
  job_id: u64,
) -> Result<BlackList, HttpError> {
  let black_list_url = format!(
    "{}/{}/{}",
    client.endpoint(),
    BLACK_LIST_RESOURCE_PATH,
    job_id
  );

  let response = client.client().delete(black_list_url).send()?;

  let content = response.text()?;
  let response: BlackListResponse = serde_json::from_str(&content)?;
  let blacklist = BlackList::from(response.data);

  Ok(blacklist)
}

#[derive(Debug, Clone, Deserialize)]
struct BlackListResponse {
  data: Vec<u64>,
  #[serde(rename = "total")]
  _total: usize,
}
