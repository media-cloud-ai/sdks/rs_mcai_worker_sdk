use super::{AuthenticatedClient, HttpError};
use serde_json::Value;

pub fn get_credential(
  client: AuthenticatedClient,
  credential_key: &str,
) -> Result<Value, HttpError> {
  let credential_url = format!("{}/credentials/{}", client.endpoint(), credential_key);

  let response: ValueResponseBody = client
    .client()
    .get(credential_url)
    .send()?
    .error_for_status()?
    .json()?;

  let value = match response.data.value.clone() {
    Value::String(string) => serde_json::from_str(&string).unwrap_or(response.data.value),
    _ => response.data.value,
  };

  Ok(value)
}

#[derive(Debug, Deserialize)]
pub struct DataResponseBody {
  #[serde(rename = "id")]
  _id: u32,
  #[serde(rename = "key")]
  _key: String,
  pub value: Value,
  #[serde(rename = "inserted_at")]
  _inserted_at: String,
}

#[derive(Debug, Deserialize)]
pub struct ValueResponseBody {
  pub data: DataResponseBody,
}
