mod access_token;
mod black_list;
mod credentials;
mod step_flow;

pub use black_list::{delete_job_from_black_list, get_black_list};
pub use credentials::get_credential;
pub use step_flow::get_step_flow_version;

use access_token::AccessToken;

use reqwest::{
  blocking::Client,
  header::{HeaderMap, HeaderValue, AUTHORIZATION},
};

#[derive(Debug, Clone)]
pub struct AuthenticatedClient {
  client: Client,
  endpoint: String,
}

impl AuthenticatedClient {
  pub fn new(store_code: &str) -> Result<Self, HttpError> {
    let access_token = AccessToken::get_access_token(store_code)?;

    let mut headers = HeaderMap::new();

    headers.insert(
      AUTHORIZATION,
      HeaderValue::from_str(&access_token.token()).map_err(|e| format!("{e:?}"))?,
    );

    let client = Client::builder()
      .default_headers(headers)
      .build()
      .map_err(|e| e.to_string())?;

    Ok(Self {
      client,
      endpoint: access_token.endpoint(),
    })
  }

  pub fn client(&self) -> Client {
    self.client.clone()
  }

  pub fn endpoint(&self) -> &str {
    &self.endpoint
  }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct HttpError {
  cause: String,
}

impl HttpError {
  pub fn new(cause: &str) -> Self {
    Self {
      cause: cause.to_string(),
    }
  }
}

impl From<serde_json::Error> for HttpError {
  fn from(error: serde_json::Error) -> Self {
    Self::new(&error.to_string())
  }
}

impl From<reqwest::Error> for HttpError {
  fn from(error: reqwest::Error) -> Self {
    Self::new(&error.to_string())
  }
}

impl From<String> for HttpError {
  fn from(error: String) -> Self {
    Self::new(&error)
  }
}

impl ToString for HttpError {
  fn to_string(&self) -> String {
    self.cause.clone()
  }
}
