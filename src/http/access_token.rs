use super::HttpError;
use crate::config::{get_store_hostname, get_store_password, get_store_username};
use reqwest::blocking::Client;

#[derive(Debug)]
pub struct AccessToken {
  endpoint: String,
  token: String,
}

impl AccessToken {
  pub fn get_access_token(store_code: &str) -> Result<Self, HttpError> {
    let backend_endpoint = get_store_hostname(store_code);
    let backend_username = get_store_username(store_code);
    let backend_password = get_store_password(store_code);

    let session_url = format!("{backend_endpoint}/sessions");

    let client = Client::builder().build()?;

    let session_body = SessionBody {
      session: Session {
        email: backend_username,
        password: backend_password,
      },
    };

    let response: SessionResponseBody = client
      .post(session_url)
      .json(&session_body)
      .send()?
      .error_for_status()?
      .json()?;

    Ok(Self {
      endpoint: backend_endpoint,
      token: response.access_token,
    })
  }

  pub fn endpoint(&self) -> String {
    self.endpoint.clone()
  }

  pub fn token(&self) -> String {
    self.token.clone()
  }
}

#[derive(Debug, Serialize)]
pub struct Session {
  pub email: String,
  pub password: String,
}

#[derive(Debug, Serialize)]
pub struct SessionBody {
  pub session: Session,
}

#[derive(Debug, Deserialize)]
pub struct SessionResponseBody {
  pub access_token: String,
}
