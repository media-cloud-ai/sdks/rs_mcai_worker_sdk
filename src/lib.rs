//! # MCAI Worker SDK
//!
//! This library is an SDK to communicate via message broker with [StepFlow](https://hexdocs.pm/step_flow/readme.html).
//! It's used for every worker as an abstraction.
//! It manage itself requirements, message parsing, direct messaging.
//!
//! ## Worker implementation
//!
//! 1. Create a Rust project
//! 2. Add MCAI Worker SDK as a dependency in Cargo.toml: `mcai_worker_sdk = "^1.0"`
//! 1. Update the main file with the example provided here to implement [MessageEvent](trait.MessageEvent.html) trait,
//! and call the [`start_worker`](fn.start_worker.html) to start the worker itself.
//!
//! ```rust
//! use mcai_worker_sdk::prelude::*;
//! use serde_derive::Deserialize;
//! use schemars::JsonSchema;
//!
//! #[derive(Debug)]
//! struct WorkerNameEvent {}
//!
//! #[derive(Debug, Deserialize, JsonSchema)]
//! struct WorkerParameters {}
//!
//! // For opensource workers
//! default_rust_mcai_worker_description!();
//! // Or uncomment one of these lines for licensed workers
//! // default_rust_mcai_worker_description!(Private);
//! // default_rust_mcai_worker_description!(Commercial);
//! // You can also specify the name of your organisation
//! // default_rust_mcai_worker_description!("My Organisation", Commercial);
//!
//! impl McaiWorker<WorkerParameters, RustMcaiWorkerDescription> for WorkerNameEvent {
//! }
//! static WORKER_NAME_EVENT: WorkerNameEvent = WorkerNameEvent {};
//!
//! // uncomment it to start the worker
//! // fn main() {
//! //   mcai_worker_sdk::start_worker(&WORKER_NAME_EVENT);
//! // }
//! ```
//!
//! ## Runtime configuration
//!
//! ### AMQP connection
//!
//! |    Variable                 | Description |
//! |-----------------------------|-------------|
//! | `AMQP_HOSTNAME`             | IP or host of AMQP server (default: `localhost`) |
//! | `AMQP_PORT`                 | AMQP server port (default: `5672`) |
//! | `AMQP_TLS`                  | enable secure connection using AMQPS (default: `false`, enable with `true` or `1` or `TRUE` or `True`) |
//! | `AMQP_USERNAME`             | Username used to connect to AMQP server (default: `guest`) |
//! | `AMQP_PASSWORD`             | Password used to connect to AMQP server (default: `guest`) |
//! | `AMQP_VHOST`                | AMQP virtual host (default: `/`) |
//! | `AMQP_QUEUE`                | AMQP queue name used to receive job orders (default: `job_undefined`) |
//! | `AMQP_SERVER_CONFIGURATION` | RabbitMP configuration. Either standalone or cluster (default: `standalone`) |
//!
//! ### Vault connection
//!
//! |    Variable        | Description |
//! |--------------------|-------------|
//! | `BACKEND_HOSTNAME` | URL used to connect to backend server (default: `http://127.0.0.1:4000/api`) |
//! | `BACKEND_USERNAME` | Username used to connect to backend server |
//! | `BACKEND_PASSWORD` | Password used to connect to backend server |
//!
//! ## Start worker locally
//!
//! MCAI Worker SDK can be launched locally - without RabbitMQ.
//! It can process some message for different purpose (functional tests, message order examples, etc.).
//!
//! To start worker in this mode, setup the environment variable `SOURCE_ORDERS` with path(s) to json orders.
//! It can take multiple orders, joined with `:` on unix platform, `;` on windows os.
//!
//! ### Examples:
//!
//! ```bash
//! RUST_LOG=info SOURCE_ORDERS=./examples/success_order.json:./examples/error_order.json cargo run --example worker
//! ```

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[cfg(feature = "media")]
#[macro_use]
extern crate yaserde_derive;

pub mod config;
mod error;
mod http;
pub mod job;
mod logging;
pub mod mcai_worker;
#[cfg(feature = "media")]
pub mod media;
pub mod message_exchange;
pub mod parameter;
pub mod prelude;
#[cfg(feature = "media")]
mod process_result;
pub mod processor;
mod start_worker;
pub mod worker;
pub mod reexport {
  pub use cargo_toml::Package;
  pub use mcai_license;
  pub use semver::Version;
}

pub use error::{MessageError, Result};
pub use message_exchange::message::publish_job_progression;
pub use parameter::container::ParametersContainer;

use message_exchange::WorkerResponseSender;
use processor::Processor;

use std::sync::{Arc, Mutex};

/// Exposed Channel type
pub type McaiChannel = Arc<Mutex<dyn WorkerResponseSender + Send>>;

#[macro_export]
macro_rules! default_rust_mcai_worker_description {
  () => {
    default_rust_mcai_worker_description! {
        None
    }
  };
  (Commercial) => {
    default_rust_mcai_worker_description! {
        None, Commercial
    }
  };
  (Private) => {
    default_rust_mcai_worker_description! {
        None, Private
    }
  };
  ($organisation:literal) => {
    default_rust_mcai_worker_description! {
      Some($organisation.to_string())
    }
  };
  ($organisation:expr) => {
    default_rust_mcai_worker_description! {
      $organisation,
      fn get_license(&self) -> mcai_worker_sdk::reexport::mcai_license::McaiWorkerLicense {
        let license = self
          .package
          .license
          .as_ref()
          .expect("Missing license in Cargo.toml. This field is required with a valid SPDX license for opensource workers.")
          .clone()
          .unwrap();
        McaiWorkerLicense::OpenSource(OpenSourceLicense::new(&license))
      }
    }
  };
  ($organisation:literal, Commercial) => {
    default_rust_mcai_worker_description! {
      Some($organisation.to_string()),
      Commercial
    }
  };
  ($organisation:literal, Private) => {
    default_rust_mcai_worker_description! {
        Some($organisation.to_string()),
        Private
    }
  };
  ($organisation:expr, Commercial) => {
    default_rust_mcai_worker_description! {
        $organisation,
        fn get_license(&self) -> mcai_worker_sdk::reexport::mcai_license::McaiWorkerLicense {
          McaiWorkerLicense::Commercial
        }
    }
  };
  ($organisation:expr, Private) => {
    default_rust_mcai_worker_description! {
        $organisation,
        fn get_license(&self) -> mcai_worker_sdk::reexport::mcai_license::McaiWorkerLicense {
          McaiWorkerLicense::Private
        }
    }
  };

  ($organisation:expr, $($license:tt)*) => {
    pub struct RustMcaiWorkerDescription {
      pub package: mcai_worker_sdk::reexport::Package,
    }

    impl mcai_worker_sdk::mcai_worker::McaiWorkerDescription for RustMcaiWorkerDescription {
      fn get_name(&self) -> String {
        self.package.name.clone()
      }

      fn get_description(&self) -> String {
        if let Some(description) = &self.package.description {
            description.clone().unwrap()
        } else {
            String::new()
        }
      }

      fn get_version(&self) -> String {
        self.package.version.as_ref().unwrap().clone()
      }

      $($license)*

      fn get_authors(&self) -> Vec<String> {
        self.package.authors.as_ref().unwrap().clone()
      }

      fn get_homepage(&self) -> Option<String> {
        if let Some(homepage) = &self.package.homepage {
            Some(homepage.clone().unwrap())
        } else {
            None
        }
      }

      fn get_repository(&self) -> Option<String> {
        if let Some(repository) = &self.package.repository {
            Some(repository.clone().unwrap())
        } else {
            None
        }
      }

      fn get_organisation(&self) -> Option<String> {
        $organisation
      }
    }

    impl Default for RustMcaiWorkerDescription {
      fn default() -> Self {
        Self {
          package: include!(concat!(env!("OUT_DIR"), "/mcai_build.rs")),
        }
      }
    }
  };
}

#[test]
fn empty_worker_impl() {
  use crate::prelude::*;

  #[derive(Debug)]
  struct CustomWorker {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  #[derive(Default)]
  struct Description;

  impl McaiWorkerDescription for Description {
    fn get_name(&self) -> String {
      "Custom".to_string()
    }

    fn get_description(&self) -> String {
      "Description".to_string()
    }

    fn get_version(&self) -> String {
      "1.2.3".to_string()
    }

    fn get_license(&self) -> McaiWorkerLicense {
      McaiWorkerLicense::OpenSource(OpenSourceLicense::new("MIT"))
    }
  }

  impl McaiWorker<CustomParameters, Description> for CustomWorker {
    fn get_mcai_worker_description(&self) -> Box<Description> {
      Box::new(Description)
    }
  }

  let custom_worker = CustomWorker {};
  let parameters = CustomParameters {};

  let job = job::Job {
    job_id: 1234,
    parameters: vec![],
  };

  let job_result = job::JobResult::new(job.job_id);

  let result = custom_worker.process(None, parameters, job_result);
  assert!(result == Err(MessageError::NotImplemented()));
}
