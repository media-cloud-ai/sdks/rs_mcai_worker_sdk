#[cfg(feature = "media")]
mod media_process;
mod process;
mod process_status;
mod simple_process;

pub use process::Process;
pub use process_status::ProcessStatus;

#[cfg(feature = "media")]
use media_process::MediaProcess as ProcessEngine;
#[cfg(not(feature = "media"))]
use simple_process::SimpleProcess as ProcessEngine;

use crate::{
  logging::SharedJobId,
  mcai_worker::{McaiWorker, McaiWorkerDescription},
  message_exchange::{
    message::{OrderMessage, ResponseMessage},
    InternalExchange, ResponseSender,
  },
  worker::WorkerConfiguration,
  MessageError, Result,
};
use async_std::task;
use schemars::JsonSchema;
use serde::de::DeserializeOwned;
use std::{
  sync::{Arc, Mutex},
  thread::spawn,
  time::Duration,
};

pub struct Processor {
  internal_exchange: Arc<Mutex<dyn InternalExchange>>,
  worker_configuration: WorkerConfiguration,
  ctrl_c_handler_enabled: bool,
}

impl Processor {
  pub fn new(
    internal_exchange: Arc<Mutex<dyn InternalExchange>>,
    worker_configuration: WorkerConfiguration,
  ) -> Self {
    Processor {
      internal_exchange,
      worker_configuration,
      ctrl_c_handler_enabled: true,
    }
  }

  pub fn disable_ctrl_c_handler(&mut self) {
    self.ctrl_c_handler_enabled = false;
  }

  pub fn run<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: 'static + McaiWorker<P, D> + Send,
  >(
    self,
    worker: Arc<Mutex<W>>,
    shared_job_id: SharedJobId,
  ) -> Result<()> {
    let (
      order_receiver_from_exchange,
      response_sender_to_exchange,
      worker_response_sender_to_exchange,
    ) = {
      let internal_exchange = self.internal_exchange.lock().unwrap();
      (
        internal_exchange.get_order_receiver(),
        internal_exchange.get_response_sender(),
        internal_exchange.get_worker_response_sender(),
      )
    };

    let cloned_worker_configuration = self.worker_configuration.clone();

    let thread = spawn(move || {
      // Create Simple or Media Process
      let mut process = ProcessEngine::new(
        worker.clone(),
        worker_response_sender_to_exchange,
        cloned_worker_configuration.clone(),
        shared_job_id.clone(),
      );

      response_sender_to_exchange
        .lock()
        .unwrap()
        .send_response(ResponseMessage::WorkerCreated(Box::new(
          cloned_worker_configuration.clone(),
        )))
        .unwrap();

      loop {
        let order_receiver = order_receiver_from_exchange.clone();

        let next_message = task::block_on(async move { order_receiver.lock().await.recv().await });

        if let Ok(message) = next_message {
          log::debug!("Processor received an order message: {:?}", message);

          let current_job_id = process.get_current_job_id(worker.clone());

          if let Err(error) = message.matches_job_id(current_job_id) {
            Self::send_error_response(response_sender_to_exchange.clone(), error).unwrap();
            continue;
          }

          match &message {
            OrderMessage::Job(job)
            | OrderMessage::InitProcess(job)
            | OrderMessage::StartProcess(job) => {
              *shared_job_id.lock().unwrap() = Some(job.job_id);
            }
            _ => {}
          }

          // process the message on the processor
          let response = process.handle(worker.clone(), message.clone());

          // manage errors returned by the processor
          if let Err(error) = response {
            Self::send_error_response(response_sender_to_exchange.clone(), error).unwrap();
          }

          match &message {
            OrderMessage::StopProcess(_) | OrderMessage::ReachedExpiration(_) => {
              *shared_job_id.lock().unwrap() = None;
            }
            _ => {}
          }

          if message == OrderMessage::StopWorker {
            log::info!("Worker stop order: send a worker terminated message...");
            response_sender_to_exchange
              .lock()
              .unwrap()
              .send_response(ResponseMessage::WorkerTerminated(Box::new(
                cloned_worker_configuration.clone(),
              )))
              .unwrap();

            // Wait a bit for the message to be sent
            std::thread::sleep(Duration::from_millis(100));

            // And then shut down properly...
            std::process::exit(0);
          }
        }
      }
    });

    self.declare_ctrl_c_handler()?;

    if let Some(error) = thread.join().unwrap() {
      Err(error)
    } else {
      Ok(())
    }
  }

  fn send_error_response(
    response_sender_to_exchange: Arc<Mutex<dyn ResponseSender>>,
    error: MessageError,
  ) -> Result<()> {
    let response = ResponseMessage::Error(error);
    log::debug!(
      "Processor send the process response message: {:?}",
      response
    );

    response_sender_to_exchange
      .lock()
      .unwrap()
      .send_response(response)?;

    log::debug!("Process response message sent!");

    Ok(())
  }

  fn declare_ctrl_c_handler(&self) -> Result<()> {
    if !self.ctrl_c_handler_enabled {
      return Ok(());
    }

    let cloned_worker_configuration = self.worker_configuration.clone();
    let response_sender_to_exchange = self.internal_exchange.lock().unwrap().get_response_sender();

    ctrlc::set_handler(move || {
      log::info!("Worker program stopped: send a worker terminated message...");
      response_sender_to_exchange
        .lock()
        .unwrap()
        .send_response(ResponseMessage::WorkerTerminated(Box::new(
          cloned_worker_configuration.clone(),
        )))
        .unwrap();

      // Wait a bit for the message to be sent
      std::thread::sleep(Duration::from_millis(100));

      // And then shut down properly...
      std::process::exit(0);
    })
    .map_err(|error| {
      MessageError::RuntimeError(format!("Could not set Ctrl-C handler: {error}"))
    })?;

    Ok(())
  }
}
