use super::{Process, ProcessStatus};
use crate::{
  job::{Job, JobProgression, JobResult, JobStatus},
  logging::SharedJobId,
  mcai_worker::{McaiWorker, McaiWorkerDescription},
  message_exchange::message::{Feedback, OrderMessage, ResponseMessage},
  worker::{WorkerActivity, WorkerBlackList, WorkerConfiguration, WorkerStatus},
  McaiChannel, MessageError, Result,
};
use schemars::JsonSchema;
use serde::de::DeserializeOwned;
use std::{
  sync::{Arc, Mutex},
  thread::spawn,
};

pub struct SimpleProcess {
  response_sender: McaiChannel,
  worker_activity: Arc<Mutex<WorkerActivity>>,
  job_status: Arc<Mutex<JobStatus>>,
  worker_configuration: WorkerConfiguration,
  current_job_result: Arc<Mutex<Option<JobResult>>>,
  shared_job_id: SharedJobId,
}

impl<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: 'static + McaiWorker<P, D> + Send,
  > Process<P, D, W> for SimpleProcess
{
  fn new(
    _worker: Arc<Mutex<W>>,
    response_sender: McaiChannel,
    worker_configuration: WorkerConfiguration,
    shared_job_id: SharedJobId,
  ) -> Self {
    SimpleProcess {
      response_sender,
      worker_activity: Arc::new(Mutex::new(WorkerActivity::Idle)),
      job_status: Arc::new(Mutex::new(JobStatus::Unknown)),
      worker_configuration,
      current_job_result: Arc::new(Mutex::new(None)),
      shared_job_id,
    }
  }

  fn handle(&mut self, worker: Arc<Mutex<W>>, order_message: OrderMessage) -> Result<()> {
    let response = match order_message {
      OrderMessage::Job(job) => {
        if let Err(err) = job.check_requirements() {
          Some(ResponseMessage::Error(err))
        } else {
          self.update_job(&job);
          self.update_status(Some(JobStatus::Processing), None);
          self.execute(worker, &job)?;
          None
        }
      }
      OrderMessage::InitProcess(job) => {
        self.update_status(Some(JobStatus::ReadyToStart), None);
        self.update_job(&job);

        Some(ResponseMessage::WorkerInitialized(
          JobResult::new(job.job_id).with_status(JobStatus::ReadyToStart),
        ))
      }
      OrderMessage::StartProcess(job) => {
        if let Err(err) = job.check_requirements() {
          Some(ResponseMessage::Error(err))
        } else {
          self.update_status(Some(JobStatus::Processing), None);

          let job_result = self.current_job_result.lock().unwrap().clone().unwrap();
          let worker_started =
            ResponseMessage::WorkerStarted(job_result.with_status(JobStatus::Processing));

          self
            .response_sender
            .lock()
            .unwrap()
            .send_response(worker_started)
            .unwrap();

          self.execute(worker, &job)?;

          None
        }
      }
      // Nothing to do here to update the current job
      OrderMessage::UpdateProcess(_job) => None,
      // Nothing to do here to stop the current job
      OrderMessage::StopProcess(_job) => None,
      OrderMessage::Status | OrderMessage::StopWorker => Some(ResponseMessage::Feedback(
        Feedback::Status(self.get_process_status()),
      )),
      OrderMessage::StopConsumingJobs => {
        self.update_status(None, Some(WorkerActivity::Suspended));
        Some(ResponseMessage::Feedback(Feedback::Status(
          self.get_process_status(),
        )))
      }
      OrderMessage::ResumeConsumingJobs => {
        self.update_status(None, Some(WorkerActivity::Idle));
        Some(ResponseMessage::Feedback(Feedback::Status(
          self.get_process_status(),
        )))
      }
      OrderMessage::ReachedExpiration(job) => {
        self.update_status(Some(JobStatus::Error), None);
        Some(ResponseMessage::Error(MessageError::ProcessingError(
          JobResult::new(job.job_id)
            .with_status(JobStatus::Error)
            .with_message("Job TTL has been reached."),
        )))
      }
      OrderMessage::GetBlackList => Some(ResponseMessage::Feedback(Feedback::BlackList(
        WorkerBlackList::new(self.worker_configuration.get_worker_description()),
      ))),
      // Nothing to do here with a black list message
      OrderMessage::AddToBlackList(_)
      | OrderMessage::DeleteFromBlackList(_)
      | OrderMessage::SyncBlackList => None,
    };

    if let Some(response) = response {
      self
        .response_sender
        .lock()
        .unwrap()
        .send_response(response)?;
    }
    Ok(())
  }

  fn get_current_job_id(&self, _worker: Arc<Mutex<W>>) -> Option<u64> {
    self
      .current_job_result
      .lock()
      .unwrap()
      .clone()
      .map(|job_result| job_result.get_job_id())
  }
}

impl SimpleProcess {
  fn update_job(&mut self, job: &Job) {
    *self.current_job_result.lock().unwrap() = Some(JobResult::new(job.job_id));
  }

  fn update_status(
    &mut self,
    job_status: Option<JobStatus>,
    worker_activity: Option<WorkerActivity>,
  ) {
    match (job_status, worker_activity) {
      (Some(status), Some(activity)) => {
        *self.job_status.lock().unwrap() = status;
        *self.worker_activity.lock().unwrap() = activity;
      }
      (Some(status), None) => {
        *self.job_status.lock().unwrap() = status;

        // Update worker activity if not suspended
        if *self.worker_activity.lock().unwrap() != WorkerActivity::Suspended {
          *self.worker_activity.lock().unwrap() = self.job_status.lock().unwrap().clone().into();
        }
      }
      (None, Some(activity)) => {
        *self.worker_activity.lock().unwrap() = activity;
      }
      (None, None) => {}
    }
  }

  fn get_process_status(&self) -> ProcessStatus {
    let status = self.job_status.lock().unwrap().clone();
    let current_job_result = self
      .current_job_result
      .lock()
      .unwrap()
      .clone()
      .map(|job_result| job_result.with_status(status.clone()));

    let worker_activity = self.worker_activity.lock().unwrap().clone();

    // Check whether the worker activity is coherent with the job status
    let worker_activity = match worker_activity {
      WorkerActivity::Idle | WorkerActivity::Busy => WorkerActivity::from(status),
      WorkerActivity::Suspended => worker_activity,
    };

    let worker_status = WorkerStatus::new(worker_activity, self.worker_configuration.clone());
    ProcessStatus::new(worker_status, current_job_result)
  }

  fn execute<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: 'static + McaiWorker<P, D> + Send,
  >(
    &mut self,
    worker: Arc<Mutex<W>>,
    job: &Job,
  ) -> Result<()> {
    if self.get_current_job_id(worker.clone()) != Some(job.job_id) {
      return Err(MessageError::RuntimeError(format!(
        "Cannot execute job {} process, since processor has been initialized with: {:?}",
        job.job_id,
        self.current_job_result.lock().unwrap()
      )));
    }

    let response_sender = self.response_sender.clone();
    let status = self.job_status.clone();
    let current_job_result = self.current_job_result.clone();
    let job = job.clone();

    let cloned_shared_job_id = self.shared_job_id.clone();

    spawn(move || {
      let job_id = job.job_id;

      let job_result = current_job_result.lock().unwrap().clone().unwrap();

      let response = match job.get_parameters() {
        Ok(parameters) => {
          // start publishing progression
          let feedback =
            ResponseMessage::Feedback(Feedback::Progression(JobProgression::new(job_id, 0)));

          response_sender
            .lock()
            .unwrap()
            .send_response(feedback)
            .unwrap();

          worker
            .lock()
            .unwrap()
            .process(Some(response_sender.clone()), parameters, job_result)
        }
        Err(error) => Err(error),
      };

      let response = if response_sender.lock().unwrap().is_stopped() {
        response
          .map(ResponseMessage::JobStopped)
          .unwrap_or_else(ResponseMessage::Error)
      } else {
        response
          .map(ResponseMessage::Completed)
          .unwrap_or_else(ResponseMessage::Error)
      };

      *status.lock().unwrap() = response.clone().into();

      *current_job_result.lock().unwrap() = None;

      response_sender
        .lock()
        .unwrap()
        .send_response(response)
        .unwrap();

      *cloned_shared_job_id.lock().unwrap() = None;
    });

    Ok(())
  }
}

impl Drop for SimpleProcess {
  fn drop(&mut self) {
    log::info!("Simple process dropped with status: {:?}", self.job_status);
  }
}
