use super::{buffers_of_frames::BuffersOfFrames, ProcessStatus};
use crate::{
  config::get_ffmpeg_waitmore_limit,
  error::MessageError::RuntimeError,
  job::{Job, JobProgression, JobResult, JobStatus},
  mcai_worker::{McaiWorker, McaiWorkerDescription},
  media::{
    decoder::DecodeResult, finish_process, initialize_process, output::Output, source::Source,
  },
  message_exchange::message::{Feedback, OrderMessage, ResponseMessage},
  publish_job_progression,
  worker::{WorkerActivity, WorkerConfiguration, WorkerStatus},
  McaiChannel, MessageError, Result,
};
use schemars::JsonSchema;
use serde::de::DeserializeOwned;
use std::sync::{mpsc::Receiver, Arc, Mutex};

pub struct ThreadedMediaProcess {
  source: Source,
  output: Output,
  keep_running: bool,
  pub job: Job,
  batch_size: u32,
  worker_activity: Arc<Mutex<WorkerActivity>>,
  pub job_result: JobResult,
  pub wait_more_timeout: i64,
  is_av_decoder: bool,
}

impl ThreadedMediaProcess {
  pub fn initialize_process<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: McaiWorker<P, D> + Send,
  >(
    worker: Arc<Mutex<W>>,
    job: Job,
    worker_activity: Arc<Mutex<WorkerActivity>>,
  ) -> Result<Self> {
    log::info!("Initialize job: {:?}", job);

    let job_result = JobResult::new(job.job_id);
    initialize_process(worker, &job).map(|(source, output, batch_size)| ThreadedMediaProcess {
      source,
      output,
      keep_running: false,
      job,
      batch_size,
      worker_activity,
      job_result,
      wait_more_timeout: 0,
      is_av_decoder: true,
    })
  }

  pub fn get_status_feedback(
    &self,
    status: JobStatus,
    worker_configuration: WorkerConfiguration,
  ) -> ResponseMessage {
    ResponseMessage::Feedback(Feedback::Status(
      self.get_process_status(status, worker_configuration),
    ))
  }

  fn get_process_status(
    &self,
    status: JobStatus,
    worker_configuration: WorkerConfiguration,
  ) -> ProcessStatus {
    let job_result = self.job_result.clone().with_status(status.clone());

    let worker_activity = self.worker_activity.lock().unwrap().clone();

    // Check whether the worker activity is coherent with the job status
    let worker_activity = match worker_activity {
      WorkerActivity::Idle | WorkerActivity::Busy => WorkerActivity::from(status),
      WorkerActivity::Suspended => worker_activity,
    };

    let worker_status = WorkerStatus::new(worker_activity, worker_configuration);
    ProcessStatus::new(worker_status, Some(job_result))
  }

  pub fn start_process<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: McaiWorker<P, D> + Send,
  >(
    &mut self,
    worker: Arc<Mutex<W>>,
    order_receiver: &Receiver<OrderMessage>,
    response_sender: McaiChannel,
    worker_configuration: WorkerConfiguration,
  ) -> ResponseMessage {
    let job = self.job.clone();
    self.job_result = self.job_result.clone().with_status(JobStatus::Processing);

    self.is_av_decoder = self.source.is_av_decoder();

    log::info!("is_av_decoder: {:?}", self.is_av_decoder);

    let mut buffers_frames = BuffersOfFrames::new(
      self.batch_size,
      &self.source.decoders.keys().collect::<Vec<&usize>>(),
    );

    response_sender
      .lock()
      .unwrap()
      .send_response(ResponseMessage::WorkerStarted(self.job_result.clone()))
      .unwrap();

    log::info!("Start processing job: {:?}", job);

    // start publishing progression
    response_sender
      .lock()
      .unwrap()
      .send_response(ResponseMessage::Feedback(Feedback::Progression(
        JobProgression::new(job.job_id, 0),
      )))
      .unwrap();

    log::info!(
      "{} - Start to process media (start: {} ms, duration: {})",
      self.job_result.get_str_job_id(),
      self.source.get_start_offset(),
      self
        .source
        .get_segment_duration()
        .map(|duration| format!("{duration} ms"))
        .unwrap_or_else(|| "unknown".to_string())
    );

    let mut processed_frames = 0;
    let mut previous_progress = 0;

    loop {
      if response_sender.lock().unwrap().is_stopped() {
        log::info!("Stopped !");
        break ResponseMessage::JobStopped(self.job_result.clone().with_status(JobStatus::Stopped));
      }

      // Process next batch of frames
      let response = self
        .process_frames(
          worker.clone(),
          response_sender.clone(),
          &mut processed_frames,
          &mut previous_progress,
          &mut buffers_frames,
        )
        .unwrap_or_else(|error| Some(ResponseMessage::Error(error)));

      // If a message is returned, stop looping and forward the message
      if let Some(message) = response {
        break message;
      }

      // Otherwise check whether an order message as been sent to this thread
      if let Ok(message) = order_receiver.try_recv() {
        let resp = match message {
          OrderMessage::Job(_) => ResponseMessage::Error(MessageError::ProcessingError(
            self
              .job_result
              .clone()
              .with_status(JobStatus::Processing)
              .with_message("Cannot handle a job while a process is running"),
          )),
          OrderMessage::InitProcess(_) => ResponseMessage::Error(MessageError::ProcessingError(
            self
              .job_result
              .clone()
              .with_status(JobStatus::Processing)
              .with_message("Cannot initialize a running process"),
          )),
          OrderMessage::StartProcess(_) => ResponseMessage::Error(MessageError::ProcessingError(
            self
              .job_result
              .clone()
              .with_status(JobStatus::Processing)
              .with_message("Cannot start a running process"),
          )),
          OrderMessage::UpdateProcess(job) => self.update_process(worker.clone(), job),
          OrderMessage::StopProcess(_) => {
            // Stop the process properly
            break finish_process(worker, &mut self.output, self.job_result.clone())
              .map(ResponseMessage::JobStopped)
              .unwrap_or_else(ResponseMessage::Error);
          }
          OrderMessage::Status => {
            self.get_status_feedback(JobStatus::Processing, worker_configuration.clone())
          }
          OrderMessage::StopWorker => {
            // Kill the process
            self.keep_running = false;
            self.get_status_feedback(JobStatus::Stopped, worker_configuration.clone())
          }
          OrderMessage::StopConsumingJobs => {
            // If this message comes here, it means a process is running
            *self.worker_activity.lock().unwrap() = WorkerActivity::Suspended;
            self.get_status_feedback(JobStatus::Processing, worker_configuration.clone())
          }
          OrderMessage::ResumeConsumingJobs => {
            // If this message comes here, it means a process is running
            *self.worker_activity.lock().unwrap() = WorkerActivity::Busy;
            self.get_status_feedback(JobStatus::Processing, worker_configuration.clone())
          }
          OrderMessage::ReachedExpiration(job) => {
            ResponseMessage::Error(MessageError::ProcessingError(
              JobResult::new(job.job_id)
                .with_status(JobStatus::Error)
                .with_message("Job TTL has been reached."),
            ))
          }
          // Nothing to do here with a black list message
          OrderMessage::AddToBlackList(_)
          | OrderMessage::DeleteFromBlackList(_)
          | OrderMessage::GetBlackList
          | OrderMessage::SyncBlackList => ResponseMessage::Error(MessageError::RuntimeError(
            "Processors should not handle black list messages".to_string(),
          )),
        };

        response_sender.lock().unwrap().send_response(resp).unwrap();
      }
    }
  }

  pub fn update_process<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: McaiWorker<P, D> + Send,
  >(
    &mut self,
    worker: Arc<Mutex<W>>,
    job: Job,
  ) -> ResponseMessage {
    let parameters = job.get_parameters().unwrap();

    log::info!("Start updating job: {:?}", job);

    // Update process
    let result = worker.lock().unwrap().update_process(parameters);

    if let Err(e) = result {
      return ResponseMessage::WorkerUpdated(
        JobResult::new(job.job_id)
          .with_status(JobStatus::Error)
          .with_message(&format!("Update error: {e:?}")),
      );
    }

    let job_result = self.job_result.clone();
    ResponseMessage::WorkerUpdated(job_result.with_status(JobStatus::Updated))
  }

  pub fn process_frames<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: McaiWorker<P, D> + Send,
  >(
    &mut self,
    worker: Arc<Mutex<W>>,
    response_sender: McaiChannel,
    processed_frames: &mut usize,
    previous_progress: &mut u8,
    buffers_frames: &mut BuffersOfFrames,
  ) -> Result<Option<ResponseMessage>> {
    // Loop until enough frames (ie. batch_size) have been accumulated in buffers to launch the process_frames
    // Frames are differenciated by stream, and stored in different buffers
    let response = loop {
      match self.source.next_frame() {
        Ok(decode_result) => match decode_result {
          DecodeResult::Frame {
            stream_index,
            frame,
          } => {
            self.wait_more_timeout = 0;
            if stream_index == self.source.get_first_stream_index() {
              (*processed_frames) += 1;
            }

            buffers_frames.push(stream_index, frame);

            // If the buffer is full, send the frames to process_frames and break to send a response message
            if let Some(frames) = buffers_frames.get_full_buffer(stream_index) {
              if let Err(message) = crate::media::process_frames(
                worker,
                &mut self.output,
                self.job_result.clone(),
                stream_index,
                &frames,
              ) {
                return Ok(Some(ResponseMessage::Error(message)));
              } else {
                break None;
              }
            };
          }
          DecodeResult::WaitMore => {
            if self.is_av_decoder {
              if self.wait_more_timeout > get_ffmpeg_waitmore_limit() {
                log::info!("Stopped due to stream not decoding for too long !");
                self.keep_running = false;
                return Ok(Some(ResponseMessage::Error(MessageError::ProcessingError(
                  JobResult::new(self.job.job_id)
                    .with_status(JobStatus::Error)
                    .with_message("FFMPEG can't decode the stream further.."),
                ))));
              }

              self.wait_more_timeout += 1;
              break None;
            }
          }
          DecodeResult::Nothing => break None,
          DecodeResult::ConnectionClosed => {
            log::warn!("Connection was unexpectedly closed. Stopping process.");
            return Ok(Some(ResponseMessage::JobStopped(
              self.job_result.clone().with_status(JobStatus::Stopped),
            )));
          }
          DecodeResult::EndOfStream => {
            // The buffers content has to be processed before ending the process
            for (stream_index, frames) in buffers_frames.buffers.iter() {
              if let Err(message) = crate::media::process_frames(
                worker.clone(),
                &mut self.output,
                self.job_result.clone(),
                *stream_index,
                frames,
              ) {
                return Ok(Some(ResponseMessage::Error(message)));
              }
            }

            log::info!("Media Process: End Of Stream");
            let response = finish_process(worker, &mut self.output, self.job_result.clone())
              .map(ResponseMessage::Completed)?;

            break Some(response);
          }
        },
        Err(error) => match error {
          RuntimeError(message) => {
            return Ok(Some(ResponseMessage::Error(MessageError::ProcessingError(
              JobResult::new(self.job.job_id)
                .with_status(JobStatus::Error)
                .with_message(&message),
            ))))
          }
          _ => {
            return Ok(Some(ResponseMessage::Error(MessageError::ProcessingError(
              JobResult::new(self.job.job_id)
                .with_status(JobStatus::Error)
                .with_message("Something wrong happened."),
            ))))
          }
        },
      };
    };

    let first_stream_fps = self
      .source
      .get_stream_fps(self.source.get_first_stream_index()) as f32;
    let processed_ms = (*processed_frames) as f32 * 1000.0 / first_stream_fps;

    if let Some(duration) = self.source.get_segment_duration() {
      let progress = std::cmp::min((processed_ms / duration as f32 * 100.0) as u8, 100);
      if progress > (*previous_progress) {
        publish_job_progression(
          Some(response_sender),
          self.job_result.get_job_id(),
          progress,
        )?;
        (*previous_progress) = progress;
      }
    }

    if ((*processed_frames) as u32 / self.batch_size) % 10 == 0 {
      log::trace!(
        "{} - Process frames {}",
        self.job_result.get_str_job_id(),
        processed_frames
      );
    };
    Ok(response)
  }
}
