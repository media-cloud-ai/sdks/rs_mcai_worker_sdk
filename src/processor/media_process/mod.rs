mod buffers_of_frames;
mod threaded_media_process;

use threaded_media_process::ThreadedMediaProcess;

use super::{Process, ProcessStatus};
use crate::{
  job::{JobResult, JobStatus},
  logging::SharedJobId,
  mcai_worker::{McaiWorker, McaiWorkerDescription},
  message_exchange::message::{Feedback, OrderMessage, ResponseMessage},
  worker::{WorkerActivity, WorkerBlackList, WorkerConfiguration, WorkerStatus},
  McaiChannel, MessageError, Result,
};
use schemars::JsonSchema;
use serde::de::DeserializeOwned;
use std::{
  cell::RefCell,
  rc::Rc,
  sync::{
    mpsc::{channel, Sender},
    Arc, Mutex,
  },
};

pub struct MediaProcess {
  order_sender: Sender<OrderMessage>,
  current_job_result: Arc<Mutex<Option<JobResult>>>,
}

impl<
    P: DeserializeOwned + JsonSchema,
    D: McaiWorkerDescription,
    W: 'static + McaiWorker<P, D> + Send,
  > Process<P, D, W> for MediaProcess
{
  fn new(
    worker: Arc<Mutex<W>>,
    response_sender: McaiChannel,
    worker_configuration: WorkerConfiguration,
    shared_job_id: SharedJobId,
  ) -> Self {
    let (order_sender, order_receiver) = channel();

    let worker_activity = Arc::new(Mutex::new(WorkerActivity::Idle));
    let job_status = Arc::new(Mutex::new(JobStatus::Unknown));
    let current_job_result = Arc::new(Mutex::new(None));
    let cloned_current_job_result = current_job_result.clone();

    std::thread::spawn(move || {
      let mut threaded_media_process: Option<Rc<RefCell<ThreadedMediaProcess>>> = None;

      let mut keep_running = true;

      // let mut received = order_receiver.recv();

      while let Ok(message) = &order_receiver.recv() {
        // Process the received order message
        let response = match message {
          OrderMessage::Job(job) => {
            if let Err(error) = job.check_requirements() {
              ResponseMessage::Error(error)
            } else {
              log::info!("Process job: {:?}", job);
              let initialization_result = ThreadedMediaProcess::initialize_process(
                worker.clone(),
                job.clone(),
                worker_activity.clone(),
              );

              match initialization_result {
                Ok(initialization_result) => {
                  // Normal initialization case
                  threaded_media_process = Some(Rc::new(RefCell::new(initialization_result)));

                  *current_job_result.lock().unwrap() = Some(
                    threaded_media_process
                      .clone()
                      .unwrap()
                      .borrow()
                      .job_result
                      .clone(),
                  );

                  // TODO send worker response Initialized
                  Self::update_status(
                    job_status.clone(),
                    worker_activity.clone(),
                    Some(JobStatus::Processing),
                    None,
                  );

                  let response = threaded_media_process
                    .clone()
                    .unwrap()
                    .borrow_mut()
                    .start_process(
                      worker.clone(),
                      &order_receiver,
                      response_sender.clone(),
                      worker_configuration.clone(),
                    );

                  log::debug!("Finished response: {:?}", response);

                  let status = Self::get_job_status_from_response(
                    response.clone(),
                    job_status.lock().unwrap().clone(),
                  );

                  Self::update_status(
                    job_status.clone(),
                    worker_activity.clone(),
                    Some(status),
                    None,
                  );

                  *current_job_result.lock().unwrap() = None;

                  response
                }
                Err(initialization_error) => Self::update_status_initialization_error(
                  initialization_error,
                  job.job_id,
                  job_status.clone(),
                  worker_activity.clone(),
                ),
              }
            }
          }
          OrderMessage::InitProcess(job) => {
            if let Err(error) = job.check_requirements() {
              ResponseMessage::Error(error)
            } else {
              let initialization_result = ThreadedMediaProcess::initialize_process(
                worker.clone(),
                job.clone(),
                worker_activity.clone(),
              );

              match initialization_result {
                Ok(initialization_result) => {
                  // Normal initialization case
                  Self::update_status(
                    job_status.clone(),
                    worker_activity.clone(),
                    Some(JobStatus::ReadyToStart),
                    None,
                  );

                  threaded_media_process = Some(Rc::new(RefCell::new(initialization_result)));

                  *current_job_result.lock().unwrap() = Some(
                    threaded_media_process
                      .clone()
                      .unwrap()
                      .borrow()
                      .job_result
                      .clone(),
                  );

                  ResponseMessage::WorkerInitialized(
                    JobResult::new(job.job_id).with_status(JobStatus::ReadyToStart),
                  )
                }
                Err(initialization_error) => Self::update_status_initialization_error(
                  initialization_error,
                  job.job_id,
                  job_status.clone(),
                  worker_activity.clone(),
                ),
              }
            }
          }
          OrderMessage::StartProcess(job) => {
            Self::update_status(
              job_status.clone(),
              worker_activity.clone(),
              Some(JobStatus::Processing),
              None,
            );

            let response = if let Some(media_process) = &threaded_media_process {
              media_process.borrow_mut().start_process(
                worker.clone(),
                &order_receiver,
                response_sender.clone(),
                worker_configuration.clone(),
              )
            } else {
              ResponseMessage::Error(MessageError::ProcessingError(
                JobResult::new(job.job_id)
                  .with_status(JobStatus::Error)
                  .with_message("Process cannot be started, it must be initialized before!"),
              ))
            };

            log::debug!("Finished response: {:?}", response);

            let status = Self::get_job_status_from_response(
              response.clone(),
              job_status.lock().unwrap().clone(),
            );

            Self::update_status(
              job_status.clone(),
              worker_activity.clone(),
              Some(status),
              None,
            );

            response
          }
          OrderMessage::UpdateProcess(job) => {
            ResponseMessage::Error(MessageError::ProcessingError(
              JobResult::new(job.job_id)
                .with_status(JobStatus::Error)
                .with_message("Cannot update a non-running job."),
            ))
          }
          OrderMessage::StopProcess(job) => ResponseMessage::Error(MessageError::ProcessingError(
            JobResult::new(job.job_id)
              .with_status(JobStatus::Error)
              .with_message("Cannot stop a non-running job."),
          )),
          OrderMessage::Status => Self::get_status_feedback(
            current_job_result.lock().unwrap().clone(),
            job_status.lock().unwrap().clone(),
            worker_activity.lock().unwrap().clone(),
            worker_configuration.clone(),
          ),
          OrderMessage::StopWorker => {
            keep_running = false;
            Self::get_status_feedback(
              current_job_result.lock().unwrap().clone(),
              job_status.lock().unwrap().clone(),
              worker_activity.lock().unwrap().clone(),
              worker_configuration.clone(),
            )
          }
          OrderMessage::StopConsumingJobs => {
            Self::update_status(
              job_status.clone(),
              worker_activity.clone(),
              None,
              Some(WorkerActivity::Suspended),
            );
            Self::get_status_feedback(
              current_job_result.lock().unwrap().clone(),
              job_status.lock().unwrap().clone(),
              worker_activity.lock().unwrap().clone(),
              worker_configuration.clone(),
            )
          }
          OrderMessage::ResumeConsumingJobs => {
            Self::update_status(
              job_status.clone(),
              worker_activity.clone(),
              None,
              Some(WorkerActivity::Idle),
            );
            Self::get_status_feedback(
              current_job_result.lock().unwrap().clone(),
              job_status.lock().unwrap().clone(),
              worker_activity.lock().unwrap().clone(),
              worker_configuration.clone(),
            )
          }
          OrderMessage::ReachedExpiration(job) => {
            Self::update_status(
              Arc::new(Mutex::new(JobStatus::Error)),
              worker_activity.clone(),
              None,
              None,
            );
            ResponseMessage::Error(MessageError::ProcessingError(
              JobResult::new(job.job_id)
                .with_status(JobStatus::Error)
                .with_message("Job TTL has been reached."),
            ))
          }
          OrderMessage::GetBlackList => ResponseMessage::Feedback(Feedback::BlackList(
            WorkerBlackList::new(worker_configuration.get_worker_description()),
          )),
          // Nothing to do here with a black list message
          OrderMessage::AddToBlackList(_)
          | OrderMessage::DeleteFromBlackList(_)
          | OrderMessage::SyncBlackList => ResponseMessage::Error(MessageError::RuntimeError(
            "Processors should not handle black list messages".to_string(),
          )),
        };

        match response {
          ResponseMessage::Completed(_)
          | ResponseMessage::Error(_)
          | ResponseMessage::JobStopped(_) => {
            *shared_job_id.lock().unwrap() = None;
            *current_job_result.lock().unwrap() = None;
          }
          _ => {}
        }

        // Send the action response

        response_sender
          .lock()
          .unwrap()
          .send_response(response)
          .unwrap();

        log::trace!("Send the action response message");

        // If the process is stopped, stop looping
        if !keep_running {
          break;
        }
      }
    });

    MediaProcess {
      order_sender,
      current_job_result: cloned_current_job_result,
    }
  }

  fn handle(&mut self, _worker: Arc<Mutex<W>>, order_message: OrderMessage) -> Result<()> {
    if let Err(error) = self.order_sender.send(order_message) {
      return Err(MessageError::RuntimeError(error.to_string())); // TODO use ProcessError
    }
    Ok(())
  }

  fn get_current_job_id(&self, _worker: Arc<Mutex<W>>) -> Option<u64> {
    self
      .current_job_result
      .lock()
      .unwrap()
      .clone()
      .map(|job_result| job_result.get_job_id())
  }
}

impl MediaProcess {
  fn update_status(
    current_job_status: Arc<Mutex<JobStatus>>,
    current_worker_activity: Arc<Mutex<WorkerActivity>>,
    job_status: Option<JobStatus>,
    worker_activity: Option<WorkerActivity>,
  ) {
    match (job_status, worker_activity) {
      (Some(status), Some(activity)) => {
        *current_job_status.lock().unwrap() = status;
        *current_worker_activity.lock().unwrap() = activity;
      }
      (Some(status), None) => {
        *current_job_status.lock().unwrap() = status;

        // Update worker activity if not suspended
        if *current_worker_activity.lock().unwrap() != WorkerActivity::Suspended {
          *current_worker_activity.lock().unwrap() =
            current_job_status.lock().unwrap().clone().into();
        }
      }
      (None, Some(activity)) => {
        *current_worker_activity.lock().unwrap() = activity;
      }
      (None, None) => {}
    }
  }

  fn get_job_status_from_response(response: ResponseMessage, default: JobStatus) -> JobStatus {
    match response {
      ResponseMessage::Completed(_) => JobStatus::Completed,
      ResponseMessage::JobStopped(_) => JobStatus::Stopped,
      ResponseMessage::Error(_) => JobStatus::Error,
      ResponseMessage::Feedback(_)
      | ResponseMessage::StatusError(_)
      | ResponseMessage::WorkerCreated(_)
      | ResponseMessage::WorkerInitialized(_)
      | ResponseMessage::WorkerStarted(_)
      | ResponseMessage::WorkerUpdated(_)
      | ResponseMessage::WorkerTerminated(_) => {
        log::error!(
          "Should not have to handle such a message as a processing response: {:?}",
          response
        );
        default
      }
    }
  }

  fn get_status_feedback(
    job_result: Option<JobResult>,
    job_status: JobStatus,
    worker_activity: WorkerActivity,
    worker_configuration: WorkerConfiguration,
  ) -> ResponseMessage {
    let job_result = job_result.map(|job_result| job_result.with_status(job_status.clone()));

    // Check whether the worker activity is coherent with the job status
    let worker_activity = match worker_activity {
      WorkerActivity::Idle | WorkerActivity::Busy => WorkerActivity::from(job_status),
      WorkerActivity::Suspended => worker_activity,
    };

    ResponseMessage::Feedback(Feedback::Status(ProcessStatus::new(
      WorkerStatus::new(worker_activity, worker_configuration),
      job_result,
    )))
  }

  fn update_status_initialization_error(
    initialization_result: MessageError,
    job_id: u64,
    job_status: Arc<Mutex<JobStatus>>,
    worker_activity: Arc<Mutex<WorkerActivity>>,
  ) -> ResponseMessage {
    // Normal initialization error case
    Self::update_status(job_status, worker_activity, Some(JobStatus::Error), None);
    ResponseMessage::Error(MessageError::ProcessingError(
      JobResult::new(job_id)
        .with_status(JobStatus::Error)
        .with_message(&initialization_result.to_string()),
    ))
  }
}
