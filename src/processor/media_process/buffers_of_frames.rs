use crate::media::process_frame::ProcessFrame;
use std::{collections::HashMap, iter::FromIterator};

pub struct BuffersOfFrames {
  pub buffers: HashMap<usize, Vec<ProcessFrame>>,
  batch_size: u32,
}

impl BuffersOfFrames {
  pub fn new(batch_size: u32, stream_indexes: &[&usize]) -> Self {
    let buffers = HashMap::from_iter(
      stream_indexes
        .iter()
        .map(|&stream_index| {
          (
            *stream_index,
            Vec::<ProcessFrame>::with_capacity(batch_size as usize),
          )
        })
        .collect::<Vec<(usize, Vec<ProcessFrame>)>>(),
    );

    BuffersOfFrames {
      buffers,
      batch_size,
    }
  }

  pub fn get_full_buffer(&mut self, stream_index: usize) -> Option<Vec<ProcessFrame>> {
    let number_of_frames = self
      .buffers
      .get(&stream_index)
      .map(|buffer| buffer.len())
      .unwrap_or_default();

    let buffer = (number_of_frames == self.batch_size as usize)
      .then(|| self.buffers.remove(&stream_index))
      .flatten();

    if buffer.is_some() {
      self.buffers.insert(
        stream_index,
        Vec::<ProcessFrame>::with_capacity(self.batch_size as usize),
      );
    };

    buffer
  }

  pub fn push(&mut self, stream_index: usize, frame: ProcessFrame) {
    if let Some(ref mut buffer) = self.buffers.get_mut(&stream_index) {
      buffer.push(frame)
    }
  }
}
