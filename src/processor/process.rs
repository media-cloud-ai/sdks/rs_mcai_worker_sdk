use crate::{
  logging::SharedJobId, message_exchange::message::OrderMessage, worker::WorkerConfiguration,
  McaiChannel, Result,
};
use std::sync::{Arc, Mutex};

// A trait to define common action between simple and media process
// Keep that only to facilitate testing between features
pub trait Process<P, D, W> {
  fn new(
    worker: Arc<Mutex<W>>,
    response_sender: McaiChannel,
    worker_configuration: WorkerConfiguration,
    shared_job_id: SharedJobId,
  ) -> Self;

  fn handle(&mut self, worker: Arc<Mutex<W>>, order_message: OrderMessage) -> Result<()>;

  fn get_current_job_id(&self, worker: Arc<Mutex<W>>) -> Option<u64>;
}
