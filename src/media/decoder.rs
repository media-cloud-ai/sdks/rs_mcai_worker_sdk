use super::{
  ebu_ttml_live::EbuTtmlLiveDecoder, json::JsonDecoder, subrip::SubRipDecoder,
  webvtt::WebVttDecoder, ProcessFrame,
};
use stainless_ffmpeg::{
  audio_decoder::AudioDecoder, check_result, filter_graph::FilterGraph, frame::Frame,
  packet::Packet, prelude::*,
};
use std::mem::forget;

pub enum DecoderType {
  Audio(AudioDecoder),
  Video(VideoDecoder),
  EbuTtmlLive(EbuTtmlLiveDecoder),
  Json(JsonDecoder),
  WebVtt(WebVttDecoder),
  SubRip(SubRipDecoder),
  Data,
}

pub enum DecodeResult {
  EndOfStream,
  Frame {
    stream_index: usize,
    frame: ProcessFrame,
  },
  Nothing,
  WaitMore,
  ConnectionClosed,
}

pub struct Decoder {
  pub(crate) decoder: DecoderType,
  pub(crate) graph: Option<FilterGraph>,
}

impl Decoder {
  pub(crate) fn decode(
    &mut self,
    packet: &Packet,
  ) -> std::result::Result<Option<ProcessFrame>, String> {
    match &mut self.decoder {
      // AUDIO
      DecoderType::Audio(audio_decoder) => {
        log::trace!("[FFmpeg] Send packet to audio decoder");

        let av_frame = unsafe {
          let ret_code = avcodec_send_packet(audio_decoder.codec_context, packet.packet);
          check_result!(ret_code);

          let av_frame = av_frame_alloc();
          let ret_code = avcodec_receive_frame(audio_decoder.codec_context, av_frame);
          check_result!(ret_code);

          let frame = Frame {
            frame: av_frame,
            name: Some("audio_source_1".to_string()),
            index: 1,
          };

          if let Some(graph) = &self.graph {
            if let Ok((audio_frames, _video_frames)) = graph.process(&[frame], &[]) {
              log::trace!("[FFmpeg] Output graph count {} frames", audio_frames.len());
              let frame = audio_frames.first().unwrap();
              av_frame_clone(frame.frame)
            } else {
              av_frame
            }
          } else {
            av_frame
          }
        };

        let frame = Frame {
          frame: av_frame,
          name: Some("audio".to_string()),
          index: 1,
        };

        Ok(Some(ProcessFrame::AudioVideo(frame)))
      }

      // VIDEO
      DecoderType::Video(video_decoder) => {
        log::trace!("[FFmpeg] Send packet to video decoder");

        let av_frame = unsafe {
          let ret_code = avcodec_send_packet(video_decoder.codec_context, packet.packet);
          check_result!(ret_code);

          let av_frame = av_frame_alloc();
          let ret_code = avcodec_receive_frame(video_decoder.codec_context, av_frame);
          check_result!(ret_code);

          let frame = Frame {
            frame: av_frame,
            name: Some("video_source_1".to_string()),
            index: 1,
          };

          if let Some(graph) = &self.graph {
            if let Ok((_audio_frames, video_frames)) = graph.process(&[], &[frame]) {
              log::trace!("[FFmpeg] Output graph count {} frames", video_frames.len());
              let frame = video_frames.first().unwrap();
              av_frame_clone(frame.frame)
            } else {
              av_frame
            }
          } else {
            av_frame
          }
        };

        let frame = Frame {
          frame: av_frame,
          name: Some("video".to_string()),
          index: 1,
        };

        Ok(Some(ProcessFrame::AudioVideo(frame)))
      }

      // EBUTTMLLIVE
      DecoderType::EbuTtmlLive(ebu_ttml_live_decoder) => {
        let result = ebu_ttml_live_decoder
          .decode(packet)?
          .map(|ttml_content| ProcessFrame::EbuTtmlLive(Box::new(ttml_content)));
        Ok(result)
      }

      // JSON
      DecoderType::Json(json_decoder) => {
        let result = json_decoder
          .decode(packet)?
          .map(|json_value| ProcessFrame::Json(Box::new(json_value)));
        Ok(result)
      }

      // WEBVTT
      DecoderType::WebVtt(webvtt_decoder) => {
        let result = webvtt_decoder
          .decode(packet)?
          .map(|webvtt_value| ProcessFrame::WebVtt(Box::new(webvtt_value)));
        Ok(result)
      }

      // SUBRIP
      DecoderType::SubRip(subrip_decoder) => {
        let result = subrip_decoder
          .decode(packet)?
          .map(|subrip_value| ProcessFrame::SubRip(Box::new(subrip_value)));
        Ok(result)
      }

      // DATA
      DecoderType::Data => {
        let data_size = unsafe { (*packet.packet).size as usize };
        let data = unsafe { (*packet.packet).data };
        let vec_data = unsafe { Vec::from_raw_parts(data, data_size, data_size) };
        let decoded_data = vec_data.clone();

        forget(vec_data);

        Ok(Some(ProcessFrame::Data(decoded_data)))
      }
    }
  }
}
