use super::{Timecode, WebVtt};
use regex::Regex;
use stainless_ffmpeg::packet::Packet;
use std::{collections::VecDeque, mem::forget};

#[derive(Default)]
pub struct WebVttDecoder {
  buffer: VecDeque<String>,
}

impl WebVttDecoder {
  pub fn new() -> Self {
    WebVttDecoder {
      buffer: VecDeque::new(),
    }
  }

  pub fn decode(&mut self, packet: &Packet) -> Result<Option<Vec<WebVtt>>, String> {
    let data_size = unsafe { (*packet.packet).size as usize };
    let data = unsafe { (*packet.packet).data };
    log::trace!("Decoding {} bytes WEBVTT content", data_size);

    let webvtt_content = unsafe { String::from_raw_parts(data, data_size, data_size) };
    log::debug!("Try decoding: {}", webvtt_content);

    let webvtt_value = self.decode_content(&webvtt_content)?;

    forget(webvtt_content);

    Ok(webvtt_value)
  }

  pub fn decode_content(&mut self, webvtt_content: &str) -> Result<Option<Vec<WebVtt>>, String> {
    let vec_webvtt: Option<Vec<WebVtt>> = {
      let buffer_size = self.buffer.len();

      let principal_webvtt = if webvtt_content.contains('}') {
        let vec: Vec<&str> = webvtt_content.split('}').collect();
        let first_webvtt = vec[0].to_string();
        let last_webvtt = vec[1].to_string();
        if !last_webvtt.is_empty() {
          self.buffer.push_back(last_webvtt);
        }
        first_webvtt
      } else {
        log::debug!(
          "Add incomplete WEBVTT content to buffer (buffer size: {})",
          self.buffer.len()
        );
        self.buffer.push_back(webvtt_content.to_string());
        log::trace!(
          "Incomplete WEBVTT content added to buffer: {}",
          webvtt_content
        );
        return Ok(None);
      };

      let actual_webvtt = if buffer_size > 0 {
        let mut complete_webvtt = String::new();
        for _ in 0..buffer_size {
          if let Some(previous_content) = self.buffer.pop_front() {
            log::debug!(
              "Get a previous WEBVTT content from buffer to complete the new one (buffer size: {})",
              self.buffer.len()
            );
            complete_webvtt = format!("{complete_webvtt}{previous_content}");
          } else {
            return Err(format!("Incorrect WEBVTT content: {principal_webvtt}",));
          }
        }
        complete_webvtt = format!("{complete_webvtt}{principal_webvtt}");
        log::debug!("Concatenated WEBVTT content: {}", complete_webvtt);
        complete_webvtt
      } else {
        principal_webvtt
      };

      let result = webvtt_vec_from_string(actual_webvtt.clone());
      if result.is_err() {
        log::error!(
          "WEBVTT segment is not in the correct format: {}",
          actual_webvtt
        );
        return Ok(None);
      };

      Some(result.unwrap())
    };

    Ok(vec_webvtt)
  }
}

pub fn webvtt_vec_from_string(input: String) -> Result<Vec<WebVtt>, String> {
  let mut vec: Vec<WebVtt> = vec![];
  let mut headers: Vec<String> = vec![];

  // HEADERS
  let re = Regex::new(r"#(.*)").unwrap();

  for caps in re.captures_iter(&input) {
    let header: String = caps.get(1).map_or("", |m| m.as_str()).trim().to_string();
    headers.push(header);
  }

  // CONTENT
  let re =
    Regex::new(r"([0-9][0-9]*)\n([0-9:\.]{12}) --> ([0-9:\.]{12})\n(.*\n.*\n[a-zA-Z].*|.*\n.*|.*)")
      .unwrap();

  for caps in re.captures_iter(&input) {
    let id: i64 = caps
      .get(1)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let begin: Timecode = caps
      .get(2)
      .map_or("", |m| m.as_str())
      .parse::<Timecode>()
      .unwrap();
    let end: Timecode = caps
      .get(3)
      .map_or("", |m| m.as_str())
      .parse::<Timecode>()
      .unwrap();
    let content: String = caps.get(4).map_or("", |m| m.as_str()).trim().to_string();

    let part = WebVtt {
      id,
      begin,
      end,
      content: Some(content),
      headers: Some(headers.clone()),
    };
    vec.push(part);
  }
  Ok(vec)
}

pub fn webvtt_vec_to_string(
  input: Vec<WebVtt>,
  put_headers: bool,
  delimiter: bool,
) -> Result<String, String> {
  let mut stringified: String = "WEBVTT".to_string();

  if put_headers && !input.is_empty() {
    if let Some(headers) = &input[0].headers {
      for header in headers {
        stringified = format!("{stringified}\n#{header}");
      }
    }
  }

  for part in input {
    let content: String = if let Some(stripped) = part.content.as_ref().unwrap().strip_suffix('\n')
    {
      stripped.to_string()
    } else {
      part.content.unwrap()
    };
    stringified = format!(
      "{}\n\n{}\n{} --> {}\n{}",
      stringified, part.id, part.begin, part.end, content
    );
  }

  if delimiter {
    stringified = format!("{}\n\n{}", stringified, '}');
  }

  stringified = stringified
    .replace("\n\n -", "\n -")
    .replace(" \n\n-", "\n -");

  Ok(stringified)
}
