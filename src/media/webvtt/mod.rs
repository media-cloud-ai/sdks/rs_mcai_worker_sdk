pub mod decoder;
mod timecode;

pub use decoder::{webvtt_vec_from_string, webvtt_vec_to_string, WebVttDecoder};
pub use timecode::Timecode;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct WebVtt {
  pub id: i64,
  pub begin: Timecode,
  pub end: Timecode,
  pub content: Option<String>,
  pub headers: Option<Vec<String>>,
}

#[test]
#[cfg(feature = "media")]
fn test_webvtt_deserialization() {
  println!("Test 1");
  let mut webvtt_decoder = WebVttDecoder::new();
  let input_string = "WEBVTT\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> 00:00:02.000\nThis is a second test\n\n}";
  let actual_value = "WEBVTT\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> 00:00:02.000\nThis is a second test";
  let value: Vec<WebVtt> = webvtt_vec_from_string(actual_value.to_string()).unwrap();
  assert_eq!(
    value,
    webvtt_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  println!("Test 2");
  let input_string =
    "WEBVTT\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> ";
  assert_eq!(None, webvtt_decoder.decode_content(input_string).unwrap());

  println!("Test 3");
  let input_string = "00:00:02.000\nThis is a second test\n\n}WEBVTT\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2";
  assert_eq!(
    value,
    webvtt_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  println!("Test 4");
  let input_string = "\n00:00:01.000 --> ";
  assert_eq!(None, webvtt_decoder.decode_content(input_string).unwrap());

  println!("Test 5");
  let input_string = "00:00:02.000\nThis is a second test\n}";
  assert_eq!(
    value,
    webvtt_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  println!("Test 6");
  let input_string = "WEBVTT\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> 00:00:02.000\nThis is a second test\n\n}";
  let stringified = webvtt_vec_to_string(value, true, true).unwrap();
  assert_eq!(input_string.to_string(), stringified);
}

#[test]
#[cfg(feature = "media")]
fn test_webvtt_deserialization_with_headers() {
  let mut webvtt_decoder = WebVttDecoder::new();
  let input_string = "WEBVTT\n#HEADER1\n#HEADER2\n#HEADER3\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> 00:00:02.000\nThis is a second test\n\n}";
  let actual_value = "WEBVTT\n#HEADER1\n#HEADER2\n#HEADER3\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> 00:00:02.000\nThis is a second test";
  let value: Vec<WebVtt> = webvtt_vec_from_string(actual_value.to_string()).unwrap();
  assert_eq!(value[0].headers.as_ref().unwrap().len(), 3);
  assert_eq!(
    value,
    webvtt_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  let input_string = "WEBVTT\n#HEADER1\n#HEADER2\n#HEADER3\n\n1\n00:00:00.001 --> 00:00:01.000\nThis is a test\n\n2\n00:00:01.000 --> 00:00:02.000\nThis is a second test\n\n}";
  let stringified = webvtt_vec_to_string(value, true, true).unwrap();
  assert_eq!(input_string.to_string(), stringified);
}
