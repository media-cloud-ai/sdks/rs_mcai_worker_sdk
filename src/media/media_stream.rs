use crate::config::is_complex_stream;
use ringbuf::Consumer;
use stainless_ffmpeg::prelude::*;
use std::{
  collections::HashMap,
  ffi::{c_void, CStr, CString},
  io::{Error, ErrorKind, Result},
  ptr::null_mut,
  str::from_utf8_unchecked,
};

unsafe fn to_string(data: *const i8) -> String {
  if data.is_null() {
    return "".to_string();
  }
  from_utf8_unchecked(CStr::from_ptr(data).to_bytes()).to_string()
}

macro_rules! check_result {
  ($condition: expr, $block: block) => {
    let errnum = $condition;
    if errnum < 0 {
      let mut data = [0i8; AV_ERROR_MAX_STRING_SIZE];
      av_strerror(errnum, data.as_mut_ptr(), AV_ERROR_MAX_STRING_SIZE);
      $block;
      return Err(Error::new(
        ErrorKind::InvalidInput,
        to_string(data.as_ptr()),
      ));
    }
  };
  ($condition: expr) => {
    let errnum = $condition;
    if errnum < 0 {
      let mut data = [0i8; AV_ERROR_MAX_STRING_SIZE];
      av_strerror(errnum, data.as_mut_ptr(), AV_ERROR_MAX_STRING_SIZE);
      return Err(Error::new(
        ErrorKind::InvalidInput,
        to_string(data.as_ptr()),
      ));
    }
  };
}

#[derive(Debug)]
pub struct MediaStream {
  pub format_context: *mut AVFormatContext,
  #[allow(dead_code)]
  stream_info: bool,
  #[allow(dead_code)]
  stream_ids: Vec<u8>,
  #[allow(dead_code)]
  decoders: HashMap<u8, Decoder>,
  #[allow(dead_code)]
  graph: Option<FilterGraph>,
}

#[derive(Debug)]
pub struct Decoder {
  #[allow(dead_code)]
  codec: *mut AVCodec,
  #[allow(dead_code)]
  context: *mut AVCodecContext,
  #[allow(dead_code)]
  decoder: AudioDecoder,
}

unsafe extern "C" fn read_data(opaque: *mut c_void, raw_buffer: *mut u8, buf_size: i32) -> i32 {
  log::trace!("Read more data: {} bytes", buf_size);
  let consumer: &mut Consumer<u8> = &mut *(opaque as *mut Consumer<u8>);

  if consumer.is_empty() {
    log::trace!("Empty source stream");
    if is_complex_stream() {
      std::thread::sleep(std::time::Duration::from_millis(100));
    }
    return 0;
  }

  let mut vec = std::slice::from_raw_parts_mut(raw_buffer, buf_size as usize);

  let size = consumer
    .write_into(&mut vec, Some(buf_size as usize))
    .unwrap();

  size as i32
}

impl MediaStream {
  pub fn new(format: &str, consumer: Consumer<u8>) -> Result<Self> {
    unsafe {
      av_log_set_level(AV_LOG_ERROR);
      av_log_set_level(AV_LOG_QUIET);
    }

    let buffer_size = 2048;
    let mut format_context = unsafe { avformat_alloc_context() };

    unsafe {
      let buffer = av_malloc(buffer_size);

      let cformat = CString::new(format).unwrap();
      let av_input_format = av_find_input_format(cformat.as_ptr());
      log::info!("[FFMpeg] Open dynamic buffer");

      let writable_buffer = 0;
      let opaque = Box::new(consumer);

      let avio_context = avio_alloc_context(
        buffer as *mut u8,
        buffer_size as i32,
        writable_buffer,
        Box::into_raw(opaque) as *mut c_void,
        Some(read_data),
        None,
        None,
      );
      (*format_context).pb = avio_context;

      log::info!("[FFMpeg] Open Input");
      check_result!(avformat_open_input(
        &mut format_context,
        null_mut(),
        av_input_format,
        null_mut(),
      ));
    }

    log::info!("MediaStream created");

    Ok(MediaStream {
      decoders: HashMap::new(),
      format_context,
      stream_info: false,
      stream_ids: vec![],
      graph: None,
    })
  }

  pub fn stream_info(&self) -> Result<()> {
    log::info!("[FFMpeg] Find stream info");
    unsafe {
      check_result!(avformat_find_stream_info(self.format_context, null_mut()));
      Ok(())
    }
  }
}
