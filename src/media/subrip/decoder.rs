use super::{SubRip, Timecode};
use regex::Regex;
use stainless_ffmpeg::packet::Packet;
use std::{collections::VecDeque, mem::forget};

#[derive(Default)]
pub struct SubRipDecoder {
  buffer: VecDeque<String>,
}

impl SubRipDecoder {
  pub fn new() -> Self {
    SubRipDecoder {
      buffer: VecDeque::new(),
    }
  }

  pub fn decode(&mut self, packet: &Packet) -> Result<Option<Vec<SubRip>>, String> {
    let data_size = unsafe { (*packet.packet).size as usize };
    let data = unsafe { (*packet.packet).data };
    log::trace!("Decoding {} bytes SubRip content", data_size);

    let subrip_content = unsafe { String::from_raw_parts(data, data_size, data_size) };
    log::debug!("Try decoding: {}", subrip_content);

    let vec_subrip = self.decode_content(&subrip_content)?;

    forget(subrip_content);

    Ok(vec_subrip)
  }

  pub fn decode_content(&mut self, subrip_content: &str) -> Result<Option<Vec<SubRip>>, String> {
    let vec_subrip: Option<Vec<SubRip>> = {
      let buffer_size = self.buffer.len();

      let principal_subrip = if subrip_content.contains('}') {
        let vec: Vec<&str> = subrip_content.split('}').collect();
        let first_subrip = vec[0].to_string();
        let last_subrip = vec[1].to_string();
        if !last_subrip.is_empty() {
          self.buffer.push_back(last_subrip);
        }
        first_subrip
      } else {
        log::debug!(
          "Add incomplete SubRip content to buffer (buffer size: {})",
          self.buffer.len()
        );
        self.buffer.push_back(subrip_content.to_string());
        log::trace!(
          "Incomplete SubRip content added to buffer: {}",
          subrip_content
        );
        return Ok(None);
      };

      let actual_subrip = if buffer_size > 0 {
        let mut complete_subrip = String::new();
        for _ in 0..buffer_size {
          if let Some(previous_content) = self.buffer.pop_front() {
            log::debug!(
              "Get a previous SUBRIP content from buffer to complete the new one (buffer size: {})",
              self.buffer.len()
            );
            complete_subrip = format!("{complete_subrip}{previous_content}");
          } else {
            return Err(format!("Incorrect SUBRIP content: {principal_subrip}"));
          }
        }
        complete_subrip = format!("{complete_subrip}{principal_subrip}");
        log::debug!("Concatenated SUBRIP content: {}", complete_subrip);
        complete_subrip
      } else {
        principal_subrip
      };

      let result = subrip_vec_from_string(actual_subrip.clone());
      if result.is_err() {
        log::error!(
          "SubRip segment is not in the correct format: {}",
          actual_subrip
        );
        return Ok(None);
      };

      Some(result.unwrap())
    };

    Ok(vec_subrip)
  }
}

pub fn subrip_vec_from_string(input: String) -> Result<Vec<SubRip>, String> {
  let mut vec: Vec<SubRip> = vec![];
  let re =
    Regex::new(r"([0-9][0-9]*)\n([0-9:,]{12}) --> ([0-9:,]{12})\n(.*\n.*\n[a-zA-Z].*|.*\n.*|.*)")
      .unwrap();

  for caps in re.captures_iter(&input) {
    let id: i64 = caps
      .get(1)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let begin: Timecode = caps
      .get(2)
      .map_or("", |m| m.as_str())
      .parse::<Timecode>()
      .unwrap();
    let end: Timecode = caps
      .get(3)
      .map_or("", |m| m.as_str())
      .parse::<Timecode>()
      .unwrap();
    let content: String = caps.get(4).map_or("", |m| m.as_str()).trim().to_string();

    let part = SubRip {
      id,
      begin,
      end,
      content: Some(content),
    };
    vec.push(part);
  }
  Ok(vec)
}

pub fn subrip_vec_to_string(input: Vec<SubRip>, delimiter: bool) -> Result<String, String> {
  let mut stringified: String = String::new();

  if delimiter {
    stringified = "SUBRIP".to_string();
  }

  for part in input {
    let content: String = if let Some(stripped) = part.content.as_ref().unwrap().strip_suffix('\n')
    {
      stripped.to_string()
    } else {
      part.content.unwrap()
    };
    stringified = format!(
      "{}\n\n{}\n{} --> {}\n{}",
      stringified, part.id, part.begin, part.end, content
    );
  }

  if delimiter {
    stringified = format!("{}\n\n{}", stringified, '}');
  }

  stringified = stringified
    .replace("\n\n -", "\n -")
    .replace(" \n\n-", "\n -");

  Ok(stringified)
}
