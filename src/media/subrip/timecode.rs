use regex::Regex;
use std::{fmt, str::FromStr};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Timecode {
  pub hours: i64,
  pub minutes: i64,
  pub seconds: i64,
  pub milliseconds: i64,
}

impl Timecode {
  pub fn to_frames(self) -> f32 {
    let fps: f32 = 25.0;

    ((f32::from(self.hours as u16) * 60.0 + f32::from(self.minutes as u16)) * 60.0
      + f32::from(self.seconds as u16)
      + f32::from(self.milliseconds as u16) * 0.001)
      * fps
  }

  pub fn to_milliseconds(self) -> i64 {
    self.hours * 3_600_000 + self.minutes * 60_000 + self.seconds * 1000 + self.milliseconds
  }
}

impl FromStr for Timecode {
  type Err = ();
  fn from_str(input: &str) -> Result<Self, Self::Err> {
    let re = Regex::new(r"([0-9]{2}):([0-9]{2}):([0-9]{2})[\.,]([0-9]{3})").unwrap();
    let caps = re.captures(input).unwrap();

    let hours: i64 = caps
      .get(1)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let minutes: i64 = caps
      .get(2)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let seconds: i64 = caps
      .get(3)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let milliseconds: i64 = caps
      .get(4)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();

    Ok(Timecode {
      hours,
      minutes,
      seconds,
      milliseconds,
    })
  }
}

impl fmt::Display for Timecode {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "{:0>2}:{:0>2}:{:0>2},{:0>3}",
      self.hours, self.minutes, self.seconds, self.milliseconds
    )
  }
}

#[test]
#[cfg(feature = "media")]
fn test_subrip_timecode_convert_to_frames() {
  let timecode = Timecode {
    hours: 1,
    minutes: 22,
    seconds: 37,
    milliseconds: 150,
  };

  assert_eq!(123928.75, timecode.to_frames());
}
