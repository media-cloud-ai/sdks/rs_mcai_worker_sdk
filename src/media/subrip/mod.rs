pub mod decoder;
pub mod timecode;

pub use decoder::{subrip_vec_from_string, subrip_vec_to_string, SubRipDecoder};
pub use timecode::Timecode;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct SubRip {
  pub id: i64,
  pub begin: Timecode,
  pub end: Timecode,
  pub content: Option<String>,
}

#[test]
#[cfg(feature = "media")]
fn test_subrip_deserialization() {
  println!("Test 1");
  let mut subrip_decoder = SubRipDecoder::new();
  let input_string = "SUBRIP\n\n1\n00:00:00,100 --> 00:00:01,000\nThis is a test\n\n2\n00:00:01,000 --> 00:00:02,000\nThis is a second test\n\n}";
  let actual_value = "SUBRIP\n\n1\n00:00:00,100 --> 00:00:01,000\nThis is a test\n\n2\n00:00:01,000 --> 00:00:02,000\nThis is a second test";
  let value: Vec<SubRip> = subrip_vec_from_string(actual_value.to_string()).unwrap();
  assert_eq!(
    value,
    subrip_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  println!("Test 2");
  let input_string =
    "SUBRIP\n\n1\n00:00:00,100 --> 00:00:01,000\nThis is a test\n\n2\n00:00:01,000 --> ";
  assert_eq!(None, subrip_decoder.decode_content(input_string).unwrap());

  println!("Test 3");
  let input_string = "00:00:02,000\nThis is a second test\n\n}SUBRIP\n\n1\n00:00:00,100 --> 00:00:01,000\nThis is a test\n\n2";
  assert_eq!(
    value,
    subrip_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  println!("Test 4");
  let input_string = "\n00:00:01,000 --> ";
  assert_eq!(None, subrip_decoder.decode_content(input_string).unwrap());

  println!("Test 5");
  let input_string = "00:00:02,000\nThis is a second test\n}";
  assert_eq!(
    value,
    subrip_decoder
      .decode_content(input_string)
      .unwrap()
      .unwrap()
  );

  println!("Test 6");
  let input_string = "SUBRIP\n\n1\n00:00:00,100 --> 00:00:01,000\nThis is a test\n\n2\n00:00:01,000 --> 00:00:02,000\nThis is a second test\n\n}";
  let stringified = subrip_vec_to_string(value, true).unwrap();
  assert_eq!(input_string.to_string(), stringified);
}
