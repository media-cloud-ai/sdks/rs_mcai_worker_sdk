pub mod audio;
pub mod decoder;
pub mod ebu_ttml_live;
pub mod filters;
pub mod json;
mod media_stream;
pub mod output;
#[cfg(feature = "media")]
pub mod process_frame;
pub mod source;
mod srt;
pub mod subrip;
pub mod video;
pub mod webvtt;

use audio::AudioFormat;
use filters::{AudioFilter, VideoFilter};
use output::Output;
use process_frame::ProcessFrame;
use source::{Source, SourceSegment};
use video::{RegionOfInterest, Scaling, VideoFormat};

use crate::{
  job::{Job, JobResult, JobStatus},
  mcai_worker::{McaiWorker, McaiWorkerDescription},
  parameter::container::ParametersContainer,
  Result,
};
use schemars::JsonSchema;
use serde::de::DeserializeOwned;
use std::sync::{Arc, Mutex};

pub const SOURCE_PATH_PARAMETER: &str = "source_path";
pub const DESTINATION_PATH_PARAMETER: &str = "destination_path";
pub const BATCH_SIZE: &str = "batch_size";
pub const START_INDEX_PARAMETER: &str = "sdk_start_index";
pub const STOP_INDEX_PARAMETER: &str = "sdk_stop_index";

#[cfg(feature = "media")]
#[derive(Debug, Eq, PartialEq)]
pub enum StreamConfiguration {
  Audio(AudioConfiguration),
  Image(ImageConfiguration),
  EbuTtmlLive,
  Json,
  WebVtt,
  SubRip,
  Data,
}

#[cfg(feature = "media")]
#[derive(Debug, Eq, PartialEq)]
pub struct StreamDescriptor {
  index: usize,
  configuration: StreamConfiguration,
}

impl StreamDescriptor {
  pub fn new_audio(index: usize, filters: Vec<AudioFilter>) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::Audio(AudioConfiguration { filters }),
    }
  }

  pub fn new_video(index: usize, filters: Vec<VideoFilter>) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::Image(ImageConfiguration { filters }),
    }
  }

  pub fn new_ebu_ttml_live(index: usize) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::EbuTtmlLive,
    }
  }

  pub fn new_json(index: usize) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::Json,
    }
  }

  pub fn new_webvtt(index: usize) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::WebVtt,
    }
  }

  pub fn new_subrip(index: usize) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::SubRip,
    }
  }

  pub fn new_data(index: usize) -> Self {
    StreamDescriptor {
      index,
      configuration: StreamConfiguration::Data,
    }
  }
}

#[cfg(feature = "media")]
#[derive(Debug, Eq, PartialEq)]
pub struct AudioConfiguration {
  filters: Vec<AudioFilter>,
}

#[cfg(feature = "media")]
#[derive(Debug, Eq, PartialEq)]
pub struct ImageConfiguration {
  filters: Vec<VideoFilter>,
}

pub fn initialize_process<
  P: DeserializeOwned + JsonSchema,
  D: McaiWorkerDescription,
  W: McaiWorker<P, D> + Send,
>(
  worker: Arc<Mutex<W>>,
  job: &Job,
) -> Result<(Source, Output, u32)> {
  let job_result = JobResult::new(job.job_id);
  let parameters = job.get_parameters()?;

  let source_url: String = job.get_parameter(SOURCE_PATH_PARAMETER)?;
  let output_url: String = job.get_parameter(DESTINATION_PATH_PARAMETER)?;
  let batch_size: u32 = job
    .get_parameter(BATCH_SIZE)
    .map(|size: i64| size.max(1))
    .unwrap_or(1) as u32;
  let source_segment = SourceSegment::from(job);

  let output = output::Output::new(&output_url)?;

  let source = source::Source::new(
    worker,
    &job_result,
    parameters,
    &source_url,
    output.get_sender(),
    source_segment,
  )?;

  Ok((source, output, batch_size))
}

pub fn finish_process<
  P: DeserializeOwned + JsonSchema,
  D: McaiWorkerDescription,
  W: McaiWorker<P, D>,
>(
  worker: Arc<Mutex<W>>,
  output: &mut Output,
  job_result: JobResult,
) -> Result<JobResult> {
  worker.lock().unwrap().ending_process()?;

  output.complete()?;
  let job_result = job_result.with_status(JobStatus::Completed);
  Ok(job_result)
}

pub fn process_frames<
  P: DeserializeOwned + JsonSchema,
  D: McaiWorkerDescription,
  W: McaiWorker<P, D>,
>(
  worker: Arc<Mutex<W>>,
  output: &mut Output,
  job_result: JobResult,
  stream_index: usize,
  frames: &[ProcessFrame],
) -> Result<()> {
  let result = worker
    .lock()
    .unwrap()
    .process_frames(job_result, stream_index, frames)?;

  output.push(result);

  Ok(())
}
