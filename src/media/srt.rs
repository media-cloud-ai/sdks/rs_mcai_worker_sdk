use crate::{
  config::{get_packet_drop_threshold, get_srt_latency, get_too_late_packet_drop},
  Result,
};
use async_std::sync::Mutex;
use bytes::Bytes;
use futures_util::{sink::SinkExt, TryStreamExt};
use srt_tokio::SrtSocket;
use std::{sync::Arc, time::Instant};
use tokio::runtime::Runtime;

pub struct SrtStream {
  socket: Arc<Mutex<SrtSocket>>,
  runtime: Runtime,
}

impl SrtStream {
  pub fn is_srt_stream(url: &str) -> bool {
    url.starts_with("srt://")
  }

  pub fn open_connection(url: &str) -> Result<SrtStream> {
    let runtime = Runtime::new().unwrap();

    let socket = runtime.block_on(async {
      if url.starts_with("srt://:") {
        let port = url.replace("srt://:", "").parse::<u16>().unwrap();
        SrtSocket::builder()
          .set(|options| {
            options.receiver.too_late_packet_drop = get_too_late_packet_drop();
            options.sender.drop_delay = get_packet_drop_threshold();
          })
          .latency(get_srt_latency())
          .listen_on(&*format!(":{}", port))
          .await
          .unwrap()
      } else {
        let url = url.replace("srt://", "");

        SrtSocket::builder()
          .set(|options| {
            options.receiver.too_late_packet_drop = get_too_late_packet_drop();
            options.sender.drop_delay = get_packet_drop_threshold();
          })
          .latency(get_srt_latency())
          .call(&*url, None)
          .await
          .unwrap()
      }
    });

    let socket = Arc::new(Mutex::new(socket));

    log::info!("SRT connected");
    Ok(SrtStream { socket, runtime })
  }

  pub fn send(&mut self, data: Bytes) {
    let socket = self.socket.clone();
    self.runtime.block_on(async {
      if let Err(reason) = socket.lock().await.send((Instant::now(), data)).await {
        log::error!("unable to send message, reason: {}", reason);
      }
    });
  }

  pub fn receive(&mut self) -> Option<(Instant, Bytes)> {
    let socket = self.socket.clone();
    self
      .runtime
      .block_on(async { socket.lock().await.try_next().await.unwrap() })
  }

  pub fn close(&mut self) {
    let socket = self.socket.clone();
    self.runtime.block_on(async {
      socket.lock().await.close().await.unwrap();
    });
  }
}

#[test]
pub fn test_is_srt_stream() {
  let file_name = "file.txt";
  assert!(!SrtStream::is_srt_stream(file_name));
  let file_path = "/path/to/file";
  assert!(!SrtStream::is_srt_stream(file_path));
  let file_url = "file://path/to/file";
  assert!(!SrtStream::is_srt_stream(file_url));
  let http_url = "http://path/to/resource";
  assert!(!SrtStream::is_srt_stream(http_url));

  let srt_url = "srt://path/to/resource";
  assert!(SrtStream::is_srt_stream(srt_url));
}
