use crate::{
  http::HttpError,
  job::{JobResult, JobStatus},
};
use serde::{ser::SerializeStruct, Serialize, Serializer};

/// Internal error status to manage process errors
#[derive(Clone, Debug, PartialEq)]
pub enum MessageError {
  Amqp(lapin::Error),
  Http(HttpError),
  RuntimeError(String),
  ParameterValueError(String),
  ProcessingError(JobResult),
  RequirementsError(String),
  NotImplemented(),
}

impl MessageError {
  pub fn from_error_and_result(error: std::io::Error, job_result: JobResult) -> Self {
    let result = job_result
      .with_status(JobStatus::Error)
      .with_message(&format!("IO Error: {error}"));

    MessageError::ProcessingError(result)
  }
}

impl From<lapin::Error> for MessageError {
  fn from(error: lapin::Error) -> Self {
    MessageError::Amqp(error)
  }
}

impl From<serde_json::Error> for MessageError {
  fn from(error: serde_json::Error) -> Self {
    MessageError::RuntimeError(error.to_string())
  }
}

impl From<HttpError> for MessageError {
  fn from(error: HttpError) -> Self {
    MessageError::Http(error)
  }
}

impl ToString for MessageError {
  fn to_string(&self) -> String {
    match self {
      MessageError::Amqp(error) => format!("RabbitMQ: {error}"),
      MessageError::RuntimeError(error) => format!("Runtime: {error}"),
      MessageError::ParameterValueError(error) => format!("Parameter value: {error}"),
      MessageError::ProcessingError(error) => format!("Processing: {error:?}"),
      MessageError::RequirementsError(error) => format!("Requirements: {error}"),
      MessageError::NotImplemented() => "Not implemented!".to_string(),
      MessageError::Http(error) => format!("HTTP {}", error.to_string()),
    }
  }
}

impl Serialize for MessageError {
  fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    let mut state = serializer.serialize_struct("Error", 2)?;
    state.serialize_field("type", "error")?;
    state.serialize_field("message", &self.to_string())?;
    state.end()
  }
}

impl Eq for MessageError {}

pub type Result<T> = std::result::Result<T, MessageError>;
