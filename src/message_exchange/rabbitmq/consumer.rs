use super::{message_handler::MessageHandler, publish, CurrentState};
use crate::{message_exchange::OrderMessage, MessageError, Result};
use amq_protocol_types::FieldTable;
use async_std::{
  channel::Sender,
  future::timeout,
  stream::StreamExt,
  task::{self, JoinHandle},
};
use lapin::{
  message::Delivery,
  options::{BasicCancelOptions, BasicConsumeOptions, BasicNackOptions},
  Channel, Consumer,
};
use std::{
  sync::{
    atomic::{AtomicBool, Ordering},
    Arc, Mutex,
  },
  time::Duration,
};

pub struct RabbitmqConsumer {
  handle: Option<JoinHandle<()>>,
  should_consume: Arc<AtomicBool>,
  consumer_triggers: Arc<Mutex<Vec<Arc<AtomicBool>>>>,
}

pub const RABBITMQ_CONSUMER_TAG_JOB: &str = "amqp_worker";
pub const RABBITMQ_CONSUMER_TAG_DIRECT: &str = "status_amqp_worker";

const CONSUMER_TIMEOUT: Duration = Duration::from_millis(500);

impl RabbitmqConsumer {
  pub async fn new(
    channel: Arc<Channel>,
    sender: Sender<OrderMessage>,
    queue_name: &str,
    consumer_tag: &str,
    current_state: Arc<Mutex<CurrentState>>,
    start_consuming: bool,
  ) -> Result<Self> {
    log::debug!("Start RabbitMQ consumer on queue {:?}", queue_name);

    let should_consume = Arc::new(AtomicBool::new(start_consuming));
    let consumer_triggers = Arc::new(Mutex::new(vec![]));

    let handle = Self::start_consumer(
      channel,
      queue_name,
      consumer_tag,
      sender,
      current_state.clone(),
      should_consume.clone(),
      consumer_triggers.clone(),
    )
    .await?;

    Ok(RabbitmqConsumer {
      handle,
      should_consume,
      consumer_triggers,
    })
  }

  pub fn connect(&mut self, rabbitmq_consumer: &RabbitmqConsumer) {
    self
      .consumer_triggers
      .lock()
      .unwrap()
      .push(rabbitmq_consumer.get_trigger());
  }

  pub fn stop(&mut self) {
    self.handle.take().map(JoinHandle::cancel);
  }

  pub(crate) fn get_trigger(&self) -> Arc<AtomicBool> {
    self.should_consume.clone()
  }

  async fn start_consumer(
    channel: Arc<Channel>,
    queue_name: &str,
    consumer_tag: &str,
    sender: Sender<OrderMessage>,
    current_status: Arc<Mutex<CurrentState>>,
    should_consume: Arc<AtomicBool>,
    consumer_triggers: Arc<Mutex<Vec<Arc<AtomicBool>>>>,
  ) -> Result<Option<JoinHandle<()>>> {
    let mut optional_consumer = None;

    let queue_name = queue_name.to_string();
    let consumer_tag = consumer_tag.to_string();

    let handle = task::spawn(async move {
      let message_handler = MessageHandler::new(
        channel.clone(),
        current_status.clone(),
        &queue_name.clone(),
        sender.clone(),
        consumer_triggers.clone(),
      );

      loop {
        match (&optional_consumer, should_consume.load(Ordering::Relaxed)) {
          (Some(_), false) => {
            // if should not consume, unregister consumer from channel
            log::debug!("{} consumer unregisters from channel...", queue_name);

            optional_consumer = None;

            Self::stop_consuming(channel.clone(), &consumer_tag, None)
              .await
              .unwrap();
          }
          (None, true) => {
            // if should consume, reset channel consumer
            log::debug!("{} consumer resume consuming channel...", queue_name);

            optional_consumer = Self::start_consuming(channel.clone(), &queue_name, &consumer_tag)
              .await
              .unwrap();
          }
          _ => {}
        }

        if let Some(mut consumer) = optional_consumer.clone() {
          // Consume messages with timeout
          let next_delivery = timeout(CONSUMER_TIMEOUT, consumer.next()).await;

          if let Ok(Some(delivery)) = next_delivery {
            let (_, delivery) = delivery.expect("error in consumer");

            if !should_consume.load(Ordering::Relaxed) {
              // if should have not consumed a message, reject it and unregister from channel

              log::warn!(
                "{} consumer nacks and requeues received message, and unregisters from channel...",
                queue_name
              );

              optional_consumer = None;

              Self::stop_consuming(channel.clone(), &consumer_tag, Some(delivery))
                .await
                .unwrap();
            } else {
              // else process received message
              if let Err(error) = message_handler.process(&delivery).await {
                log::error!("RabbitMQ consumer: {:?}", error);
                if let Err(error) = publish::error(channel.clone(), vec![delivery], &error).await {
                  log::error!("Unable to publish response: {:?}", error);
                }
              }
            }
          }
        }
      }
    });

    Ok(Some(handle))
  }

  async fn start_consuming(
    channel: Arc<Channel>,
    queue_name: &str,
    consumer_tag: &str,
  ) -> Result<Option<Consumer>> {
    log::info!(
      "Starting RabbitMQ consumer {} on channel {} and queue {}",
      consumer_tag,
      channel.id(),
      queue_name
    );
    Ok(Some(
      channel
        .basic_consume(
          queue_name,
          consumer_tag,
          BasicConsumeOptions::default(),
          FieldTable::default(),
        )
        .await
        .map_err(MessageError::Amqp)?,
    ))
  }

  async fn stop_consuming(
    channel: Arc<Channel>,
    consumer_tag: &str,
    current_delivery: Option<Delivery>,
  ) -> Result<()> {
    log::info!(
      "Stopping RabbitMQ consumer {} from channel {}",
      consumer_tag,
      channel.id()
    );
    if let Some(delivery) = current_delivery {
      channel
        .basic_nack(
          delivery.delivery_tag,
          BasicNackOptions {
            requeue: true,
            ..Default::default()
          },
        )
        .await?;
    }

    channel
      .basic_cancel(consumer_tag, BasicCancelOptions::default())
      .await
      .map_err(MessageError::Amqp)
  }
}

impl Drop for RabbitmqConsumer {
  fn drop(&mut self) {
    self.stop();
  }
}
