use super::{
  helpers::get_message_death_count,
  order_delivery::{OrderDelivery, OrderDeliveryType},
};
use crate::{
  config::{get_soft_reject_count_limit, get_x_death_count_limit},
  http::{delete_job_from_black_list, get_black_list, get_step_flow_version, AuthenticatedClient},
  job::MINIMUM_BLACK_LIST_STEP_FLOW_VERSION,
  prelude::{CurrentState, Job, MessageError, OrderMessage, Result},
};
use async_std::channel::Sender;
use lapin::{
  message::Delivery,
  options::{BasicAckOptions, BasicRejectOptions},
  Channel,
};
use std::{
  convert::TryFrom,
  sync::{
    atomic::{AtomicBool, Ordering},
    Arc, Mutex,
  },
};

pub(crate) struct MessageHandler {
  channel: Arc<Channel>,
  current_state: Arc<Mutex<CurrentState>>,
  queue_name: String,
  sender: Sender<OrderMessage>,
  consumer_triggers: Arc<Mutex<Vec<Arc<AtomicBool>>>>,
}

impl MessageHandler {
  pub(crate) fn new(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    queue_name: &str,
    sender: Sender<OrderMessage>,
    consumer_triggers: Arc<Mutex<Vec<Arc<AtomicBool>>>>,
  ) -> Self {
    Self {
      channel,
      current_state,
      queue_name: queue_name.to_string(),
      sender,
      consumer_triggers,
    }
  }

  pub(crate) async fn process(&self, delivery: &Delivery) -> Result<()> {
    let (count, soft_count) = get_message_death_count(delivery);
    let count = count.unwrap_or(0);
    let soft_count = soft_count.unwrap_or(0);
    let message_data = std::str::from_utf8(&delivery.data)
      .map_err(|e| MessageError::RuntimeError(format!("unable to retrieve raw message: {e:?}")))?;

    let mut order_message = OrderMessage::try_from(message_data)?;

    log::debug!(
      "RabbitMQ consumer on {:?} queue received message: {:?} (iteration: {}, delivery: {})",
      self.queue_name,
      order_message,
      count + soft_count,
      delivery.delivery_tag,
    );

    // Message TTL management
    // TTL is handled within SDK by checking if `x-death` count is above limits
    // BUT for the `requirements` reject, it increases the custom `soft_reject` count and not
    // x-death to distinguish the two, as you cannot both reject and modify headers in RMQ.

    // Case when worker crashed and requeued message with flag redelivered
    // Reject to DLX in order to increase `x-death` count
    if delivery.redelivered {
      log::debug!("Send redelivered message to DLX for x-death increment.");
      return self.reject_delivery(delivery.delivery_tag, false).await;
    }

    // Check if `x-death` count or `soft_reject` count is above specified limits
    let forward_message =
      if count > get_x_death_count_limit() || soft_count > get_soft_reject_count_limit() {
        order_message = match order_message {
          OrderMessage::Job(job) => OrderMessage::ReachedExpiration(job),
          OrderMessage::StartProcess(job) => OrderMessage::ReachedExpiration(job),
          _ => order_message,
        };
        self.set_order_delivery(delivery, OrderDeliveryType::ReachedExpiration);
        true
      } else {
        self.handle(&order_message, delivery).await?
      };

    if forward_message {
      self.forward_message_to_processor(order_message).await?;
    }

    Ok(())
  }

  async fn handle(&self, order_message: &OrderMessage, delivery: &Delivery) -> Result<bool> {
    let mut forward_message = true;
    match &order_message {
      OrderMessage::Job(job) => {
        if self.is_job_black_listed(job) {
          self.handle_black_listed_job(delivery, job).await?;
          forward_message = false;
        } else {
          self.handle_job(delivery).await?;
        }
      }
      OrderMessage::InitProcess(job) => {
        if self.is_job_black_listed(job) {
          self.handle_black_listed_job(delivery, job).await?;
          forward_message = false;
        } else {
          self.handle_init_process(delivery).await?;
        }
      }
      OrderMessage::StartProcess(job) => {
        if self.is_job_black_listed(job) {
          self.handle_black_listed_job(delivery, job).await?;
          forward_message = false;
        } else {
          self.handle_start_process(delivery).await?;
        }
      }
      OrderMessage::UpdateProcess(_job) => {
        self.handle_update_process(delivery).await?;
      }
      OrderMessage::StopProcess(_job) => {
        self.handle_stop_process(delivery).await?;
        forward_message = false;
      }
      OrderMessage::StopWorker => {
        self.handle_stop_worker(delivery).await?;
      }
      OrderMessage::Status => {
        self.handle_status(delivery).await?;
      }
      OrderMessage::StopConsumingJobs => {
        self.handle_stop_consuming_jobs(delivery).await?;
      }
      OrderMessage::ResumeConsumingJobs => {
        self.handle_resume_consuming_jobs(delivery).await?;
      }
      OrderMessage::ReachedExpiration(_job) => {
        self.handle_reached_expiration(delivery).await?;
      }
      OrderMessage::AddToBlackList(message) => {
        self
          .handle_black_list_addition(delivery, message.job_id())
          .await?;
        forward_message = false;
      }
      OrderMessage::DeleteFromBlackList(message) => {
        self
          .handle_black_list_deletion(delivery, message.job_id())
          .await?;
        forward_message = false;
      }
      OrderMessage::SyncBlackList => {
        self.handle_black_list_synchronization(delivery).await?;
        forward_message = false;
      }
      OrderMessage::GetBlackList => {
        self.handle_black_list_request(delivery).await?;
      }
    }
    Ok(forward_message)
  }

  pub(crate) async fn handle_job(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_initializing = current_order_types.contains(&OrderDeliveryType::Init);
    let is_starting = current_order_types.contains(&OrderDeliveryType::Start);
    let has_job = current_order_types.contains(&OrderDeliveryType::Job);

    if is_initializing || is_starting || has_job {
      // Worker already processing
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Job);

    Ok(())
  }

  pub(crate) async fn handle_init_process(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_initializing = current_order_types.contains(&OrderDeliveryType::Init);
    let has_job = current_order_types.contains(&OrderDeliveryType::Job);

    if is_initializing || has_job {
      // Worker already processing
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Init);

    Ok(())
  }

  pub(crate) async fn handle_start_process(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_starting = current_order_types.contains(&OrderDeliveryType::Start);

    if is_starting {
      // Worker already processing
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Start);

    Ok(())
  }

  pub(crate) async fn handle_stop_process(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_initializing = current_order_types.contains(&OrderDeliveryType::Init);
    let is_starting = current_order_types.contains(&OrderDeliveryType::Start);
    let is_stopping = current_order_types.contains(&OrderDeliveryType::Stop);
    let has_job = current_order_types.contains(&OrderDeliveryType::Job);

    if is_stopping {
      log::warn!("Worker process already stopping");
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    if !is_initializing && !is_starting && !has_job {
      log::warn!("No process to stop");
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    // Setting the stop to Some make the exchange to return true on is_stopped()
    self.set_order_delivery(delivery, OrderDeliveryType::Stop);

    Ok(())
  }

  pub(crate) async fn handle_update_process(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_initializing = current_order_types.contains(&OrderDeliveryType::Init);
    let is_starting = current_order_types.contains(&OrderDeliveryType::Start);
    let is_stopping = current_order_types.contains(&OrderDeliveryType::Stop);
    let has_job = current_order_types.contains(&OrderDeliveryType::Job);

    if is_stopping {
      log::warn!("Worker process is stopping");
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    if !is_initializing && !is_starting && !has_job {
      log::warn!("No process to update");
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    // Setting the update to Some make the exchange to return true on is_updated()
    self.set_order_delivery(delivery, OrderDeliveryType::Update);

    Ok(())
  }

  pub(crate) async fn handle_stop_worker(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_initializing = current_order_types.contains(&OrderDeliveryType::Init);
    let is_starting = current_order_types.contains(&OrderDeliveryType::Start);
    let is_stopping = current_order_types.contains(&OrderDeliveryType::Stop);
    let has_job = current_order_types.contains(&OrderDeliveryType::Job);

    if !is_stopping && is_initializing || is_starting || has_job {
      log::info!("Stopping process...");
      // Setting the stop to Some make the exchange to return true on is_stopped()
      self.set_order_delivery(delivery, OrderDeliveryType::Stop);
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Kill);

    Ok(())
  }

  pub(crate) async fn handle_status(&self, delivery: &Delivery) -> Result<()> {
    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_checking_status = current_order_types.contains(&OrderDeliveryType::Status);

    if is_checking_status {
      // Worker already checking status
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Status);

    Ok(())
  }

  pub(crate) async fn handle_stop_consuming_jobs(&self, delivery: &Delivery) -> Result<()> {
    // Stop consuming jobs queues
    for trigger in self.consumer_triggers.lock().unwrap().iter() {
      trigger.store(false, Ordering::Relaxed);
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Suspend);

    Ok(())
  }

  pub(crate) async fn handle_resume_consuming_jobs(&self, delivery: &Delivery) -> Result<()> {
    // Resume consuming jobs queues
    for trigger in self.consumer_triggers.lock().unwrap().iter() {
      trigger.store(true, Ordering::Relaxed);
    }

    self.set_order_delivery(delivery, OrderDeliveryType::Resume);

    Ok(())
  }

  pub(crate) async fn handle_reached_expiration(&self, delivery: &Delivery) -> Result<()> {
    self.set_order_delivery(delivery, OrderDeliveryType::ReachedExpiration);

    Ok(())
  }

  pub(crate) async fn handle_black_list_addition(
    &self,
    delivery: &Delivery,
    job_id: u64,
  ) -> Result<()> {
    log::info!("Received job {} to add to black list.", job_id);
    self.current_state.lock().unwrap().black_list.add(job_id);

    self.ack_delivery(delivery.delivery_tag).await?;

    Ok(())
  }

  pub(crate) async fn handle_black_list_deletion(
    &self,
    delivery: &Delivery,
    job_id: u64,
  ) -> Result<()> {
    log::info!("Received job {} to remove from black list.", job_id);
    self.current_state.lock().unwrap().black_list.remove(job_id);

    self.ack_delivery(delivery.delivery_tag).await?;

    Ok(())
  }

  pub(crate) async fn handle_black_list_synchronization(&self, delivery: &Delivery) -> Result<()> {
    log::info!("Received a black list synchronization notification.");

    let client = AuthenticatedClient::new("BACKEND")?;
    let black_list = get_black_list(client)?;

    log::info!("Black listed jobs: {:?}", black_list);

    self.current_state.lock().unwrap().black_list = black_list;

    self.ack_delivery(delivery.delivery_tag).await?;

    Ok(())
  }

  pub(crate) async fn handle_black_list_request(&self, delivery: &Delivery) -> Result<()> {
    log::info!("Received a black list request.");

    let current_order_types = self.current_state.lock().unwrap().get_types();
    let is_retrieving_black_list = current_order_types.contains(&OrderDeliveryType::GetBlackList);

    if is_retrieving_black_list {
      // Worker already retrieving black list
      return self.reject_delivery(delivery.delivery_tag, true).await;
    }

    self.set_order_delivery(delivery, OrderDeliveryType::GetBlackList);

    Ok(())
  }

  async fn handle_black_listed_job(&self, delivery: &Delivery, job: &Job) -> Result<()> {
    log::warn!("Consume black listed job: {}", job.job_id);

    let client = AuthenticatedClient::new("BACKEND")?;
    let step_flow_version = get_step_flow_version(client.clone())?;

    if step_flow_version.ge(&MINIMUM_BLACK_LIST_STEP_FLOW_VERSION) {
      let black_list = delete_job_from_black_list(client, job.job_id)?;
      self.current_state.lock().unwrap().black_list = black_list;
    }

    log::debug!(
      "Remaining black listed jobs: {:?}",
      self.current_state.lock().unwrap().black_list.get()
    );

    self.ack_delivery(delivery.delivery_tag).await
  }

  async fn forward_message_to_processor(&self, order_message: OrderMessage) -> Result<()> {
    log::debug!(
      "RabbitMQ consumer on {:?} queue forwards the order message: {:?}",
      self.queue_name,
      order_message
    );

    self.sender.send(order_message.clone()).await.map_err(|e| {
      MessageError::RuntimeError(format!("unable to send {order_message:?} order: {e:?}"))
    })?;

    log::debug!("Order message sent!");

    Ok(())
  }

  pub(crate) async fn reject_delivery(&self, delivery_tag: u64, requeue: bool) -> Result<()> {
    log::warn!("Reject delivery {}", delivery_tag);
    self
      .channel
      .basic_reject(delivery_tag, BasicRejectOptions { requeue })
      .await
      .map_err(MessageError::Amqp)
  }

  async fn ack_delivery(&self, delivery_tag: u64) -> Result<()> {
    log::warn!("Acknowledge delivery {}", delivery_tag);
    self
      .channel
      .basic_ack(delivery_tag, BasicAckOptions::default())
      .await?;

    Ok(())
  }

  pub(crate) fn is_job_black_listed(&self, job: &Job) -> bool {
    self
      .current_state
      .lock()
      .unwrap()
      .is_job_black_listed(job.job_id)
  }

  pub(crate) fn set_order_delivery(&self, delivery: &Delivery, order_type: OrderDeliveryType) {
    self
      .current_state
      .lock()
      .unwrap()
      .orders
      .push(OrderDelivery::new(delivery.clone(), order_type));
  }
}
