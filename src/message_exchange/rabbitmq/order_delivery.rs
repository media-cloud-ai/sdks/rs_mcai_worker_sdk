use lapin::message::Delivery;
use std::fmt::{Display, Formatter};

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) enum OrderDeliveryType {
  Init,
  Job,
  Kill,
  Start,
  Stop,
  Status,
  Suspend,
  ReachedExpiration,
  Resume,
  Update,
  GetBlackList,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct OrderDelivery {
  pub(crate) delivery: Delivery,
  pub(crate) order_type: OrderDeliveryType,
  pub(crate) acknowledged: bool,
}

impl OrderDelivery {
  pub(crate) fn new(delivery: Delivery, order_type: OrderDeliveryType) -> Self {
    Self {
      delivery,
      order_type,
      acknowledged: false,
    }
  }

  pub(crate) fn get_type(&self) -> OrderDeliveryType {
    self.order_type.clone()
  }

  pub(crate) fn get_unacknowledged_delivery(&self) -> Option<Delivery> {
    if self.acknowledged {
      return None;
    }
    Some(self.delivery.clone())
  }

  pub(crate) fn set_acknowledged(&mut self) {
    if self.acknowledged {
      log::warn!(
        "Try to acknowledge an already acknowledged delivery message: {}",
        self
      );
      return;
    }
    self.acknowledged = true;
  }

  pub(crate) fn has_order_type(&self, order_type: OrderDeliveryType) -> bool {
    self.order_type == order_type
  }
}

impl Display for OrderDelivery {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    write!(
      f,
      "order: {:?}, delivery: {}, acknowledged: {}",
      self.order_type, self.delivery.delivery_tag, self.acknowledged
    )
  }
}

impl Eq for OrderDelivery {}
