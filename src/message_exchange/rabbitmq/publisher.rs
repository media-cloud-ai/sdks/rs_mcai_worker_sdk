use super::{order_delivery::OrderDeliveryType, publish, CurrentState};
use crate::{
  job::JobProgression,
  message_exchange::message::{Feedback, ResponseMessage},
  worker::WorkerBlackList,
  MessageError, Result,
};
use async_std::{
  channel::{self, Receiver, Sender},
  sync::{Arc, Mutex as AsyncMutex},
  task::{self, JoinHandle},
};
use lapin::{options::QueueDeleteOptions, Channel};
use std::sync::Mutex;

pub struct RabbitmqPublisher {
  handle: Option<JoinHandle<()>>,
  response_sender: Sender<ResponseMessage>,
}

impl RabbitmqPublisher {
  pub async fn new(channel: Arc<Channel>, current_state: Arc<Mutex<CurrentState>>) -> Result<Self> {
    let (response_sender, response_receiver) = channel::unbounded();

    let response_receiver = Arc::new(AsyncMutex::new(response_receiver));

    let handle = Some(task::spawn(async move {
      loop {
        if response_receiver.lock().await.is_closed() {
          break;
        }

        if let Err(error) = Self::handle_response(
          response_receiver.clone(),
          channel.clone(),
          current_state.clone(),
        )
        .await
        {
          log::error!("{:?}", error);
        }
      }
    }));

    Ok(RabbitmqPublisher {
      handle,
      response_sender,
    })
  }

  pub async fn send_response(&self, response: ResponseMessage) {
    self.response_sender.send(response).await.unwrap();
  }

  pub fn stop(&mut self) {
    log::debug!("Close processor-publisher channel...");
    self.response_sender.close();
    self.handle.take().map(JoinHandle::cancel);
  }

  async fn handle_response(
    response_receiver: Arc<AsyncMutex<Receiver<ResponseMessage>>>,
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
  ) -> Result<()> {
    let response = response_receiver.lock().await.recv().await.map_err(|e| {
      MessageError::RuntimeError(format!(
        "unable to wait response from processor: {:?}",
        e.to_string()
      ))
    })?;

    log::debug!("Response: {:?}", response);
    log::debug!("{}", current_state.lock().unwrap());

    match &response {
      ResponseMessage::Feedback(Feedback::Progression(progression)) => {
        Self::handle_progression_feedback_response(channel, progression)?;
      }
      ResponseMessage::Feedback(Feedback::BlackList(worker_black_list)) => {
        Self::handle_black_list_feedback_response(
          channel,
          current_state.clone(),
          worker_black_list.clone(),
        )
        .await?;
      }
      ResponseMessage::WorkerCreated(_) => {
        Self::handle_worker_created_response(channel, &response).await?;
      }
      ResponseMessage::WorkerTerminated(_) => {
        Self::handle_worker_terminated_response(channel, current_state.clone(), &response).await?;
      }
      ResponseMessage::WorkerInitialized(_) => {
        Self::handle_worker_initialized_response(channel, current_state.clone(), &response).await?;
      }
      ResponseMessage::WorkerStarted(_) => {
        Self::handle_worker_started_response(channel, current_state.clone(), &response).await?;
      }
      ResponseMessage::WorkerUpdated(_) => {
        Self::handle_worker_updated_response(channel, current_state.clone(), &response).await?;
      }
      ResponseMessage::JobStopped(_) => {
        Self::handle_job_stopped_response(channel, current_state.clone(), &response).await?;
      }
      ResponseMessage::Completed(_) | ResponseMessage::Error(_) => {
        Self::handle_completed_or_error_response(channel, current_state.clone(), &response).await?;
      }
      ResponseMessage::Feedback(_) | ResponseMessage::StatusError(_) => {
        Self::handle_feedback_or_status_response(channel, current_state.clone(), &response).await?;
      }
    }

    let mut current_state = current_state.lock().unwrap();
    match response {
      ResponseMessage::Feedback(Feedback::Progression(_))
      | ResponseMessage::WorkerCreated(_)
      | ResponseMessage::WorkerInitialized(_)
      | ResponseMessage::WorkerStarted(_)
      | ResponseMessage::WorkerUpdated(_)
      | ResponseMessage::WorkerTerminated(_) => {}
      ResponseMessage::Completed(_)
      | ResponseMessage::Error(_)
      | ResponseMessage::JobStopped(_) => {
        current_state.reset_process_deliveries();
      }
      ResponseMessage::Feedback(_) | ResponseMessage::StatusError(_) => {
        current_state.reset_status_deliveries();
      }
    };

    Ok(())
  }

  async fn handle_completed_or_error_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    let order_type_mapping = vec![
      (OrderDeliveryType::Job, vec![OrderDeliveryType::Job]),
      (OrderDeliveryType::Start, vec![OrderDeliveryType::Start]),
      (OrderDeliveryType::Init, vec![OrderDeliveryType::Init]),
      (OrderDeliveryType::Stop, vec![OrderDeliveryType::Stop]),
      (
        OrderDeliveryType::ReachedExpiration,
        vec![OrderDeliveryType::ReachedExpiration],
      ),
    ];
    Self::respond_with_delivery_regarding_type(
      channel,
      current_state.clone(),
      order_type_mapping,
      response,
    )
    .await
  }

  fn handle_progression_feedback_response(
    channel: Arc<Channel>,
    progression: &JobProgression,
  ) -> Result<()> {
    publish::job_progression(channel, progression.clone())
  }

  async fn handle_feedback_or_status_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    let order_type_mapping = vec![
      (OrderDeliveryType::Suspend, vec![OrderDeliveryType::Suspend]),
      (OrderDeliveryType::Resume, vec![OrderDeliveryType::Resume]),
      (OrderDeliveryType::Status, vec![OrderDeliveryType::Status]),
    ];
    Self::respond_with_delivery_regarding_type(
      channel,
      current_state.clone(),
      order_type_mapping,
      response,
    )
    .await
  }

  async fn handle_black_list_feedback_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    mut worker_black_list: WorkerBlackList,
  ) -> Result<()> {
    worker_black_list.set_black_list(current_state.lock().unwrap().black_list.clone());
    let response = ResponseMessage::Feedback(Feedback::BlackList(worker_black_list));

    let order_type_mapping = vec![(
      OrderDeliveryType::GetBlackList,
      vec![OrderDeliveryType::GetBlackList],
    )];
    Self::respond_with_delivery_regarding_type(
      channel,
      current_state.clone(),
      order_type_mapping,
      &response,
    )
    .await
  }

  async fn handle_job_stopped_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    let mut order_types = vec![];

    if current_state
      .lock()
      .unwrap()
      .has_order_delivery_for_type(OrderDeliveryType::Stop)
    {
      order_types.push(OrderDeliveryType::Stop);
    }

    if current_state
      .lock()
      .unwrap()
      .has_order_delivery_for_type(OrderDeliveryType::Job)
    {
      order_types.push(OrderDeliveryType::Job);
    }

    if !order_types.is_empty() {
      Self::respond_with_delivery(
        channel,
        current_state.clone(),
        order_types,
        response.clone(),
      )
      .await
    }
    Ok(())
  }

  async fn handle_worker_created_response(
    channel: Arc<Channel>,
    response: &ResponseMessage,
  ) -> Result<()> {
    publish::response_with_delivery(channel.clone(), vec![], &response.clone()).await
  }

  async fn handle_worker_initialized_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    let order_type_mapping = vec![
      (OrderDeliveryType::Init, vec![OrderDeliveryType::Init]),
      (OrderDeliveryType::Job, vec![]),
    ];
    Self::respond_with_delivery_regarding_type(
      channel,
      current_state.clone(),
      order_type_mapping,
      response,
    )
    .await
  }

  async fn handle_worker_started_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    let order_type_mapping = vec![
      (OrderDeliveryType::Start, vec![OrderDeliveryType::Start]),
      (OrderDeliveryType::Job, vec![]),
    ];
    Self::respond_with_delivery_regarding_type(
      channel,
      current_state.clone(),
      order_type_mapping,
      response,
    )
    .await
  }

  async fn handle_worker_terminated_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    if current_state
      .lock()
      .unwrap()
      .has_order_delivery_for_type(OrderDeliveryType::Kill)
    {
      Self::respond_with_delivery(
        channel.clone(),
        current_state.clone(),
        vec![OrderDeliveryType::Kill],
        response.clone(),
      )
      .await;
    } else {
      publish::response_with_delivery(channel.clone(), vec![], &response.clone()).await?;
    }

    // Remove direct messaging queue
    if let ResponseMessage::WorkerTerminated(worker_configuration) = response {
      if let Err(err) = channel
        .queue_delete(
          &worker_configuration.get_direct_messaging_queue_name(),
          QueueDeleteOptions::default(),
        )
        .await
      {
        log::warn!("Couldn't remove direct messaging queue. {}", err);
      }
    }

    Ok(())
  }

  async fn handle_worker_updated_response(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    response: &ResponseMessage,
  ) -> Result<()> {
    let order_type_mapping = vec![
      (OrderDeliveryType::Update, vec![OrderDeliveryType::Update]),
      (OrderDeliveryType::Job, vec![]),
    ];
    Self::respond_with_delivery_regarding_type(
      channel,
      current_state.clone(),
      order_type_mapping,
      response,
    )
    .await
  }

  async fn respond_with_delivery_regarding_type(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    order_type_mapping: Vec<(OrderDeliveryType, Vec<OrderDeliveryType>)>,
    response: &ResponseMessage,
  ) -> Result<()> {
    for (sought_type, order_types) in order_type_mapping {
      if current_state
        .lock()
        .unwrap()
        .has_order_delivery_for_type(sought_type)
      {
        Self::respond_with_delivery(
          channel,
          current_state.clone(),
          order_types,
          response.clone(),
        )
        .await;
        break;
      }
    }
    Ok(())
  }

  async fn respond_with_delivery(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
    order_types: Vec<OrderDeliveryType>,
    response: ResponseMessage,
  ) {
    let mut unacknowledged_deliveries = vec![];
    for order_type in order_types {
      if let Some(unacknowledged_delivery) = current_state
        .lock()
        .unwrap()
        .get_order_delivery_for_type(order_type.clone())
        .unwrap()
        .get_unacknowledged_delivery()
      {
        unacknowledged_deliveries.push(unacknowledged_delivery);
      }
    }

    match publish::response_with_delivery(
      channel.clone(),
      unacknowledged_deliveries.clone(),
      &response,
    )
    .await
    {
      Ok(_) => {
        for unacknowledged_delivery in unacknowledged_deliveries {
          current_state
            .lock()
            .unwrap()
            .get_order_delivery_for_tag(unacknowledged_delivery.delivery_tag)
            .unwrap()
            .set_acknowledged()
        }
      }
      Err(error) => {
        if let Err(error) = publish::error(channel.clone(), unacknowledged_deliveries, &error).await
        {
          log::error!("Unable to publish response: {:?}", error);
        }
      }
    }
  }
}

impl Drop for RabbitmqPublisher {
  fn drop(&mut self) {
    self.handle.take().map(JoinHandle::cancel);
  }
}
