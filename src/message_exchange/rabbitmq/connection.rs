use super::{
  channels::declare_consumer_channel,
  consumer::{RABBITMQ_CONSUMER_TAG_DIRECT, RABBITMQ_CONSUMER_TAG_JOB},
  CurrentState, RabbitmqConsumer, RabbitmqPublisher,
};
use crate::{
  config,
  http::{get_black_list, get_step_flow_version, AuthenticatedClient, HttpError},
  job::{BlackList, MINIMUM_BLACK_LIST_STEP_FLOW_VERSION},
  message_exchange::{OrderMessage, ResponseMessage},
  worker::WorkerConfiguration,
  MessageError, Result,
};
use amq_protocol::{tcp::TLSConfig, uri::AMQPScheme};
use async_amqp::*;
use async_std::{channel::Sender, task::JoinHandle};
use lapin::{Channel, Connection, ConnectionProperties};
use std::{
  ops::DerefMut,
  sync::{atomic::Ordering, Arc, Mutex},
  time::Duration,
};

pub struct RabbitmqConnection {
  consumers: Vec<RabbitmqConsumer>,
  publisher: Option<RabbitmqPublisher>,
  current_state: Arc<Mutex<CurrentState>>,
  worker_configuration: WorkerConfiguration,
  connection: Connection,
  channel: Arc<Channel>,
  order_sender: Sender<OrderMessage>,
}

impl RabbitmqConnection {
  pub async fn new(
    worker_configuration: &WorkerConfiguration,
    order_sender: Sender<OrderMessage>,
  ) -> Result<Self> {
    let connection = Self::connect(0)?;

    log::info!("Connected to RabbitMQ");

    let channel = declare_consumer_channel(&connection, worker_configuration);

    let channel = Arc::new(channel.clone());

    let current_state = CurrentState::default();
    let current_state = Arc::new(Mutex::new(current_state));

    let consumers = Self::start_consumers(
      channel.clone(),
      worker_configuration,
      order_sender.clone(),
      current_state.clone(),
    )
    .await?;

    let publisher = Some(Self::start_publisher(channel.clone(), current_state.clone()).await?);

    Ok(RabbitmqConnection {
      consumers,
      publisher,
      current_state,
      worker_configuration: worker_configuration.clone(),
      connection,
      channel,
      order_sender,
    })
  }

  pub async fn start_watching_connection_and_channel(
    connection: Arc<async_std::sync::Mutex<RabbitmqConnection>>,
  ) -> Result<JoinHandle<()>> {
    let task = async_std::task::spawn(async move {
      loop {
        if let Err(error) = Self::check_rabbitmq_connection_and_channel(connection.clone()).await {
          log::error!(
            "An error occurred checking RabbitMQ connection and channel: {:?}",
            error
          );
        }
        async_std::task::sleep(Duration::from_secs(10)).await;
      }
    });

    Ok(task)
  }

  async fn check_rabbitmq_connection_and_channel(
    connection: Arc<async_std::sync::Mutex<RabbitmqConnection>>,
  ) -> Result<()> {
    let connection_status = connection.lock().await.connection.status().clone();
    log::debug!("Check RabbitMQ connection status: {:?}", connection_status);
    if !connection_status.connected() {
      connection.lock().await.stop_consumers_and_publishers();

      let worker_configuration = connection.lock().await.worker_configuration.clone();
      let order_sender = connection.lock().await.order_sender.clone();

      match RabbitmqConnection::new(&worker_configuration, order_sender).await {
        Ok(rabbitmq_connection) => {
          *connection.lock().await.deref_mut() = rabbitmq_connection;
        }
        Err(error) => log::error!(
          "Cannot restart RabbitMQ connection: {:?}. Let's retry after a while...",
          error
        ),
      }
      return Ok(());
    }

    let channel_status = connection.lock().await.channel.status().clone();
    log::debug!("Check RabbitMQ channel status: {:?}", channel_status);
    if !channel_status.initializing() && !channel_status.connected() {
      connection.lock().await.stop_consumers_and_publishers();

      log::warn!("Restart channel, consumers & publisher...");
      let channel = declare_consumer_channel(
        &connection.lock().await.connection,
        &connection.lock().await.worker_configuration,
      );

      let channel = Arc::new(channel.clone());

      let consumers = Self::start_consumers(
        channel.clone(),
        &connection.lock().await.worker_configuration,
        connection.lock().await.order_sender.clone(),
        connection.lock().await.current_state.clone(),
      )
      .await?;

      connection.lock().await.publisher = Some(
        Self::start_publisher(
          channel.clone(),
          connection.lock().await.current_state.clone(),
        )
        .await?,
      );

      connection.lock().await.channel = channel;
      connection.lock().await.consumers = consumers;
    }

    Ok(())
  }

  fn stop_consumers_and_publishers(&mut self) {
    log::info!("Stopping consumers...");
    for consumer in &mut self.consumers {
      consumer.get_trigger().store(false, Ordering::Relaxed);
      consumer.stop();
    }

    self.consumers.clear();

    log::info!("Stopping publisher...");
    if let Some(publisher) = &mut self.publisher {
      publisher.stop();
    }

    self.publisher = None;
  }

  fn connect(attempt: usize) -> Result<Connection> {
    std::thread::sleep(Duration::from_secs((attempt * 2) as u64));

    let connection = async_std::task::block_on(async {
      let amqp_uri = config::get_amqp_uri();
      let properties = ConnectionProperties::default()
        .with_default_executor(8)
        .with_async_std();

      match amqp_uri.scheme {
        AMQPScheme::AMQP => Connection::connect_uri(amqp_uri, properties).await,
        AMQPScheme::AMQPS => {
          let certificate = config::get_amqp_tls_certificate();
          let tls_config = TLSConfig {
            identity: None,
            cert_chain: certificate.as_deref(),
          };
          Connection::connect_uri_with_identity(amqp_uri, properties, tls_config).await
        }
      }
    });

    if let Err(error) = &connection {
      if attempt < config::get_amqp_connection_max_retry() {
        log::error!("{error}, retry...");
        return Self::connect(attempt + 1);
      } else {
        log::error!("Max connection retries reached, let's crash!");
      }
    }

    connection.map_err(|e| e.into())
  }

  async fn start_consumers(
    channel: Arc<Channel>,
    worker_configuration: &WorkerConfiguration,
    order_sender: Sender<OrderMessage>,
    current_state: Arc<Mutex<CurrentState>>,
  ) -> Result<Vec<RabbitmqConsumer>> {
    let queue_name = worker_configuration.get_queue_name();

    let job_consumer = RabbitmqConsumer::new(
      channel.clone(),
      order_sender.clone(),
      &queue_name,
      RABBITMQ_CONSUMER_TAG_JOB,
      current_state.clone(),
      false,
    )
    .await?;

    let queue_name = worker_configuration.get_direct_messaging_queue_name();

    let mut order_consumer = RabbitmqConsumer::new(
      channel,
      order_sender,
      &queue_name,
      RABBITMQ_CONSUMER_TAG_DIRECT,
      current_state.clone(),
      true,
    )
    .await?;

    // Retrieve backend black listed jobs
    let black_list = Self::get_black_list().map_err(|e| {
      MessageError::RuntimeError(format!(
        "Could not retrieve black list from backend: {}",
        e.to_string()
      ))
    })?;

    log::info!("Black listed jobs: {:?}", black_list);

    current_state.lock().unwrap().black_list = black_list;

    // enable job consumption
    job_consumer.get_trigger().store(true, Ordering::Relaxed);

    order_consumer.connect(&job_consumer);

    Ok(vec![job_consumer, order_consumer])
  }

  async fn start_publisher(
    channel: Arc<Channel>,
    current_state: Arc<Mutex<CurrentState>>,
  ) -> Result<RabbitmqPublisher> {
    RabbitmqPublisher::new(channel, current_state).await
  }

  pub async fn send_response(&mut self, response: ResponseMessage) -> Result<()> {
    self
      .publisher
      .as_ref()
      .ok_or_else(|| {
        MessageError::RuntimeError(format!(
          "Cannot publish message: {response:?}. No available RabbitMQ publisher"
        ))
      })?
      .send_response(response.clone())
      .await;
    Ok(())
  }

  pub fn get_current_state(&self) -> Arc<Mutex<CurrentState>> {
    self.current_state.clone()
  }

  fn get_black_list() -> std::result::Result<BlackList, HttpError> {
    let client = AuthenticatedClient::new("BACKEND")?;
    let step_flow_version = get_step_flow_version(client.clone())?;

    if step_flow_version.lt(&MINIMUM_BLACK_LIST_STEP_FLOW_VERSION) {
      log::warn!(
        "Outdated step_flow version: {}. Won't consider jobs black list!",
        step_flow_version
      );
      return Ok(BlackList::default());
    }

    get_black_list(client)
  }
}

impl Drop for RabbitmqConnection {
  fn drop(&mut self) {
    self.stop_consumers_and_publishers();
    self
      .current_state
      .lock()
      .unwrap()
      .reset_process_deliveries();
    self.current_state.lock().unwrap().reset_status_deliveries();
  }
}
