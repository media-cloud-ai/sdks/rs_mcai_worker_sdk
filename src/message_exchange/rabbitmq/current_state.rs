use super::order_delivery::{OrderDelivery, OrderDeliveryType};
use crate::job::BlackList;
use amq_protocol::types::LongLongUInt;
use std::fmt::{Display, Formatter};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct CurrentState {
  pub(crate) orders: Vec<OrderDelivery>,
  pub(crate) black_list: BlackList,
}

impl CurrentState {
  pub(crate) fn reset_process_deliveries(&mut self) {
    self.orders.retain(|order_delivery| {
      order_delivery.has_order_type(OrderDeliveryType::Status)
        || order_delivery.has_order_type(OrderDeliveryType::Kill)
    })
  }

  pub(crate) fn get_types(&self) -> Vec<OrderDeliveryType> {
    self.orders.iter().map(|order| order.get_type()).collect()
  }

  pub(crate) fn is_job_black_listed(&self, job_id: u64) -> bool {
    self.black_list.contains(job_id)
  }

  pub(crate) fn has_order_delivery_for_type(&self, order_type: OrderDeliveryType) -> bool {
    self
      .orders
      .iter()
      .any(|order_delivery| order_delivery.has_order_type(order_type.clone()))
  }

  pub(crate) fn get_order_delivery_for_type(
    &mut self,
    order_type: OrderDeliveryType,
  ) -> Option<&mut OrderDelivery> {
    self
      .orders
      .iter_mut()
      .find(|order_delivery| order_delivery.has_order_type(order_type.clone()))
  }

  pub(crate) fn get_order_delivery_for_tag(
    &mut self,
    delivery_tag: LongLongUInt,
  ) -> Option<&mut OrderDelivery> {
    self
      .orders
      .iter_mut()
      .find(|order_delivery| order_delivery.delivery.delivery_tag == delivery_tag)
  }

  pub(crate) fn reset_status_deliveries(&mut self) {
    self.orders.retain(|order_delivery| {
      !order_delivery.has_order_type(OrderDeliveryType::Status)
        && !order_delivery.has_order_type(OrderDeliveryType::Suspend)
        && !order_delivery.has_order_type(OrderDeliveryType::Resume)
    })
  }
}

impl Display for CurrentState {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    let orders_as_string: Vec<String> = self
      .orders
      .iter()
      .map(|order_delivery| format!("{order_delivery}"))
      .collect();
    write!(f, "CurrentOrders: {orders_as_string:?}")
  }
}

#[test]
pub fn test_current_state() {
  use lapin::message::Delivery;

  fn check_order_delivery(
    delivery: &Delivery,
    optional_order_delivery: Option<&mut OrderDelivery>,
  ) {
    assert!(optional_order_delivery.is_some());

    let extracted_order_delivery = optional_order_delivery.unwrap();
    assert!(!extracted_order_delivery.acknowledged);
    assert_eq!(delivery, &extracted_order_delivery.delivery);
    assert_eq!(
      Some(delivery.clone()),
      extracted_order_delivery.get_unacknowledged_delivery()
    );
    assert_eq!(
      OrderDeliveryType::Status,
      extracted_order_delivery.order_type
    );

    extracted_order_delivery.set_acknowledged();
    assert!(extracted_order_delivery.acknowledged);
    assert_eq!(None, extracted_order_delivery.get_unacknowledged_delivery());

    extracted_order_delivery.acknowledged = false;
  }

  let delivery = Delivery {
    delivery_tag: 0,
    exchange: Default::default(),
    routing_key: Default::default(),
    redelivered: false,
    properties: Default::default(),
    data: vec![],
    acker: Default::default(),
  };

  let order_delivery = OrderDelivery::new(delivery.clone(), OrderDeliveryType::Status);

  assert!(!order_delivery.acknowledged);
  assert_eq!(delivery, order_delivery.delivery);
  assert_eq!(OrderDeliveryType::Status, order_delivery.order_type);

  let mut current_state = CurrentState::default();
  current_state.orders.push(order_delivery);

  assert!(current_state.has_order_delivery_for_type(OrderDeliveryType::Status));

  let extracted_order_delivery_for_type =
    current_state.get_order_delivery_for_type(OrderDeliveryType::Status);
  check_order_delivery(&delivery, extracted_order_delivery_for_type);

  let extracted_order_delivery_for_tag =
    current_state.get_order_delivery_for_tag(delivery.delivery_tag);
  check_order_delivery(&delivery, extracted_order_delivery_for_tag);

  current_state
    .orders
    .push(OrderDelivery::new(delivery, OrderDeliveryType::Job));

  assert!(current_state.has_order_delivery_for_type(OrderDeliveryType::Status));
  assert!(current_state.has_order_delivery_for_type(OrderDeliveryType::Job));
  assert_eq!(
    current_state.get_types(),
    vec![OrderDeliveryType::Status, OrderDeliveryType::Job]
  );

  current_state.reset_process_deliveries();

  assert!(current_state.has_order_delivery_for_type(OrderDeliveryType::Status));
  assert!(!current_state.has_order_delivery_for_type(OrderDeliveryType::Job));
  assert_eq!(current_state.get_types(), vec![OrderDeliveryType::Status]);

  current_state.reset_status_deliveries();

  assert!(!current_state.has_order_delivery_for_type(OrderDeliveryType::Status));
  assert!(!current_state.has_order_delivery_for_type(OrderDeliveryType::Job));

  assert!(current_state.orders.is_empty());
}
