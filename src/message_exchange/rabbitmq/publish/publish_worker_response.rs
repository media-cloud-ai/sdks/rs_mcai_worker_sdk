use super::ack_or_reject_deliveries;
use crate::{config, message_exchange::rabbitmq::EXCHANGE_NAME_WORKER_RESPONSE, Result};
use lapin::{message::Delivery, options::BasicPublishOptions, BasicProperties, Channel};
use std::sync::Arc;

pub async fn publish_worker_response(
  channel: Arc<Channel>,
  deliveries: Vec<Delivery>,
  queue_name: &str,
  payload: &str,
) -> Result<()> {
  log::debug!("Worker publish {}", payload);
  let result = channel
    .basic_publish(
      EXCHANGE_NAME_WORKER_RESPONSE,
      queue_name,
      BasicPublishOptions::default(),
      payload.as_bytes().to_vec(),
      BasicProperties::default().with_delivery_mode(config::get_amqp_delivery_mode()),
    )
    .wait()
    .is_ok();

  ack_or_reject_deliveries(channel, deliveries, result).await?;

  Ok(())
}
