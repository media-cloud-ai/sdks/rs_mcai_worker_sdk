mod job_missing_requirements;
mod job_not_implemented;
mod job_parameter_error;
mod job_progression;
mod publish_job_response;
mod publish_worker_response;

pub use job_missing_requirements::job_missing_requirements;
pub use job_not_implemented::job_not_implemented;
pub use job_parameter_error::job_parameter_error;
pub use job_progression::job_progression;
pub use publish_job_response::publish_job_response;
pub use publish_worker_response::publish_worker_response;

use crate::{
  job::{JobResult, JobStatus},
  message_exchange::{
    message::{Feedback, ResponseMessage},
    rabbitmq::{
      QUEUE_JOB_COMPLETED, QUEUE_JOB_ERROR, QUEUE_JOB_STOPPED, QUEUE_WORKER_BLACK_LIST,
      QUEUE_WORKER_CREATED, QUEUE_WORKER_INITIALIZED, QUEUE_WORKER_STARTED, QUEUE_WORKER_STATUS,
      QUEUE_WORKER_TERMINATED, QUEUE_WORKER_UPDATED,
    },
  },
  MessageError, Result,
};
use amq_protocol::types::LongLongUInt;
use lapin::{
  message::Delivery,
  options::{BasicAckOptions, BasicRejectOptions},
  Channel,
};
use std::sync::Arc;

const NON_UTF8_REPLACEMENT_CHAR: char = '?';

macro_rules! json_payload {
  ($($json:tt)+) => {
    {
      let string = json!($($json)+).to_string();
      // Filter and replace non UTF-8 characters
      string
        .char_indices()
        .map(|(pos, c)| {
          if c.len_utf8() > 1 {
            log::warn!(
              "Invalid {}-bytes character encoding in {} at {}: {}. Expected UTF-8 encoding.",
              c.len_utf8(),
              string,
              pos,
              c.escape_unicode(),
            );
            NON_UTF8_REPLACEMENT_CHAR
          } else {
            c
          }
        })
        .collect::<String>()
    }
  };
}

pub async fn response_with_delivery(
  channel: Arc<Channel>,
  deliveries: Vec<Delivery>,
  response: &ResponseMessage,
) -> Result<()> {
  match response {
    ResponseMessage::WorkerCreated(worker_configuration) => {
      let payload = json_payload!(worker_configuration);

      publish_worker_response(channel, deliveries, QUEUE_WORKER_CREATED, &payload).await
    }
    ResponseMessage::WorkerInitialized(job_result) => {
      let payload = json_payload!(job_result);

      publish_worker_response(channel, deliveries, QUEUE_WORKER_INITIALIZED, &payload).await
    }
    ResponseMessage::WorkerStarted(job_result) => {
      let payload = json_payload!(job_result);

      publish_worker_response(channel, deliveries, QUEUE_WORKER_STARTED, &payload).await
    }
    ResponseMessage::WorkerUpdated(job_result) => {
      let payload = json_payload!(job_result);

      publish_worker_response(channel, deliveries, QUEUE_WORKER_UPDATED, &payload).await
    }
    ResponseMessage::WorkerTerminated(worker_configuration) => {
      let payload = json_payload!(worker_configuration);

      publish_worker_response(channel, deliveries, QUEUE_WORKER_TERMINATED, &payload).await
    }
    ResponseMessage::Completed(job_result) => {
      let payload = json_payload!(job_result);

      publish_job_response(channel, deliveries, QUEUE_JOB_COMPLETED, &payload).await
    }
    ResponseMessage::Error(message_error) => error(channel, deliveries, message_error).await,
    ResponseMessage::JobStopped(job_result) => {
      let payload = json_payload!(job_result);

      publish_job_response(channel, deliveries, QUEUE_JOB_STOPPED, &payload).await
    }
    ResponseMessage::Feedback(feedback) => match feedback {
      Feedback::Progression(progression) => job_progression(channel, progression.clone()),
      Feedback::Status(_process_status) => {
        let payload = json_payload!(feedback);

        publish_worker_response(channel, deliveries, QUEUE_WORKER_STATUS, &payload).await
      }
      Feedback::BlackList(worker_black_list) => {
        let payload = json_payload!(worker_black_list);

        publish_worker_response(channel, deliveries, QUEUE_WORKER_BLACK_LIST, &payload).await
      }
    },
    ResponseMessage::StatusError(message_error) => error(channel, deliveries, message_error).await,
  }
}

pub async fn error(
  channel: Arc<Channel>,
  deliveries: Vec<Delivery>,
  error: &MessageError,
) -> Result<()> {
  match error {
    MessageError::Amqp(lapin_error) => Err(lapin_error.clone().into()),
    MessageError::RequirementsError(details) => {
      job_missing_requirements(channel, deliveries, details).await
    }
    MessageError::NotImplemented() => job_not_implemented(channel, deliveries).await,
    MessageError::ParameterValueError(error_message) => {
      job_parameter_error(channel, deliveries, error_message).await
    }
    MessageError::ProcessingError(job_result) => {
      log::error!(target: &job_result.get_str_job_id(), "Job returned in error: {:?}", job_result.get_parameters());

      let job_result = JobResult::new(job_result.get_job_id())
        .with_status(JobStatus::Error)
        .with_parameters(&mut job_result.get_parameters().clone());

      let payload = json_payload!(job_result).to_string();

      publish_job_response(channel, deliveries, QUEUE_JOB_ERROR, &payload).await
    }
    MessageError::RuntimeError(error_message) => {
      log::error!("An error occurred: {:?}", error_message);
      let payload = json_payload!({
        "status": "error",
        "message": error_message
      })
      .to_string();

      publish_job_response(channel, deliveries, QUEUE_JOB_ERROR, &payload).await
    }
    MessageError::Http(error) => {
      let error_message = error.to_string();
      log::error!("An error occurred: {:?}", error_message);
      let payload = json_payload!({
        "status": "error",
        "message": error_message
      })
      .to_string();

      publish_job_response(channel, deliveries, QUEUE_JOB_ERROR, &payload).await
    }
  }
}

pub(crate) async fn ack_or_reject_deliveries(
  channel: Arc<Channel>,
  deliveries: Vec<Delivery>,
  response_published: bool,
) -> Result<()> {
  if response_published {
    for delivery in deliveries {
      ack(channel.clone(), delivery.delivery_tag).await?;
    }
  } else {
    for delivery in deliveries {
      reject(channel.clone(), delivery.delivery_tag).await?;
    }
  }
  Ok(())
}
async fn ack(channel: Arc<Channel>, delivery_tag: LongLongUInt) -> Result<()> {
  log::debug!("Ack delivery ack {}", delivery_tag);
  channel
    .basic_ack(
      delivery_tag,
      BasicAckOptions::default(), /*not requeue*/
    )
    .await
    .map_err(|e| e.into())
}

async fn reject(channel: Arc<Channel>, delivery_tag: LongLongUInt) -> Result<()> {
  log::debug!("Reject delivery ack {}", delivery_tag);
  channel
    .basic_reject(
      delivery_tag,
      BasicRejectOptions { requeue: true }, /*requeue*/
    )
    .await
    .map_err(|e| e.into())
}

#[test]
pub fn test_json_payload_utf8_char_filtering() {
  assert_eq!("\"Hello!\"", json_payload!("Hello!"));
  assert_eq!("\"?\"", json_payload!("�"));
  assert_eq!(
    "{\"country\":\"????????\"}",
    json_payload!({"country": "Ελληνικά"})
  );
}
