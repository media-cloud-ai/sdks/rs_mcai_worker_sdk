use crate::Result;
use lapin::{message::Delivery, options::BasicRejectOptions, Channel};
use std::sync::Arc;

pub async fn job_parameter_error(
  channel: Arc<Channel>,
  deliveries: Vec<Delivery>,
  details: &str,
) -> Result<()> {
  log::error!("Parameter value error: {}", details);
  for delivery in deliveries {
    channel
      .basic_reject(delivery.delivery_tag, BasicRejectOptions::default())
      .await?;
  }
  Ok(())
}
