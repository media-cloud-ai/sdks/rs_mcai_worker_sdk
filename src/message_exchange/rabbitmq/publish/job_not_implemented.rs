use crate::Result;
use lapin::{message::Delivery, options::BasicRejectOptions, Channel};
use std::sync::Arc;

pub async fn job_not_implemented(channel: Arc<Channel>, deliveries: Vec<Delivery>) -> Result<()> {
  log::error!("Not implemented feature");
  for delivery in deliveries {
    channel
      .basic_reject(
        delivery.delivery_tag,
        BasicRejectOptions { requeue: true }, /*requeue*/
      )
      .await?;
  }

  Ok(())
}
