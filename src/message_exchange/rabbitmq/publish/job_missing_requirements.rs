use crate::{config, Result};
use amq_protocol_types::{AMQPValue, FieldTable};
use lapin::{
  message::Delivery,
  options::{BasicAckOptions, BasicPublishOptions},
  BasicProperties, Channel,
};
use std::{collections::BTreeMap, sync::Arc};

pub async fn job_missing_requirements(
  channel: Arc<Channel>,
  deliveries: Vec<Delivery>,
  details: &str,
) -> Result<()> {
  log::warn!("{}", details);
  for delivery in deliveries {
    if let Err(error) = channel
      .basic_ack(delivery.delivery_tag, BasicAckOptions::default())
      .await
    {
      log::error!(
        "Could not acknowledge message for missing requirements reject: {:?}",
        error
      );
    }

    let original_headers = delivery.properties.headers().as_ref();
    let headers: FieldTable = if let Some(header) = original_headers {
      insert_and_increment_soft_reject_into_headers(header.clone())
    } else {
      FieldTable::default()
    };

    let payload = delivery.data;

    channel
      .basic_publish(
        delivery.exchange.as_str(),
        delivery.routing_key.as_str(),
        BasicPublishOptions::default(),
        payload,
        BasicProperties::default()
          .with_headers(headers)
          .with_delivery_mode(config::get_amqp_delivery_mode()),
      )
      .wait()?;
  }
  Ok(())
}

fn insert_and_increment_soft_reject_into_headers(mut header: FieldTable) -> FieldTable {
  let mut properties = FieldTable::from(BTreeMap::new());
  if let Some(AMQPValue::FieldArray(array)) = header.inner().get("soft_reject") {
    let raw_array = array.as_slice();
    if !raw_array.is_empty() {
      if let AMQPValue::FieldTable(params) = &raw_array[0] {
        if let Some(AMQPValue::LongLongInt(value)) = params.inner().get("count") {
          properties.insert("count".into(), AMQPValue::LongLongInt(*value + 1));
          header.insert(
            "soft_reject".into(),
            AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties)].into()),
          );
          return header;
        }
      }
    }
  }

  properties.insert("count".into(), AMQPValue::LongLongInt(1));
  header.insert(
    "soft_reject".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties)].into()),
  );

  header
}
