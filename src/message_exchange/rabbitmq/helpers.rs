use amq_protocol_types::{AMQPValue, FieldTable};
use lapin::message::Delivery;

pub fn get_message_death_count(message: &Delivery) -> (Option<i64>, Option<i64>) {
  get_counts_from_header(message.properties.headers())
}

fn get_counts_from_header(header: &Option<FieldTable>) -> (Option<i64>, Option<i64>) {
  if let Some(header) = header {
    let x_death_count: Option<i64> = get_header_count_content(header, "x-death");

    let soft_reject_count: Option<i64> = get_header_count_content(header, "soft_reject");

    return (x_death_count, soft_reject_count);
  }
  (None, None)
}

fn get_header_count_content(header: &FieldTable, entry: &str) -> Option<i64> {
  if let Some(AMQPValue::FieldArray(array)) = header.inner().get(entry) {
    let raw_array = array.as_slice();
    if raw_array.is_empty() {
      return None;
    }
    if let AMQPValue::FieldTable(params) = &raw_array[0] {
      if let Some(AMQPValue::LongLongInt(value)) = params.inner().get("count") {
        return Some(*value);
      }
    }
  }
  None
}

#[test]
fn header_information() {
  use std::collections::BTreeMap;

  let header = None;
  let (count, soft_count) = get_counts_from_header(&header);
  assert!(count.is_none());
  assert!(soft_count.is_none());

  let mut map = FieldTable::from(BTreeMap::new());
  let mut properties = FieldTable::from(BTreeMap::new());
  properties.insert("count".into(), AMQPValue::LongLongInt(666));

  map.insert("x-death".into(), AMQPValue::FieldArray(vec![].into()));
  let header = Some(map);
  let (count, soft_count) = get_counts_from_header(&header);
  assert!(count.is_none());
  assert!(soft_count.is_none());

  let mut map = FieldTable::from(BTreeMap::new());
  let mut properties = FieldTable::from(BTreeMap::new());
  properties.insert("count".into(), AMQPValue::LongLongInt(666));

  map.insert(
    "x-death".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties)].into()),
  );
  let header = Some(map);
  let (count, soft_count) = get_counts_from_header(&header);
  assert_eq!(count, Some(666));
  assert!(soft_count.is_none());

  let mut map = FieldTable::from(BTreeMap::new());
  let mut properties = FieldTable::from(BTreeMap::new());
  properties.insert("count".into(), AMQPValue::LongLongInt(666));

  map.insert(
    "x-death".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties)].into()),
  );
  map.insert("soft_reject".into(), AMQPValue::FieldArray(vec![].into()));
  let header = Some(map);
  let (count, soft_count) = get_counts_from_header(&header);
  assert_eq!(count, Some(666));
  assert!(soft_count.is_none());

  let mut map = FieldTable::from(BTreeMap::new());
  let mut properties_1 = FieldTable::from(BTreeMap::new());
  properties_1.insert("count".into(), AMQPValue::LongLongInt(666));
  let mut properties_2 = FieldTable::from(BTreeMap::new());
  properties_2.insert("count".into(), AMQPValue::LongLongInt(444));

  map.insert(
    "x-death".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties_1)].into()),
  );
  map.insert(
    "soft_reject".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties_2)].into()),
  );
  let header = Some(map);
  let (count, soft_count) = get_counts_from_header(&header);
  assert!(count == Some(666));
  assert!(soft_count == Some(444));

  let mut map = FieldTable::from(BTreeMap::new());
  let mut properties_1 = FieldTable::from(BTreeMap::new());
  properties_1.insert("count".into(), AMQPValue::LongLongInt(666));
  let mut properties_2 = FieldTable::from(BTreeMap::new());
  properties_2.insert("count".into(), AMQPValue::LongLongInt(666));

  map.insert(
    "x-death".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties_1)].into()),
  );
  map.insert(
    "soft_reject".into(),
    AMQPValue::FieldArray(vec![AMQPValue::FieldTable(properties_2)].into()),
  );
  let header = Some(map);
  let (count, soft_count) = get_counts_from_header(&header);
  assert!(count == Some(666));
  assert!(soft_count == Some(666));
}
