use super::{order_delivery::OrderDeliveryType, RabbitmqConnection};
use crate::{
  message_exchange::{
    InternalExchange, OrderMessage, ResponseMessage, ResponseSender, WorkerResponseSender,
  },
  prelude::*,
};
use async_std::{
  channel::{self, Receiver},
  task,
  task::JoinHandle,
};
use std::sync::{Arc, Mutex};

pub struct RabbitmqExchange {
  connection: Arc<async_std::sync::Mutex<RabbitmqConnection>>,
  order_receiver: Arc<async_std::sync::Mutex<Receiver<OrderMessage>>>,
  watching_task: Option<JoinHandle<()>>,
}

impl RabbitmqExchange {
  pub async fn new(worker_configuration: &WorkerConfiguration) -> Result<Self> {
    let (order_sender, order_receiver) = channel::unbounded();

    let connection = RabbitmqConnection::new(worker_configuration, order_sender).await?;
    let connection = Arc::new(async_std::sync::Mutex::new(connection));
    let watching_task =
      Some(RabbitmqConnection::start_watching_connection_and_channel(connection.clone()).await?);

    let order_receiver = Arc::new(async_std::sync::Mutex::new(order_receiver));

    Ok(RabbitmqExchange {
      connection,
      order_receiver,
      watching_task,
    })
  }
}

impl InternalExchange for RabbitmqExchange {
  fn send_response(&mut self, response: ResponseMessage) -> Result<()> {
    task::block_on(async move { self.connection.lock().await.send_response(response).await })
  }

  fn get_response_sender(&self) -> Arc<Mutex<dyn ResponseSender + Send>> {
    let connection = self.connection.clone();
    Arc::new(Mutex::new(RabbitmqResponseSender { connection }))
  }

  fn get_worker_response_sender(&self) -> McaiChannel {
    Arc::new(Mutex::new(RabbitmqResponseSender {
      connection: self.connection.clone(),
    }))
  }

  fn get_order_receiver(&self) -> Arc<async_std::sync::Mutex<Receiver<OrderMessage>>> {
    self.order_receiver.clone()
  }
}

impl Drop for RabbitmqExchange {
  fn drop(&mut self) {
    self.watching_task.take().map(JoinHandle::cancel);
  }
}

struct RabbitmqResponseSender {
  connection: Arc<async_std::sync::Mutex<RabbitmqConnection>>,
}

impl ResponseSender for RabbitmqResponseSender {
  fn send_response(&'_ self, message: ResponseMessage) -> Result<()> {
    task::block_on(async move {
      self
        .connection
        .lock()
        .await
        .send_response(message)
        .await
        .unwrap()
    });
    Ok(())
  }
}

impl WorkerResponseSender for RabbitmqResponseSender {
  fn progression(&'_ self, job_id: u64, progression: u8) -> Result<()> {
    let message = ResponseMessage::Feedback(Feedback::Progression(JobProgression::new(
      job_id,
      progression,
    )));
    task::block_on(async move {
      self
        .connection
        .lock()
        .await
        .send_response(message)
        .await
        .unwrap()
    });

    Ok(())
  }

  fn is_stopped(&self) -> bool {
    task::block_on(async move {
      self
        .connection
        .lock()
        .await
        .get_current_state()
        .lock()
        .unwrap()
        .has_order_delivery_for_type(OrderDeliveryType::Stop)
    })
  }
}
