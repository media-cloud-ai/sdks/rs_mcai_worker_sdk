//! Implements a local `MessageExchange`
mod exchange;

pub use exchange::{ExternalLocalExchange, InternalLocalExchange, LocalExchange};
