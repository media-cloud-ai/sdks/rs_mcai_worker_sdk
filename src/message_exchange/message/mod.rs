//! Message definitions between Worker and Message Exchange
//!
//! Message Exchange --> Order Message --> Worker
//! Message Exchange <-- Response Message <-- Worker

mod feedback;
mod order_message;
mod response_message;

pub use feedback::Feedback;
pub use order_message::OrderMessage;
pub use response_message::ResponseMessage;

use crate::{McaiChannel, Result};

/// Function to publish a progression event
///
/// It will be an integer between 0 and 100.
pub fn publish_job_progression(
  channel: Option<McaiChannel>,
  job_id: u64,
  progression: u8,
) -> Result<()> {
  if let Some(response_channel) = channel {
    return response_channel
      .lock()
      .unwrap()
      .progression(job_id, progression);
  }
  Ok(())
}
