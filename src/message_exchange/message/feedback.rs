use crate::{job::JobProgression, processor::ProcessStatus, worker::WorkerBlackList};

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(tag = "type", rename_all = "snake_case")]
#[allow(clippy::large_enum_variant)]
pub enum Feedback {
  Progression(JobProgression),
  Status(ProcessStatus),
  BlackList(WorkerBlackList),
}
