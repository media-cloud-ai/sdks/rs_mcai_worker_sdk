use amq_protocol_uri::{AMQPAuthority, AMQPScheme, AMQPUri, AMQPUserInfo};
use std::{env, str::FromStr, time::Duration};

macro_rules! get_env_value {
  ($key:expr, $default:expr) => {
    match env::var($key) {
      Ok(value) => value,
      _ => $default.to_string(),
    }
  };
}
const AMQP_HOSTNAME: &str = "AMQP_HOSTNAME";
const AMQP_PORT: &str = "AMQP_PORT";
const AMQP_USERNAME: &str = "AMQP_USERNAME";
const AMQP_PASSWORD: &str = "AMQP_PASSWORD";
const AMQP_VHOST: &str = "AMQP_VHOST";
const AMQP_VIRTUAL_HOST: &str = "AMQP_VIRTUAL_HOST";
const AMQP_QUEUE: &str = "AMQP_QUEUE";
const AMQP_TLS: &str = "AMQP_TLS";
const AMQP_TLS_CERT: &str = "AMQP_TLS_CERT";
const AMQP_DELIVERY_MODE: &str = "AMQP_DELIVERY_MODE";
const AMQP_SERVER_CONFIGURATION: &str = "AMQP_SERVER_CONFIGURATION";
const AMQP_CONSUMER_TIMEOUT: &str = "AMQP_CONSUMER_TIMEOUT";
const AMQP_CONNECTION_MAX_RETRY: &str = "AMQP_CONNECTION_MAX_RETRY";

const MESSAGE_TTL_LIMIT: &str = "MESSAGE_TTL_LIMIT";
const MESSAGE_SOFT_LIMIT: &str = "MESSAGE_SOFT_LIMIT";

const FFMPEG_TTL_LIMIT: &str = "FFMPEG_TTL_LIMIT";

const SRT_TIMEOUT_LIMIT: &str = "SRT_TIMEOUT_LIMIT";
const SRT_LATENCY: &str = "SRT_LATENCY";
const SRT_TOO_LATE_PACKET_DROP: &str = "SRT_TOO_LATE_PACKET_DROP";
const SRT_DROP_THRESHOLD_COEFF_PERCENTAGE: &str = "SRT_DROP_THRESHOLD_COEFF_PERCENTAGE";

const SOURCE_ORDERS: &str = "SOURCE_ORDERS";

const COMPLEX_STREAM: &str = "COMPLEX_STREAM";

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum AmqpClusterMode {
  Cluster,
  Standalone,
}

impl AmqpClusterMode {
  pub fn rabbitmq_queue_type(&self) -> String {
    match self {
      AmqpClusterMode::Cluster => "quorum",
      AmqpClusterMode::Standalone => "classic",
    }
    .to_string()
  }
}

fn get_amqp_tls() -> bool {
  let value = get_env_value!(AMQP_TLS, "true");
  matches!(value.as_str(), "true" | "1" | "True" | "TRUE")
}

fn get_amqp_tls_certificate_path() -> Option<String> {
  env::var(AMQP_TLS_CERT).ok()
}

pub fn get_amqp_tls_certificate() -> Option<String> {
  get_amqp_tls_certificate_path()
    .and_then(|certificate_path| std::fs::read_to_string(certificate_path).ok())
}

fn get_amqp_hostname() -> String {
  get_env_value!(AMQP_HOSTNAME, "127.0.0.1")
}

fn get_amqp_port() -> u16 {
  let value = get_env_value!(AMQP_PORT, "5672");
  match value.parse::<u16>() {
    Ok(value) => value,
    _ => 5672,
  }
}

fn get_amqp_username() -> String {
  get_env_value!(AMQP_USERNAME, "guest")
}

fn get_amqp_password() -> String {
  get_env_value!(AMQP_PASSWORD, "guest")
}

fn get_amqp_vhost() -> String {
  get_env_value!(AMQP_VHOST, get_env_value!(AMQP_VIRTUAL_HOST, "/"))
}

pub fn get_amqp_queue() -> String {
  get_env_value!(AMQP_QUEUE, "job_undefined")
}

pub fn get_amqp_delivery_mode() -> u8 {
  let delivery_mode = get_env_value!(AMQP_DELIVERY_MODE, "2");
  u8::from_str(&delivery_mode)
    .ok()
    .and_then(|value| {
      if [1, 2].contains(&value) {
        Some(value)
      } else {
        None
      }
    })
    .unwrap_or_else(|| {
      panic!(
        "Invalid AMQP delivery mode value: {}. Possible values: 1 (transient) or 2 (persistent).",
        delivery_mode
      )
    })
}

pub fn get_amqp_server_configuration() -> AmqpClusterMode {
  if env::var(AMQP_SERVER_CONFIGURATION) == Ok("cluster".to_string()) {
    AmqpClusterMode::Cluster
  } else {
    AmqpClusterMode::Standalone
  }
}

pub fn get_amqp_consumer_timeout() -> Option<u32> {
  let consumer_timeout = get_env_value!(AMQP_CONSUMER_TIMEOUT, "undefined");
  match consumer_timeout.parse::<u32>() {
    Ok(value) => Some(value),
    _ => None,
  }
}

pub fn get_amqp_connection_max_retry() -> usize {
  env::var(AMQP_CONNECTION_MAX_RETRY)
    .map_err(|e| e.to_string())
    .and_then(|value| value.parse::<usize>().map_err(|e| e.to_string()))
    .unwrap_or(5)
}

pub fn get_store_hostname(store_code: &str) -> String {
  get_store_hostname_with_default(store_code, "http://127.0.0.1:4000/api")
}

pub fn get_store_hostname_with_default(store_code: &str, default: &str) -> String {
  get_env_value!(&format!("{store_code}_HOSTNAME"), default)
}

pub fn get_store_username(store_code: &str) -> String {
  get_env_value!(&format!("{store_code}_USERNAME"), "")
}

pub fn get_store_password(store_code: &str) -> String {
  get_env_value!(&format!("{store_code}_PASSWORD"), "")
}

pub fn get_store_token(store_code: &str) -> String {
  get_env_value!(&format!("{store_code}_TOKEN"), "")
}

pub fn get_x_death_count_limit() -> i64 {
  let value = get_env_value!(MESSAGE_TTL_LIMIT, "4");
  match value.parse::<i64>() {
    Ok(value) => value,
    _ => 4,
  }
}

pub fn get_soft_reject_count_limit() -> i64 {
  let value = get_env_value!(MESSAGE_SOFT_LIMIT, "800");
  match value.parse::<i64>() {
    Ok(value) => value,
    _ => 800,
  }
}

pub fn get_srt_timeout_limit() -> u64 {
  let value = get_env_value!(SRT_TIMEOUT_LIMIT, 120_000);
  match value.parse::<u64>() {
    Ok(value) => value,
    _ => 120_000,
  }
}

pub fn get_srt_latency() -> Duration {
  let value = get_env_value!(SRT_LATENCY, 500);
  match value.parse::<u64>() {
    Ok(value) => Duration::from_millis(value),
    _ => Duration::from_millis(500),
  }
}

pub fn get_too_late_packet_drop() -> bool {
  let value = get_env_value!(SRT_TOO_LATE_PACKET_DROP, false);
  match value.parse::<bool>() {
    Ok(value) => value,
    _ => false,
  }
}

pub fn get_packet_drop_threshold() -> Duration {
  let value = get_env_value!(SRT_DROP_THRESHOLD_COEFF_PERCENTAGE, 125);
  match value.parse::<u32>() {
    Ok(value) => value * get_srt_latency() / 100,
    Err(err) => {
      log::warn!(
        "Cannot deserialize SRT packet drop threshold coefficient: {:?}. Drop back to 1.25",
        err
      );
      5 * get_srt_latency() / 4
    }
  }
}

pub fn get_ffmpeg_waitmore_limit() -> i64 {
  let value = get_env_value!(FFMPEG_TTL_LIMIT, "1_000_000");
  match value.parse::<i64>() {
    Ok(value) => value,
    _ => 1_000_000,
  }
}

pub fn get_amqp_uri() -> AMQPUri {
  let amqp_tls = get_amqp_tls();
  let amqp_hostname = get_amqp_hostname();
  let amqp_port = get_amqp_port();
  let amqp_username = get_amqp_username();
  let amqp_password = get_amqp_password();
  let amqp_vhost = get_amqp_vhost();
  let amqp_queue = get_amqp_queue();

  log::info!("Start connection with configuration:");
  log::info!("AMQP TLS: {}", amqp_tls);
  if amqp_tls {
    log::info!("AMQP TLS CERT: {:?}", get_amqp_tls_certificate_path());
  }
  log::info!("AMQP HOSTNAME: {}", amqp_hostname);
  log::info!("AMQP PORT: {}", amqp_port);
  log::info!("AMQP USERNAME: {}", amqp_username);
  log::info!("AMQP VIRTUAL HOST: {}", amqp_vhost);
  log::info!("AMQP QUEUE: {}", amqp_queue);

  let scheme = if amqp_tls {
    AMQPScheme::AMQPS
  } else {
    AMQPScheme::AMQP
  };

  AMQPUri {
    scheme,
    authority: AMQPAuthority {
      userinfo: AMQPUserInfo {
        username: amqp_username,
        password: amqp_password,
      },
      host: amqp_hostname,
      port: amqp_port,
    },
    vhost: amqp_vhost,
    query: Default::default(),
  }
}

pub fn get_source_orders() -> Option<Vec<String>> {
  env::var(SOURCE_ORDERS)
    .map(|source_orders| {
      Some(
        source_orders
          .split(':')
          .map(|path| path.to_string())
          .collect(),
      )
    })
    .unwrap_or(None)
}

pub fn is_complex_stream() -> bool {
  let value = get_env_value!(COMPLEX_STREAM, false);
  match value.parse::<bool>() {
    Ok(value) => value,
    _ => false,
  }
}

#[test]
fn configuration() {
  fn test_config<T: std::cmp::PartialEq + std::fmt::Debug>(
    func: fn() -> T,
    expected_value: T,
    env_var: &str,
  ) {
    // Store and remove environment variable
    let env_value = env::var_os(env_var);
    env::remove_var(env_var);

    // Compare values
    assert_eq!(func(), expected_value);

    // Reset environment variable
    if let Some(env_value) = env_value {
      env::set_var(env_var, env_value)
    }
  }

  test_config(get_amqp_hostname, "127.0.0.1".to_string(), AMQP_HOSTNAME);
  test_config(get_amqp_port, 5672, AMQP_PORT);
  test_config(get_amqp_username, "guest".to_string(), AMQP_USERNAME);
  test_config(get_amqp_password, "guest".to_string(), AMQP_PASSWORD);
  test_config(get_amqp_queue, "job_undefined".to_string(), AMQP_QUEUE);
  test_config(get_amqp_delivery_mode, 2, AMQP_DELIVERY_MODE);

  assert!(get_store_hostname("BACKEND") == *"http://127.0.0.1:4000/api");
  assert!(get_store_username("BACKEND") == *"");
  assert!(get_store_password("BACKEND") == *"");

  env::set_var("AMQP_TLS", "1");
  assert!(get_amqp_tls());
  env::set_var("AMQP_TLS", "False");
  assert!(!get_amqp_tls());
  env::set_var("AMQP_VHOST", "/");
  assert!(get_amqp_vhost() == *"/");
  env::set_var("AMQP_PORT", "BAD_VALUE");
  assert!(get_amqp_port() == 5672);

  env::set_var("MESSAGE_TTL_LIMIT", "2");
  assert!(get_x_death_count_limit() == 2);
  env::set_var("MESSAGE_SOFT_LIMIT", "1000");
  assert!(get_soft_reject_count_limit() == 1000);

  env::set_var("SRT_TIMEOUT_LIMIT", "100000");
  assert!(get_srt_timeout_limit() == 100000);

  assert!(get_srt_latency() == Duration::from_millis(500));
  env::set_var("SRT_LATENCY", "1000");
  assert!(get_srt_latency() == Duration::from_millis(1000));

  assert!(!get_too_late_packet_drop());
  env::set_var("SRT_TOO_LATE_PACKET_DROP", "true");
  assert!(get_too_late_packet_drop());

  assert!(get_packet_drop_threshold() == Duration::from_millis(1250));
  env::set_var("SRT_DROP_THRESHOLD_COEFF_PERCENTAGE", "666");
  assert!(get_packet_drop_threshold() == Duration::from_millis(6660));

  env::set_var("FFMPEG_TTL_LIMIT", "10000000");
  assert!(get_ffmpeg_waitmore_limit() == 10000000);

  env::set_var("AMQP_DELIVERY_MODE", "1");
  assert!(get_amqp_delivery_mode() == 1);

  env::set_var("AMQP_SERVER_CONFIGURATION", "cluster");
  assert!(get_amqp_server_configuration() == AmqpClusterMode::Cluster);

  assert!(!is_complex_stream());
  env::set_var("COMPLEX_STREAM", "true");
  assert!(is_complex_stream());
}

#[test]
#[serial_test::serial]
#[should_panic(
  expected = "Invalid AMQP delivery mode value: 0. Possible values: 1 (transient) or 2 (persistent)."
)]
pub fn test_invalid_amqp_delivery_mode_value() {
  env::set_var("AMQP_DELIVERY_MODE", "0");
  get_amqp_delivery_mode();
}

#[test]
#[serial_test::serial]
#[should_panic(
  expected = "Invalid AMQP delivery mode value: Hello!. Possible values: 1 (transient) or 2 (persistent)."
)]
pub fn test_invalid_amqp_delivery_mode_type() {
  env::set_var("AMQP_DELIVERY_MODE", "Hello!");
  get_amqp_delivery_mode();
}
