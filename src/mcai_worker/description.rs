use super::{McaiWorkerDocumentation, McaiWorkerLicense};

pub trait McaiWorkerDescription: Default {
  fn get_name(&self) -> String;
  fn get_description(&self) -> String;
  fn get_documentation(&self) -> Option<McaiWorkerDocumentation> {
    None
  }

  fn get_version(&self) -> String;
  fn get_license(&self) -> McaiWorkerLicense;
  fn get_authors(&self) -> Vec<String> {
    vec![]
  }

  fn get_homepage(&self) -> Option<String> {
    None
  }
  fn get_repository(&self) -> Option<String> {
    None
  }
  fn get_organisation(&self) -> Option<String> {
    None
  }
}
