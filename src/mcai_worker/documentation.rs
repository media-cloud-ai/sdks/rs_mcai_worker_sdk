#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum McaiWorkerDocumentation {
  String(String),
  Url(String),
}
