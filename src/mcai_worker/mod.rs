mod description;
mod documentation;

pub use description::McaiWorkerDescription;
pub use documentation::McaiWorkerDocumentation;

use crate::prelude::*;
use schemars::{schema::RootSchema, schema_for, JsonSchema};
use serde::de::DeserializeOwned;

// #[cfg(feature = "media")]
// use crate::{FormatContext, ProcessFrame, ProcessResult, StreamDescriptor};
#[cfg(feature = "media")]
use crate::media::{DESTINATION_PATH_PARAMETER, SOURCE_PATH_PARAMETER};
#[cfg(feature = "media")]
use schemars::schema::{InstanceType::Object, SingleOrVec::Single};
#[cfg(feature = "media")]
use std::{
  ops::Deref,
  sync::{mpsc::Sender, Arc, Mutex},
};

/// # Trait to describe a worker
/// Implement this trait to implement a worker
pub trait McaiWorker<P: DeserializeOwned + JsonSchema, D: McaiWorkerDescription> {
  fn get_mcai_worker_description(&self) -> Box<D> {
    Box::<D>::default()
  }

  fn get_parameters_schema(&self) -> Result<RootSchema> {
    let parameters: RootSchema = schema_for!(P);

    #[cfg(feature = "media")]
    if let Some(Single(object)) = &parameters.schema.instance_type {
      if matches!(object.deref(), Object) {
        if let Some(object_validation) = &parameters.schema.object {
          if !object_validation
            .properties
            .contains_key(SOURCE_PATH_PARAMETER)
          {
            return Err(MessageError::ParameterValueError(format!(
              "Expected media parameter missing: '{SOURCE_PATH_PARAMETER}'"
            )));
          }
          if !object_validation
            .properties
            .contains_key(DESTINATION_PATH_PARAMETER)
          {
            return Err(MessageError::ParameterValueError(format!(
              "Expected media parameter missing: '{DESTINATION_PATH_PARAMETER}'"
            )));
          }
        }
      }
    }

    Ok(parameters)
  }

  fn init(&mut self) -> Result<()> {
    Ok(())
  }

  #[cfg(feature = "media")]
  fn init_process(
    &mut self,
    _parameters: P,
    _format_context: Arc<Mutex<FormatContext>>,
    _response_sender: Arc<Mutex<Sender<ProcessResult>>>,
  ) -> Result<Vec<StreamDescriptor>> {
    Ok(vec![])
  }

  #[cfg(feature = "media")]
  fn process_frames(
    &mut self,
    _job_result: JobResult,
    _stream_index: usize,
    _frames: &[ProcessFrame],
  ) -> Result<ProcessResult> {
    Err(MessageError::NotImplemented())
  }

  #[cfg(feature = "media")]
  fn update_process(&mut self, _parameters: P) -> Result<()> {
    Ok(())
  }

  #[cfg(feature = "media")]
  fn ending_process(&mut self) -> Result<()> {
    Ok(())
  }

  fn is_current_job_stopped(channel: &Option<McaiChannel>) -> bool {
    if let Some(channel) = channel {
      return channel.lock().unwrap().is_stopped();
    }
    false
  }

  /// Not called when the "media" feature is enabled
  fn process(
    &self,
    _channel: Option<McaiChannel>,
    _parameters: P,
    _job_result: JobResult,
  ) -> Result<JobResult>
  where
    Self: std::marker::Sized,
  {
    Err(MessageError::NotImplemented())
  }
}
