use crate::media::{
  subrip::{decoder::subrip_vec_to_string, SubRip},
  webvtt::{decoder::webvtt_vec_to_string, WebVtt},
};
use serde::Serialize;
use yaserde::YaSerialize;

#[derive(Debug)]
pub struct ProcessResult {
  pub end_of_process: bool,
  pub json_content: Option<String>,
  pub xml_content: Option<String>,
  pub webvtt_content: Option<String>,
  pub subrip_content: Option<String>,
}

impl ProcessResult {
  pub fn empty() -> Self {
    ProcessResult {
      end_of_process: false,
      json_content: None,
      xml_content: None,
      webvtt_content: None,
      subrip_content: None,
    }
  }

  pub fn end_of_process() -> Self {
    ProcessResult {
      end_of_process: true,
      json_content: None,
      xml_content: None,
      webvtt_content: None,
      subrip_content: None,
    }
  }

  pub fn new_json<S: Serialize>(content: S) -> Self {
    let content = serde_json::to_string(&content).unwrap();

    ProcessResult {
      end_of_process: false,
      json_content: Some(content),
      xml_content: None,
      webvtt_content: None,
      subrip_content: None,
    }
  }

  pub fn new_xml<Y: YaSerialize>(content: Y) -> Self {
    let content = yaserde::ser::to_string(&content).unwrap();

    ProcessResult {
      end_of_process: false,
      json_content: None,
      xml_content: Some(content),
      webvtt_content: None,
      subrip_content: None,
    }
  }

  pub fn new_webvtt(content: Vec<WebVtt>) -> Self {
    let content = webvtt_vec_to_string(content, true, true).unwrap();

    ProcessResult {
      end_of_process: false,
      json_content: None,
      xml_content: None,
      webvtt_content: Some(content),
      subrip_content: None,
    }
  }

  pub fn new_subrip(content: Vec<SubRip>) -> Self {
    let content = subrip_vec_to_string(content, true).unwrap();

    ProcessResult {
      end_of_process: false,
      json_content: None,
      xml_content: None,
      webvtt_content: None,
      subrip_content: Some(content),
    }
  }
}
