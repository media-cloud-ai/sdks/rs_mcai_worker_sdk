use chrono::Utc;
use log::{kv::Key, LevelFilter, Record};
use log4rs::{
  append::{
    console::{ConsoleAppender, Target},
    file::FileAppender,
  },
  config::{Appender, Root},
  encode,
  encode::Encode,
  filter::threshold::ThresholdFilter,
  Config,
};
use std::{
  env,
  str::FromStr,
  sync::{Arc, Mutex},
};

pub type SharedJobId = Arc<Mutex<Option<u64>>>;

#[derive(Debug, Clone)]
pub struct McaiLogEncoder {
  pattern: String,
  job_id: SharedJobId,
}

impl McaiLogEncoder {
  pub fn new(pattern: &str) -> (Self, SharedJobId) {
    let job_id = Arc::new(Mutex::new(None));

    (
      Self {
        pattern: pattern.to_owned(),
        job_id: job_id.clone(),
      },
      job_id,
    )
  }

  pub fn job_id(&self) -> String {
    self
      .job_id
      .lock()
      .unwrap()
      .map(|j| j.to_string())
      .unwrap_or_else(|| "-1".to_string())
  }
}

impl Encode for McaiLogEncoder {
  fn encode(&self, w: &mut dyn encode::Write, record: &Record) -> anyhow::Result<()> {
    let json_content = record
      .key_values()
      .get(Key::from("json"))
      .map(|j| serde_json::to_string(&j).unwrap());

    let job_id = if let Ok(job_id) = record.metadata().target().parse::<u64>() {
      job_id.to_string()
    } else {
      self.job_id()
    };

    if json_content.is_none() && env::var("ONLY_JSON_LOGS").unwrap_or_default() == "1" {
      return Ok(());
    }

    let content = json_content.unwrap_or(format!("{}", record.args()));

    let log_line = self
      .pattern
      .replace("{d}", &Utc::now().to_string())
      .replace("{l}", &record.level().to_string())
      .replace("{job_id}", &job_id)
      .replace("{message}", &content);

    w.write_all(log_line.as_bytes())?;
    Ok(())
  }
}

pub fn configure_logger(container_id: &str, amqp_queue: &str) -> (Config, SharedJobId) {
  // RUST_LOG will be deprecated in future versions
  let log_level = if let Ok(Ok(level)) = std::env::var("MCAI_LOG")
    .or_else(|_| env::var("RUST_LOG"))
    .map(|l| LevelFilter::from_str(&l))
  {
    level
  } else {
    LevelFilter::Info
  };

  let (mcai_encoder, shared_job_id) = McaiLogEncoder::new(&format!(
    "{{d}} - {container_id} - {amqp_queue} - {{job_id}} - {{l}} - {{message}}\n"
  ));

  let stderr = ConsoleAppender::builder()
    .encoder(Box::new(mcai_encoder.clone()))
    .target(Target::Stderr)
    .build();

  let log_builder = Config::builder().appender(
    Appender::builder()
      .filter(Box::new(ThresholdFilter::new(log_level)))
      .build("stderr", Box::new(stderr)),
  );

  let root_builder = Root::builder().appender("stderr");

  let (log_builder, root_builder) = if let Ok(file_logging) = std::env::var("FILE_LOGGING") {
    let file_appender = FileAppender::builder()
      .encoder(Box::new(mcai_encoder))
      .build(file_logging)
      .unwrap();

    let log_builder =
      log_builder.appender(Appender::builder().build("file", Box::new(file_appender)));
    let root_builder = root_builder.appender("file");
    (log_builder, root_builder)
  } else {
    (log_builder, root_builder)
  };

  (
    log_builder.build(root_builder.build(log_level)).unwrap(),
    shared_job_id,
  )
}
