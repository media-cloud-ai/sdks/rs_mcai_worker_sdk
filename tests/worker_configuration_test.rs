extern crate assert_matches;
extern crate mcai_worker_sdk;
#[macro_use]
extern crate serde_derive;

use mcai_worker_sdk::prelude::*;
use schemars::JsonSchema;

#[test]
#[cfg(not(feature = "media"))]
pub fn test_worker_configuration_new() {
  let package: cargo_toml::Package = include!(concat!(env!("OUT_DIR"), "/mcai_build.rs"));

  let queue_name = "queue_name".to_string();
  let instance_id = "instance_id".to_string();
  let sdk_version = if let Ok(version_string) = package.version.get() {
    Version::parse(version_string).unwrap_or_else(|_| Version::new(0, 0, 0))
  } else {
    Version::new(0, 0, 0)
  };

  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!();

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  let result = WorkerConfiguration::new(&queue_name, &mcai_worker, &instance_id);
  assert!(result.is_ok());

  let worker_configuration = result.unwrap();

  assert_eq!(queue_name, worker_configuration.get_queue_name());
  assert_eq!(instance_id, worker_configuration.get_instance_id());
  assert_eq!(
    format!("direct_messaging_{}", instance_id),
    worker_configuration.get_direct_messaging_queue_name()
  );
  assert_eq!(
    sdk_version.to_string(),
    worker_configuration.get_sdk_version()
  );

  let manifest = cargo_toml::Manifest::from_path("./Cargo.toml").unwrap();
  assert_eq!(
    manifest.package.clone().unwrap().name,
    worker_configuration.get_worker_name()
  );
  assert_eq!(
    manifest.package.unwrap().version.get().unwrap(),
    &worker_configuration.get_worker_version()
  );
}

#[test]
pub fn test_worker_configuration_with_license_commercial() {
  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!(Commercial);

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  assert_eq!(
    McaiWorkerLicense::Commercial,
    mcai_worker.get_mcai_worker_description().get_license()
  );

  assert_eq!(
    None,
    mcai_worker.get_mcai_worker_description().get_organisation()
  );
}

#[test]
pub fn test_worker_configuration_with_license_private() {
  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!(Private);

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  assert_eq!(
    McaiWorkerLicense::Private,
    mcai_worker.get_mcai_worker_description().get_license()
  );

  assert_eq!(
    None,
    mcai_worker.get_mcai_worker_description().get_organisation()
  );
}

#[test]
pub fn test_worker_configuration_with_license_commercial_and_organisation() {
  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!("Organisation test", Commercial);

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  assert_eq!(
    McaiWorkerLicense::Commercial,
    mcai_worker.get_mcai_worker_description().get_license()
  );

  assert_eq!(
    Some("Organisation test".to_string()),
    mcai_worker.get_mcai_worker_description().get_organisation()
  );
}

#[test]
pub fn test_worker_configuration_with_license_private_and_organisation() {
  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!("Organisation test", Private);

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  assert_eq!(
    McaiWorkerLicense::Private,
    mcai_worker.get_mcai_worker_description().get_license()
  );

  assert_eq!(
    Some("Organisation test".to_string()),
    mcai_worker.get_mcai_worker_description().get_organisation()
  );
}

#[test]
pub fn test_worker_configuration_with_organisation() {
  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!("Organisation test");

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  assert_eq!(
    McaiWorkerLicense::OpenSource(OpenSourceLicense::new("MIT")),
    mcai_worker.get_mcai_worker_description().get_license()
  );

  assert_eq!(
    Some("Organisation test".to_string()),
    mcai_worker.get_mcai_worker_description().get_organisation()
  );
}

#[test]
#[cfg(feature = "media")]
pub fn test_media_worker_configuration_new() {
  let package: cargo_toml::Package = include!(concat!(env!("OUT_DIR"), "/mcai_build.rs"));

  let queue_name = "queue_name".to_string();
  let instance_id = "instance_id".to_string();
  let sdk_version = if let Ok(version_string) = package.version.get() {
    Version::parse(version_string).unwrap_or_else(|_| Version::new(0, 0, 0))
  } else {
    Version::new(0, 0, 0)
  };

  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {
    #[allow(dead_code)]
    source_path: String,
    #[allow(dead_code)]
    destination_path: String,
  }

  default_rust_mcai_worker_description!();

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  let manifest = cargo_toml::Manifest::from_path("./Cargo.toml").unwrap();

  assert_eq!(
    manifest.package.clone().unwrap().name,
    mcai_worker.get_mcai_worker_description().get_name()
  );
  assert_eq!(
    manifest
      .package
      .clone()
      .unwrap()
      .description
      .unwrap()
      .get()
      .unwrap(),
    &mcai_worker.get_mcai_worker_description().get_description()
  );

  assert_eq!(
    manifest.package.clone().unwrap().version.get().unwrap(),
    &mcai_worker
      .get_mcai_worker_description()
      .get_version()
      .to_string()
  );

  let result = WorkerConfiguration::new(&queue_name, &mcai_worker, &instance_id);
  assert!(result.is_ok());

  let worker_configuration = result.unwrap();

  assert_eq!(queue_name, worker_configuration.get_queue_name());
  assert_eq!(instance_id, worker_configuration.get_instance_id());
  assert_eq!(
    format!("direct_messaging_{instance_id}"),
    worker_configuration.get_direct_messaging_queue_name()
  );
  assert_eq!(
    sdk_version.to_string(),
    worker_configuration.get_sdk_version()
  );

  assert_eq!(
    manifest.package.clone().unwrap().name,
    worker_configuration.get_worker_name()
  );
  assert_eq!(
    manifest.package.unwrap().version.get().unwrap(),
    &worker_configuration.get_worker_version()
  );
}

#[test]
#[cfg(feature = "media")]
pub fn test_media_worker_configuration_new_missing_source_parameter() {
  let queue_name = "queue_name".to_string();
  let instance_id = "instance_id".to_string();

  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {}

  default_rust_mcai_worker_description!();

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  let manifest = cargo_toml::Manifest::from_path("./Cargo.toml").unwrap();
  assert_eq!(
    manifest.package.clone().unwrap().name,
    mcai_worker.get_mcai_worker_description().get_name()
  );
  assert_eq!(
    manifest
      .package
      .clone()
      .unwrap()
      .description
      .unwrap()
      .get()
      .unwrap(),
    &mcai_worker.get_mcai_worker_description().get_description()
  );

  assert_eq!(
    manifest.package.unwrap().version.get().unwrap(),
    &mcai_worker
      .get_mcai_worker_description()
      .get_version()
      .to_string()
  );

  let result = WorkerConfiguration::new(&queue_name, &mcai_worker, &instance_id);
  let expected = MessageError::ParameterValueError(
    "Expected media parameter missing: 'source_path'".to_string(),
  );

  assert!(result.is_err());
  assert_eq!(expected, result.unwrap_err());
}

#[test]
#[cfg(feature = "media")]
pub fn test_media_worker_configuration_new_missing_destination_parameter() {
  let queue_name = "queue_name".to_string();
  let instance_id = "instance_id".to_string();

  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {
    #[allow(dead_code)]
    source_path: String,
  }

  default_rust_mcai_worker_description!();

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  let manifest = cargo_toml::Manifest::from_path("./Cargo.toml").unwrap();

  assert_eq!(
    manifest.package.clone().unwrap().name,
    mcai_worker.get_mcai_worker_description().get_name()
  );
  assert_eq!(
    manifest
      .package
      .clone()
      .unwrap()
      .description
      .unwrap()
      .get()
      .unwrap(),
    &mcai_worker.get_mcai_worker_description().get_description()
  );

  assert_eq!(
    manifest.package.unwrap().version.get().unwrap(),
    &mcai_worker
      .get_mcai_worker_description()
      .get_version()
      .to_string()
  );
  let result = WorkerConfiguration::new(&queue_name, &mcai_worker, &instance_id);
  let expected = MessageError::ParameterValueError(
    "Expected media parameter missing: 'destination_path'".to_string(),
  );

  assert!(result.is_err());
  assert_eq!(expected, result.unwrap_err());
}

#[test]
#[cfg(feature = "media")]
pub fn test_media_worker_configuration_new_missing_parameters() {
  let queue_name = "queue_name".to_string();
  let instance_id = "instance_id".to_string();

  #[derive(Debug)]
  struct CustomEvent {}

  #[derive(JsonSchema, Deserialize)]
  struct CustomParameters {
    #[allow(dead_code)]
    other: String,
  }

  default_rust_mcai_worker_description!();

  impl McaiWorker<CustomParameters, RustMcaiWorkerDescription> for CustomEvent {}

  let mcai_worker = CustomEvent {};

  let manifest = cargo_toml::Manifest::from_path("./Cargo.toml").unwrap();

  assert_eq!(
    manifest.package.clone().unwrap().name,
    mcai_worker.get_mcai_worker_description().get_name()
  );
  assert_eq!(
    manifest
      .package
      .clone()
      .unwrap()
      .description
      .unwrap()
      .get()
      .unwrap(),
    &mcai_worker.get_mcai_worker_description().get_description()
  );

  assert_eq!(
    manifest.package.unwrap().version.get().unwrap(),
    &mcai_worker
      .get_mcai_worker_description()
      .get_version()
      .to_string()
  );

  let result = WorkerConfiguration::new(&queue_name, &mcai_worker, &instance_id);
  let expected = MessageError::ParameterValueError(
    "Expected media parameter missing: 'source_path'".to_string(),
  );

  assert!(result.is_err());
  assert_eq!(expected, result.unwrap_err());
}
