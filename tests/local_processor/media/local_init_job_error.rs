use crate::{instantiate_mcai_worker_description, instantiate_mcai_worker_tests};
use assert_matches::assert_matches;
use mcai_worker_sdk::{prelude::*, ParametersContainer};
use serde::Deserialize;
use std::sync::{mpsc::Sender, Arc, Mutex};

#[test]
fn processor() {
  struct Worker {}

  #[derive(Clone, Debug, Deserialize, JsonSchema)]
  pub struct WorkerParameters {
    #[allow(dead_code)]
    source_path: String,
    #[allow(dead_code)]
    destination_path: String,
  }

  instantiate_mcai_worker_tests!();

  impl McaiWorker<WorkerParameters, WorkerDescription> for Worker {
    instantiate_mcai_worker_description!();
    fn init(&mut self) -> Result<()> {
      log::info!("Initialize processor test worker!");
      Ok(())
    }

    fn init_process(
      &mut self,
      _parameters: WorkerParameters,
      _format_context: Arc<Mutex<FormatContext>>,
      _result: Arc<Mutex<Sender<ProcessResult>>>,
    ) -> Result<Vec<StreamDescriptor>> {
      unimplemented!();
    }

    fn process_frames(
      &mut self,
      _job_result: JobResult,
      _stream_index: usize,
      _frames: &[ProcessFrame],
    ) -> Result<ProcessResult> {
      unimplemented!();
    }
  }

  std::env::set_var("BACKEND_HOSTNAME", mockito::server_url());
  use mockito::mock;

  let _m = mock("POST", "/sessions")
    .with_header("content-type", "application/json")
    .with_body(r#"{"access_token": "fake_access_token"}"#)
    .create();

  let _m = mock("GET", "/credentials/input_file")
    .with_status(404)
    .create();

  let (internal_local_exchange, mut external_local_exchange) = LocalExchange::create();
  let internal_local_exchange = Arc::new(Mutex::new(internal_local_exchange));

  let worker = Worker {};
  let worker_configuration = WorkerConfiguration::new("", &worker, "instance_id").unwrap();

  let worker = Arc::new(Mutex::new(worker));

  std::thread::spawn(move || {
    let mut processor = Processor::new(internal_local_exchange, worker_configuration);
    processor.disable_ctrl_c_handler();

    assert!(processor.run(worker, Arc::new(Mutex::new(None))).is_ok());
  });

  // Check if the worker is created successfully
  assert_matches!(
    external_local_exchange.next_response(),
    Ok(Some(ResponseMessage::WorkerCreated(_)))
  );

  let job = Job::new(
    r#"{
    "job_id": 999,
    "parameters": [
      {
        "id": "source_path",
        "type": "string",
        "store": "BACKEND",
        "value": "input_file"
      },
      {
        "id": "destination_path",
        "type": "string",
        "value": "./test_media_processor.json"
      }
    ]
  }"#,
  )
  .unwrap();

  external_local_exchange
    .send_order(OrderMessage::InitProcess(job.clone()))
    .unwrap();

  let expected_error = format!(
    "\"HTTP status client error (404 Not Found) for url ({}/credentials/input_file)\"",
    mockito::server_url()
  );
  assert_eq!(
    external_local_exchange.next_response(),
    Ok(Some(ResponseMessage::Error(
      MessageError::ParameterValueError(expected_error)
    )))
  );

  external_local_exchange
    .send_order(OrderMessage::StartProcess(job))
    .unwrap();

  assert_matches!(
    external_local_exchange.next_response(),
    Ok(Some(ResponseMessage::Error(MessageError::ProcessingError(job_result)))) => {
      let message: String = job_result.get_parameter("message").unwrap();
      assert_eq!(message, "Cannot start a not initialized job.".to_string());
    }
  );
}
