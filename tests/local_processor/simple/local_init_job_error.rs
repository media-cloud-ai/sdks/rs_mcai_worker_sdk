use crate::{instantiate_mcai_worker_description, instantiate_mcai_worker_tests};
use assert_matches::assert_matches;
use mcai_worker_sdk::prelude::*;
use mockito::mock;
use serde::Deserialize;

#[test]
fn processor_initialization_error() {
  struct Worker {}

  #[derive(Clone, Debug, Deserialize, JsonSchema)]
  pub struct WorkerParameters {
    #[allow(dead_code)]
    credential: String,
  }

  instantiate_mcai_worker_tests!();

  impl McaiWorker<WorkerParameters, WorkerDescription> for Worker {
    instantiate_mcai_worker_description!();

    fn init(&mut self) -> Result<()> {
      log::info!("Initialize processor test worker!");
      Ok(())
    }

    fn process(
      &self,
      _channel: Option<McaiChannel>,
      _parameters: WorkerParameters,
      _job_result: JobResult,
    ) -> Result<JobResult>
    where
      Self: std::marker::Sized,
    {
      unimplemented!();
    }
  }

  std::env::set_var("BACKEND_HOSTNAME", mockito::server_url());

  let _m = mock("POST", "/sessions")
    .with_header("content-type", "application/json")
    .with_body(r#"{"access_token": "fake_access_token"}"#)
    .create();

  let _m = mock("GET", "/credentials/credential_key")
    .with_status(404)
    .create();

  let (internal_local_exchange, mut external_local_exchange) = LocalExchange::create();
  let internal_local_exchange = Arc::new(Mutex::new(internal_local_exchange));

  let worker = Worker {};
  let worker_configuration = WorkerConfiguration::new("", &worker, "instance_id").unwrap();

  let worker = Arc::new(Mutex::new(worker));

  std::thread::spawn(move || {
    let mut processor = Processor::new(internal_local_exchange, worker_configuration);
    processor.disable_ctrl_c_handler();

    assert!(processor.run(worker, Arc::new(Mutex::new(None))).is_ok());
  });

  // Check if the worker is created successfully
  assert_matches!(
    external_local_exchange.next_response(),
    Ok(Some(ResponseMessage::WorkerCreated(_)))
  );

  let message = r#"{ "job_id": 666, "parameters": [
      { "id": "credential", "store": "BACKEND", "type": "string", "value": "credential_key" }
    ] }"#;

  external_local_exchange
    .send_order(OrderMessage::BlackList(BlackList::default()))
    .unwrap();

  let job = Job::new(message).unwrap();
  external_local_exchange
    .send_order(OrderMessage::Job(job))
    .unwrap();

  // let response = local_exchange.next_response().unwrap();
  // assert_matches!(response.unwrap(), ResponseMessage::WorkerInitialized(_));
  //
  // let response = local_exchange.next_response().unwrap();
  // assert_matches!(response.unwrap(), ResponseMessage::WorkerStarted(_));

  assert_matches!(
    external_local_exchange.next_response(),
    Ok(Some(ResponseMessage::Feedback(Feedback::Progression(
      JobProgression {
        job_id: 666,
        progression: 0,
        ..
      }
    ))))
  );

  let expected_error_message = format!(
    "\"HTTP status client error (404 Not Found) for url ({}/credentials/credential_key)\"",
    mockito::server_url()
  );
  let expected_error = MessageError::ParameterValueError(expected_error_message);
  assert_eq!(
    external_local_exchange.next_response(),
    Ok(Some(ResponseMessage::Error(expected_error)))
  );
}
