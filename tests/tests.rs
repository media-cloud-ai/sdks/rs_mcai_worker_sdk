#[cfg(feature = "media")]
extern crate stainless_ffmpeg;

mod worker_definition;

#[cfg(feature = "local_processor_testing")]
mod local_processor {
  #[cfg(not(feature = "media"))]
  mod simple {
    mod local_init_job_error;
    mod local_init_start_processor;
    mod local_job_processor;
    mod local_stop_job;
  }

  #[cfg(feature = "media")]
  use super::generator::{ffmpeg, srt_stream};

  #[cfg(feature = "media")]
  mod media {
    use super::*;
    mod batch_frame_processing;
    mod local_complete_job;
    mod local_init_job_error;
    mod local_interrupt_worker_at_init;
  }
}

#[cfg(feature = "rabbitmq_testing")]
mod rabbitmq_processor {
  #[cfg(not(feature = "media"))]
  mod simple {
    mod rabbitmq_stop_job;
    mod rabbitmq_ttl_job;
  }

  #[cfg(feature = "media")]
  use super::generator::{ffmpeg, srt_stream};

  #[cfg(feature = "media")]
  mod media {
    use super::*;

    mod rabbitmq_stop_job;
    mod rabbitmq_timeout_at_init;
    #[cfg(feature = "live_testing")]
    mod rabbitmq_ttl_job;
    mod rabbitmq_update_job;
  }
}

#[cfg(feature = "media")]
mod generator {
  pub mod ffmpeg;
  pub mod srt_stream;
}

#[cfg(feature = "media")]
mod media {
  use super::generator::ffmpeg;
  mod seek;
}

#[cfg(feature = "rabbitmq_testing")]
mod client {
  mod amqp_connection;
  pub use amqp_connection::AmqpConnection;
}

#[cfg(feature = "functional_testing")]
mod functional {
  #[cfg(not(feature = "media"))]
  mod vault_credentials_test;
}
