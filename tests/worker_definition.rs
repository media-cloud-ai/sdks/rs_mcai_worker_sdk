#[macro_export]
macro_rules! instantiate_mcai_worker_description {
  () => {
    fn get_mcai_worker_description(&self) -> Box<WorkerDescription> {
      Box::new(WorkerDescription)
    }
  };
}

#[macro_export]
macro_rules! instantiate_mcai_worker_tests {
  () => {
    #[derive(Default)]
    struct WorkerDescription;

    impl McaiWorkerDescription for WorkerDescription {
      fn get_name(&self) -> String {
        "Test Worker".to_string()
      }

      fn get_description(&self) -> String {
        "The Worker defined in unit tests".to_string()
      }

      fn get_version(&self) -> String {
        "1.2.3".to_string()
      }

      fn get_license(&self) -> McaiWorkerLicense {
        McaiWorkerLicense::OpenSource(OpenSourceLicense::new("MIT"))
      }
    }
  };
}
