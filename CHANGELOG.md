<a name="unreleased"></a>
## [Unreleased]


<a name="1.1.1"></a>
## [1.1.1] - 2022-01-26
### Feat
- bump to 1.1.1


<a name="1.1.1-rc4"></a>
## [1.1.1-rc4] - 2022-01-24
### Fix
- remove publish started process for simple process


<a name="1.1.1-rc3"></a>
## [1.1.1-rc3] - 2021-12-14
### Fix
- ffmpeg timeout by variable and only for av


<a name="1.1.1-rc2"></a>
## [1.1.1-rc2] - 2021-12-10
### Feat
- bump version to 1.1.1-rc2
- bump to rust 1.56.1

### Fix
- error handling for live process


<a name="1.1.1-rc"></a>
## [1.1.1-rc] - 2021-11-20
### Fix
- add clone to json structure


<a name="1.1.0"></a>
## [1.1.0] - 2021-11-09
### Feat
- bump to 1.1.0

### Fix
- add new cases for new cgroup structure introduced by new docker version


<a name="1.1.0-rc4"></a>
## [1.1.0-rc4] - 2021-11-02
### Fix
- prevent double free in process frame data


<a name="1.1.0-rc3"></a>
## [1.1.0-rc3] - 2021-10-07

<a name="1.1.0-rc2"></a>
## [1.1.0-rc2] - 2021-09-22

<a name="1.1.0-rc"></a>
## [1.1.0-rc] - 2021-09-15
### CI
- add --allow-releaseinfo-change update for coverage step
- set AMQP_HOSTNAME environment variable globally

### Doc
- document ttl algorithm

### Feat
- add ttl handling
- add rabbitmq service in ci
- add updatable params

### Fix
- better handle error message

### Refactor
- add helper functions for insert in headers
- create function returning count from header entry

### Test
- add test for ttl handling

### Tests
- add media part tests for ttl handling


<a name="1.0.0"></a>
## [1.0.0] - 2021-06-29

<a name="1.0.0-rc5"></a>
## [1.0.0-rc5] - 2021-06-24

<a name="1.0.0-rc4"></a>
## [1.0.0-rc4] - 2021-06-11
### CI
- install media dependencies for doc publication

### Reverts
- Simple processor does not respond on job initialized


<a name="1.0.0-rc3"></a>
## [1.0.0-rc3] - 2021-06-03

<a name="1.0.0-rc2"></a>
## [1.0.0-rc2] - 2021-05-04
### Build
- bump stainless ffmpeg to 0.2.6

### CI
- Setup specific environment per job
- override coverage job using tarpaulin config file
- run functional tests
- override test and clippy jobs

### Feat
- bump version to 1.0.0-rc2
- add json support to sdk

### Fix
- get instance uid from kubernetes pod

### Refactor
- use enum for decoder and streaming config
- simplified buffer handling

### Test
- add unit test for json decoding

### Travis
- update Python version to 3.7

### Reverts
- Travis: update Python version to 3.7


<a name="1.0.0-rc1"></a>
## [1.0.0-rc1] - 2021-02-10

<a name="1.0.0-alpha6"></a>
## [1.0.0-alpha6] - 2021-02-09

<a name="1.0.0-alpha5"></a>
## [1.0.0-alpha5] - 2021-02-05
### Feat
- bump srt rs version for multiplex support

### Travis
- update Rust versions
- limit test threads number for tarpaulin


<a name="0.11.9"></a>
## [0.11.9] - 2020-10-29

<a name="v0.11.8"></a>
## [v0.11.8] - 2020-10-22
### Feat
- add python3.7 image and job_id in process


<a name="v0.11.7"></a>
## [v0.11.7] - 2020-10-14

<a name="v0.11.6"></a>
## [v0.11.6] - 2020-10-13

<a name="v0.11.5"></a>
## [v0.11.5] - 2020-10-12

<a name="v0.11.4"></a>
## [v0.11.4] - 2020-10-08

<a name="v0.11.3"></a>
## [v0.11.3] - 2020-10-08

<a name="v0.11.2"></a>
## [v0.11.2] - 2020-10-06

<a name="v0.11.1"></a>
## [v0.11.1] - 2020-10-05

<a name="v0.11.0"></a>
## [v0.11.0] - 2020-09-21
### Python
- clean worker.py file (and apply pep8 code format)
- remove useless Frame::display() function
- worker init() is optional
- errors and iterators handling refactoring
- move py_error_to_string function to helpers
- fix frame data access
- move out some code to new parameters module
- Update parameter support

### Travis
- execute tests with media feature
- install ffmpeg


<a name="v0.10.4"></a>
## [v0.10.4] - 2020-06-02
### Travis
- don't allow Tarpaulin failure
- change caching management


<a name="v0.10.3"></a>
## [v0.10.3] - 2020-05-21

<a name="v0.10.2"></a>
## v0.10.2 - 2020-05-07
### Travis
- set worker library file environment variable and clean build script


[Unreleased]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.1...HEAD
[1.1.1]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.1-rc4...1.1.1
[1.1.1-rc4]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.1-rc3...1.1.1-rc4
[1.1.1-rc3]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.1-rc2...1.1.1-rc3
[1.1.1-rc2]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.1-rc...1.1.1-rc2
[1.1.1-rc]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.0...1.1.1-rc
[1.1.0]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.0-rc4...1.1.0
[1.1.0-rc4]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.0-rc3...1.1.0-rc4
[1.1.0-rc3]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.0-rc2...1.1.0-rc3
[1.1.0-rc2]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.1.0-rc...1.1.0-rc2
[1.1.0-rc]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0...1.1.0-rc
[1.0.0]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-rc5...1.0.0
[1.0.0-rc5]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-rc4...1.0.0-rc5
[1.0.0-rc4]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-rc3...1.0.0-rc4
[1.0.0-rc3]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-rc2...1.0.0-rc3
[1.0.0-rc2]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-rc1...1.0.0-rc2
[1.0.0-rc1]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-alpha6...1.0.0-rc1
[1.0.0-alpha6]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/1.0.0-alpha5...1.0.0-alpha6
[1.0.0-alpha5]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/0.11.9...1.0.0-alpha5
[0.11.9]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.8...0.11.9
[v0.11.8]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.7...v0.11.8
[v0.11.7]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.6...v0.11.7
[v0.11.6]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.5...v0.11.6
[v0.11.5]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.4...v0.11.5
[v0.11.4]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.3...v0.11.4
[v0.11.3]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.2...v0.11.3
[v0.11.2]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.1...v0.11.2
[v0.11.1]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.11.0...v0.11.1
[v0.11.0]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.10.4...v0.11.0
[v0.10.4]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.10.3...v0.10.4
[v0.10.3]: https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk/compare/v0.10.2...v0.10.3
